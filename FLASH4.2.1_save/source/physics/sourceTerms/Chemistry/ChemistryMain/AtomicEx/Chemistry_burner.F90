!***if* Chemistry_burner
!! NAME
!!
!!  Chemistry_burner
!!
!! SYNOPSIS
!!
!!  Chemistry_burner(
!!	 real(in)   :: tstep,
!!	 real(in)   :: temp,
!!       real(in)   :: density,
!!       real(in)   :: xIn(:),
!!	 real(out)  :: xOut(:),
!!	 real(out)  :: sdotRate)
!!
!! DESCRIPTION
!!
!!  Routine chem_burner drives the chemistry network
!!     given time step tstep, temperature temp, density denstiy, and
!!     composition xIn, this routine return the "chemed" composition xOut
!!     and the energy generation rate sdotRate.
!!
!! ARGUMENTS
!!
!! tstep:	time step
!! temp:	temperature
!! density: 	density
!! xIn:		composition in
!! xOut: 	composition out
!! sdotRate:	energy generation rate
!!
!! PARAMETERS
!!
!!
!!  NOTES
!!
!!***

subroutine Chemistry_burner(tstep,temp,density,xIn,xOut,sdotRate,ei,eiback,counts,jcounts,mfrac,tback,clum,cool_ptime,cool_ktime)

	   use Chemistry_data
	   use Eos_interface, ONLY : Eos
	   !use Chemistry_interface 
	   use Simulation_data
	   implicit none

#include "constants.h"
#include "Flash.h"
#include "Multispecies.h"
#include "Eos.h"

real   :: tstep, temp, density, sdotRate, counts, jcounts, mfrac, tback, ei, eiback
real, dimension(13)         :: clum
real, dimension(NSPECIES)  :: xIn, xOut
external  Chemistry_integrate, Chemistry_network, Chemistry_networkDJ,  &
        & Chemistry_networkSP, Chemistry_netIntRosen, Chemistry_networkSJ, &
        & Chemistry_netIntBader

integer   :: i,k,nok,nbad,kount,cool_ptime,cool_ktime
integer, parameter   :: tdim=10, iprint=0, nostore=0
integer  :: issmall

real     :: stptry,stpmin,ys2(NSPECIES+1),ttime(tdim),elem(NSPECIES+1,tdim)
real, parameter       :: avo = 6.0221367e23, ev2erg = 1.602e-12, &
                         conv = ev2erg*1.0e0*avo, tol = 1.0e-7, &
                         beg = 0.0, odescal = 1.0e-7
real       :: ttt, ddyddx(NSPECIES+1)  !!DUMMY variables for use in error control 
real       :: dtprime, ttmin, ttemp, smallx, avgm
real                  :: eosData(EOS_NUM), ysav(NSPECIES+1)
real       :: xeos(SPECIES_BEGIN:SPECIES_END) !!For EOS calls
real       :: eisave, tsave, ctemp, den
real	   :: c_dens, c_temp, c_ei, co_ei, co_temp
real	   :: eis, tauei
real, dimension(NSPECIES) :: photocc

!!Here we go


tback = 0.0e0 !!send temperature back
ctemp = temp
den = density
smallx = 1.0e-20
eisave = ei
tsave = ctemp
xeos(:) = 0.0e0
eosData(:) = 0.0e0
photocc(:) = 0.0e0

!!print *, 'BURNER'
!print *, 'temp: ', ctemp, 'dens: ', den
!print *,' Top of CB: ', ctemp

eosData(EOS_TEMP) = ctemp
eosData(EOS_DENS) = den
eosData(EOS_EINT) = ei

!!Get initial mass fractions and save them to xmass
do i=1,NSPECIES
   xmass(i) = xIn(i)
enddo

call Chemistry_azbar() !!get ymass

do i=1,NSPECIES
   ys2(i) = ymass(i)
   if(ys2(i) .lt. 0.0) then
      ys2(i) = smallx
   endif
enddo

!!Setup initial timestep and counters 
stptry = tstep
stpmin = stptry*1.0e-20 

ttt = 0.0
counts = 0.0
jcounts = 0.0

call Chemistry_networkRates(ctemp,den,ys2)          !!get the reaction rates from our equations
call Chemistry_network(ttt,ys2,ddyddx) !! This grabs my odes and puts them in a dummy array for me and it works!

dtprime = tstep  !!Setup the initial time step
ttemp = tstep
issmall = -1  

ttmin = 0.0e0
do i=1,NSPECIES  !!Don't count electrons
   ttmin = abs(sim_chem_time*(ys2(i)+0.01*ys2(iHP))/(ddyddx(i)+1.0e-30))
   if( ttmin .lt. ttemp) then 
       if(ys2(i) .gt. smallx) then 
         ttemp = ttmin
         issmall = i
       endif
   endif
enddo

eis = 0.0e0
tauei= 0.0e0
do i=1,NSPECIES
   eis = eis + ddyddx(i)*bion(i)
enddo
tauei = 0.1*abs(ei/(eis*conv))   
if( tauei .lt. ttemp) then
    ttemp = tauei
endif

do while(dtprime .gt. 0.0)    
  if(dtprime .le. ttemp) then  !NO SUB
     stptry = dtprime
     stpmin = dtprime*1.0e-20 
     counts = counts

    do k=1,NSPECIES
         ysav(k) = ys2(k)
    enddo
    
!    call Chemistry_integrate(beg,stptry,stpmin,dtprime,ys2,tol,beg,nostore,ttime,elem, &
!           tdim,NSPECIES,tdim,NSPECIES,nok,nbad,kount,odescal,iprint, &
!          Chemistry_network,Chemistry_networkDJ,Chemistry_networkSP,Chemistry_netIntRosen,jcounts)
    !call Chemistry_integrate(beg,stptry,stpmin,dtprime,ys2,tol,beg,nostore,ttime,elem, &
    !       tdim,NSPECIES,tdim,NSPECIES,nok,nbad,kount,odescal,iprint, &
    !      Chemistry_network,Chemistry_networkSJ,Chemistry_netIntRosen,jcounts)

    !write(*,'(A,1PE10.2,A,1PE10.2)') 'DENS: ', den, ' TEMP: ', temp
    !write(*,'(54(1X,1PE10.2))') ys2
    call Chemistry_integrate(beg,stptry,stpmin,dtprime,ys2,tol,beg,nostore,ttime,elem, &
           tdim,NSPECIES,tdim,NSPECIES,nok,nbad,kount,odescal,iprint, &
          Chemistry_network,Chemistry_networkSJ,Chemistry_netIntBader,jcounts,den,ctemp)

    ys2(iELEC) = ys2(iHP) + ys2(iHEP) + 2.0*ys2(iHEPP)
 
!!make sure nothing went neg
   do k=1,NSPECIES
     if(ys2(i) .lt. 0.0) ys2(i) = smallx
   enddo

   photocc = 0.0e0
   call Chemistry_photo(ys2,ddyddx,photocc)

   sdotRate = 0.0e0
   !print *,'SDOT_BEFORE: ', sdotRate
    do k=1,NSPECIES
       sdotRate = sdotRate + (ys2(k)-ysav(k))*bion(k)*photocc(k)
      !write(*,'(3(A,1pe10.3))'), 'old_y: ', ysav(k), ' new_y: ', ys2(k), ' bion: ', bion(k)
    enddo
    !print *,'SDOT_BEFORE: ', sdotRate, ' eichange: ', sdotRate*conv, ' ei_old: ', ei
   
   if(cool_ktime == 1) sdotRate = 0.0e0

   ei = ei + sdotRate*conv  !!Update ei from burning

   !print *,'NO SUB'
   !print *,'Before EOS After ei change: ', ctemp
   eosData(EOS_DENS) = den
   eosData(EOS_TEMP) = ctemp
   eosData(EOS_EINT) = ei
   !print *,'denb: ', den, ' ctempb:', ctemp, ' eib: ', ei  

   if(ei .lt. 0.0e0) then
     write(*,*) "BROKE HERE: 1"
     write(*,*) 'DENS: ', den, ' Temp: ', temp, ' EI: ', ei
     write(*,*) ' Y_BEFORE: ', ysav
     write(*,*) ' Y_AFTER: ', ys2
     call Driver_abortFlash("BROKE HERE 1 : CHEM")
   endif

   call do_eos(ys2,eosData,cool_ktime)
   
   den = eosData(EOS_DENS)
   ctemp = eosData(EOS_TEMP)
   ei = eosData(EOS_EINT)
   !print *,'dena: ', den, ' ctempa:', ctemp, ' eia: ', ei  
   !print *,'After EOS After ei change: ', ctemp
!!ADD COOLING HERE NEXT


   if(chem_doCool .eq. 1 .and. cool_ptime == 1) then
     ! print *,' IN CB First Cool: ', ctemp
      c_dens = den
      c_temp = ctemp
      c_ei   = ei
      co_temp = 0.0
      co_ei   = 0.0
      !!subroutine Cool_function(temp_in, dens_in, y_in, ei_in, times, mfrac_in, temp_out, ei_out)
      !!print *, 'before cool_fun'
      call Cool_function(c_temp, c_dens, ys2, c_ei, dtprime, mfrac, co_temp, co_ei,clum)
      !!print *, 'after cool_fun', ' cotemp: ', co_temp  

      eosData(EOS_DENS) = den
      eosData(EOS_TEMP) = co_temp
      eosData(EOS_EINT) = co_ei
      !print *,'co_temp: ', co_temp, ' co_ei: ', co_ei

      call do_eos(ys2,eosData,cool_ktime)

      den = eosData(EOS_DENS)
      ctemp = eosData(EOS_TEMP)
      ei = eosData(EOS_EINT)

      if(ei .lt. 0.0e0) then
        write(*,*) "BROKE HERE: 3"
    	write(*,*) 'DENS: ', den, ' Temp: ', temp, ' EI: ', ei
     	write(*,*) ' Y_BEFORE: ', ysav
     	write(*,*) ' Y_AFTER: ', ys2
     	call Driver_abortFlash("BROKE HERE 3 : CHEM")
      endif

   endif
   dtprime = 0.0

  else
   counts = counts + 1.0

!write(*,*) 'DING!'
!write(*,*) 'ismall: ', issmall, ' ttemp: ', ttemp, ' dtprime: ', dtprime

   do k=1,NSPECIES+1
       ysav(k) = ys2(k)
   enddo

    ! Inside of the ODE steppers, the full timestep is now ttemp
    stpmin = ttemp*1.0e-20
    stptry = ttemp
    
!    call Chemistry_integrate(beg,stptry,stpmin,dtprime,ys2,tol,beg,nostore,ttime,elem, &
!    tdim,NSPECIES,tdim,NSPECIES,nok,nbad,kount,odescal,iprint, &
!    Chemistry_network,Chemistry_networkDJ,Chemistry_networkSP,Chemistry_netIntRosen,jcounts)
 
 
    !call Chemistry_integrate(beg,stptry,stpmin,ttemp,ys2,tol,beg,nostore,ttime,elem, &
    !tdim,NSPECIES,tdim,NSPECIES,nok,nbad,kount,odescal,iprint, &
    !Chemistry_network,Chemistry_networkSJ,Chemistry_netIntRosen,jcounts)
 
    call Chemistry_integrate(beg,stptry,stpmin,ttemp,ys2,tol,beg,nostore,ttime,elem, &
    tdim,NSPECIES,tdim,NSPECIES,nok,nbad,kount,odescal,iprint, &
    Chemistry_network,Chemistry_networkSJ,Chemistry_netIntBader,jcounts,den,ctemp)

    ys2(iELEC) = ys2(iHP) + ys2(iHEP) + 2.0*ys2(iHEPP) 
             
    do k=1,NSPECIES
       if(ys2(i) .lt. 0.0) ys2(i) = smallx
    enddo
    photocc = 0.0e0
    call Chemistry_photo(ys2,ddyddx,photocc)

!!Update times
    dtprime = dtprime - ttemp
    !!ttemp = dtprime
    sdotRate = 0.0e0
    do k=1,NSPECIES
      sdotRate = sdotRate + (ys2(k)-ysav(k))*bion(k)*photocc(k)
    enddo

    if(cool_ktime == 1) sdotRate = 0.0e0

    ei = ei + sdotRate*conv  !!Update ei from burning

    eosData(EOS_DENS) = den
    eosData(EOS_TEMP) = ctemp
    eosData(EOS_EINT) = ei
 !   print *, 'before eos: ', ctemp, ' ' , ei
 !   print *, 'ys: ', ys2

    if(ei .lt. 0.0e0) then
     write(*,*) "BROKE HERE: 2"
     write(*,*) 'DENS: ', den, ' Temp: ', temp, ' EI: ', ei, ' JCOUNTS: ', jcounts, ' counts: ', counts
     write(*,*) ' Y_BEFORE: ', ysav
     write(*,*) ' Y_AFTER: ', ys2
     call Driver_abortFlash("BROKE HERE 2 : CHEM")
    endif

    call do_eos(ys2,eosData,cool_ktime)

    den = eosData(EOS_DENS)
    ctemp = eosData(EOS_TEMP)
    ei = eosData(EOS_EINT)
 !   print *, 'after eos: ', ctemp, ' ' , ei

!!Next would be cooling, need to add next
    if(chem_doCool .eq. 1 .and. cool_ptime == 1) then
       c_dens = den
       c_temp = ctemp
       c_ei   = ei
       co_temp = 0.0
       co_ei   = 0.0
!!   subroutine Cool_function(temp_in, dens_in, y_in, ei_in, times, mfrac_in, temp_out, ei_out)
       call Cool_function(c_temp, c_dens, ys2, c_ei, ttemp, mfrac, co_temp, co_ei,clum)
   
       eosData(EOS_DENS) = den
       eosData(EOS_TEMP) = co_temp
       eosData(EOS_EINT) = co_ei
       call do_eos(ys2,eosData,cool_ktime)
       den = eosData(EOS_DENS)
       ctemp = eosData(EOS_TEMP)
       ei = eosData(EOS_EINT)
    endif


    call Chemistry_networkRates(ctemp,den,ys2)          !!get the reaction rates from our equations
    call Chemistry_network(ttt,ys2,ddyddx)

    ttemp = dtprime
    do i=1,NSPECIES  !!Don't count electrons
       ttmin = abs(sim_chem_time*(ys2(i)+0.01*ys2(iHP))/ddyddx(i))
       if(ttmin .lt. ttemp) then 
          if(ys2(i) .gt. smallx) then 
            ttemp = ttmin
            issmall = i
          endif
        endif 
    enddo   
    
    eis = 0.0e0
    tauei= 0.0e0
    do i=1,NSPECIES
       eis = eis + ddyddx(i)*bion(i)
    enddo
    tauei = 0.1*abs(ei/(eis*conv))   
    if( tauei .lt. ttemp) then
    	ttemp = tauei
    endif

    !if(ttemp .lt. 1.0e4) then
    !   ttemp = 1.0e4
    !endif
    ! if(ttemp .gt. dtprime) then
    !     ttemp = dtprime
    !endif

  endif
enddo


do i=1,NSPECIES
   xOut(i) = ys2(i)*aion(i)  
   if(xOut(i) .lt. 0.0) xOut(i) = smallx
enddo
tback = ctemp  
eiback = ei
return
end subroutine Chemistry_burner





subroutine do_eos(ys2,eosData,cool_ktime)

  use Chemistry_data
  use Eos_interface, ONLY : Eos
  use Chemistry_interface
  implicit none

#include "constants.h"
#include "Multispecies.h"
#include "Flash.h"
#include "Eos.h"


real, dimension(NSPECIES), intent(IN)          :: ys2
real, dimension(EOS_NUM), intent(INOUT)        :: eosData
real, dimension(EOS_NUM)                       :: mask, eosDataS
integer                                        :: specieMap, i, cool_ktime
real, dimension(SPECIES_BEGIN:SPECIES_END)     :: xe

xe(:) = 1.0e-20
do i=1,NSPECIES
   call Chemistry_mapNetworkToSpecies(i,specieMap)
   xe(specieMap) = ys2(i)*aion(i)
   !!write(*,'(2(A,I),3(A,1pe10.3))'), 'I: ', i, ' smap: ', specieMap, ' xe: ', xe(specieMap), ' ys2: ', ys2(i), ' aion(i): ', aion(i)
enddo
!!call abort()

eosDatas = eosData

!print *,' chem_doCool: ', chem_doCool
!!!print *,'eostemp_before: ', eosData(EOS_TEMP)
if(chem_doCool .eq. 1 .and. cool_ktime .eq. 0 ) then
    call Eos(MODE_DENS_EI,1,eosData,xe) !!,mask)
else
    call Eos(MODE_DENS_TEMP,1,eosData,xe) !!,mask)
endif

!!print *,'eostemp_after: ', eosData(EOS_TEMP)

if(eosData(EOS_TEMP) .lt. 0.0e0) then
  write(*,*) 'BEFORE:   DENS: ', eosDatas(EOS_DENS), ' TEMP: ', eosDatas(EOS_TEMP), ' PRES: ', eosDatas(EOS_PRES), ' EINT: ', eosDatas(EOS_EINT)
  write(*,*) 'AFTER:   DENS: ', eosData(EOS_DENS), ' TEMP: ', eosData(EOS_TEMP), ' PRES: ', eosData(EOS_PRES), ' EINT: ', eosData(EOS_EINT)
  write(*,*) 'SPECIES IN: ', ys2
  call Driver_abortFlash("ERROR IN DO_EOS. EOS RETURNED NEG TEMP")
endif

if(eosData(EOS_TEMP) .lt. chem_tempmin) then
   print *, 'HIT MIN: ',eosdata(EOS_TEMP)
   !print *, 'BEFORE: ', eosData
   !print *, 'EOS_TEMP: ', eosData(EOS_TEMP)
   !print *, 'EOS_EINT: ', eosData(EOS_EINT)
   !print *, 'EOS_DENS: ', eosData(EOS_DENS)
   !print *, 'EOS_PRES: ', eosData(EOS_PRES)
   !print *, 'B: XH:    ', xe(H_SPEC)
   !print *, 'B: XH+:   ', xe(HP_SPEC)
   !print *, 'B: XHe:   ', xe(HE_SPEC)
   !print *, 'B: XHe+:  ', xe(HEP_SPEC)
   !print *, 'B: XHe2+: ', xe(HEPP_SPEC)
   !print *, 'B: XC:    ', xe(C_SPEC)
   !print *, 'B: XC+:   ', xe(CP_SPEC)
   !print *, 'B: XC2+:  ', xe(C2P_SPEC)
   !print *, 'B: XC3+:  ', xe(C3P_SPEC)
   !print *, 'B: XC4+:  ', xe(C4P_SPEC)
   !print *, 'B: XO:    ', xe(O_SPEC)
   !print *, 'B: XO+:   ', xe(OP_SPEC)
   !print *, 'B: XO2+:  ', xe(O2P_SPEC)
   !print *, 'B: XO3+:  ', xe(O3P_SPEC)
   !print *, 'B: XO4+:  ', xe(O4P_SPEC)
   !print *, 'B: XO5+:  ', xe(O5P_SPEC)
   !print *, 'B: XO6+:  ', xe(O6P_SPEC)
   !print *, 'B: XO7+:  ', xe(O7P_SPEC)
   !print *, 'B: XNa:   ', xe(NA_SPEC)
   !print *, 'B: XNa+:  ', xe(NAP_SPEC)
   !print *, 'B: XMg:   ', xe(MG_SPEC)
   !print *, 'B: XMg+:  ', xe(MGP_SPEC)
   !print *, 'B: XMg2+: ', xe(MG2P_SPEC)
   !print *, 'B: Xe:    ', xe(ELEC_SPEC)
   
   eosData(EOS_TEMP) = chem_tempmin
   call Eos(MODE_DENS_TEMP,1,eosData,xe)
   !print *, 'EOS_TEMP: ', eosData(EOS_TEMP)
   !print *, 'EOS_EINT: ', eosData(EOS_EINT)
   !print *, 'EOS_DENS: ', eosData(EOS_DENS)
   !print *, 'EOS_PRES: ', eosData(EOS_PRES)
endif

return
end subroutine do_eos
