import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('chem_rates.txt')

NP = 5
Nplots = data.shape[1]


fig = plt.figure()
plt.clf()

Nrot = (Nplots/NP)

iat = 0
for i in range(Nrot + 1):
	if(i == Nrot):
		NN = (Nplots-NP*Nrot)
	else:
		NN = 5
	print i,NN
	for j in range(NN):
		plt.loglog(data[:,-1],data[:,iat],lw=3,label='%i'%iat)
		iat = iat +1
	plt.legend()
	ii = str('%i'%(i)).zfill(3)
	ln = 'Plot_'+ii+'.pdf'
	plt.savefig(ln)
	plt.clf()

