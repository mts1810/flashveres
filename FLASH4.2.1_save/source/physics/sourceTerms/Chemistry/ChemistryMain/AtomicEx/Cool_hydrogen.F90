!!****Cool_hydrogen
!!
!!
!!
!! NAME
!!  Cool_hydrogen
!!
!! SYNOPSIS
!!	Calculates the cooling due to hydrogen
!!  	Total cooling rate is returned as cout_h
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_hydrogen(temp,rho,tindex,nin,cout_h)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_h
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: h0term, h1term

!! This will calculate the cooing from hydrogen
!! We are linearaly interpolating along the cooling functions

cout_h = 0.0e0
h0term = 0.0e0
h1term = 0.0e0


!!H0 first. All of these are n_ion * n_elec * CF / rho

if(tindex == clength) then
   h0term = coolh0(clength)
   h1term = coolh1(clength)
else if (tindex == 1) then
   h0term = coolh0(1)
   h1term = coolh1(1)
else
   h0term = coolh0(tindex) + (coolh0(tindex+1)-coolh0(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   h1term = coolh1(tindex) + (coolh1(tindex+1)-coolh1(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
endif

h0term = h0term * nin(iH)  * nin(iELEC) 
if(h0term .ne. h0term) h0term = 1.0e-30
h1term = h1term * nin(iHP) * nin(iELEC) 
if(h1term .ne. h1term) h1term = 1.0e-30
cout_h = (h0term+h1term)/rho
if(cout_h .ne. cout_h) cout_h = 1.0e-30

!!write(*,'(10(A,E11.4),A,I)') 'temp: ', temp, ' rho: ', rho, ' ninH: ', nin(iH), ' ninH+: ', nin(iHP) , ' nine-: ', nin(iELEC), ' h0: ', h0term, ' h1: ', h1term, ' cout_h: ', cout_h, ' h0term: ', h0term/(nin(iH)  * nin(iELEC)), ' h1term: ', h1term/(nin(iHP) * nin(iELEC)), ' tindex: ', tindex

!!print *, 'ninH: ', nin(iH), ' ninHP: ', nin(iHP), 'ninE: ', nin(iELEC)
!!print *, 'h0: ', h0term, ' h1: ', h1term
!!print *, 'cout_h: ', cout_h, ' rho: ', rho

return 
end subroutine Cool_hydrogen
