from yt.config import ytcfg
ytcfg["yt","loglevel"] = "50"
from yt.mods import *
import glob
import numpy as np
import matplotlib.pyplot as plt
#import matplotlib as mpl
#mpl.rcParams['

def _hspec(field,data):
    return data['h   ']
add_field("hspec",function=_hspec)

def _hpspec(field,data):
    return data['hp  ']
add_field("hpspec",function=_hpspec)

def _hespec(field,data):
    return data['he  ']
add_field("hespec",function=_hespec)

def _hepspec(field,data):
    return data['hep ']
add_field("hepspec",function=_hepspec)

def _heppspec(field,data):
    return data['hepp']
add_field("heppspec",function=_heppspec)

def _cspec(field,data):
    return data['c   ']
add_field("cspec",function=_cspec)

def _cpspec(field,data):
    return data['cp  ']
add_field("cpspec",function=_cpspec)

def _c2pspec(field,data):
    return data['c2p ']
add_field("c2pspec",function=_c2pspec)

def _c3pspec(field,data):
    return data['c3p ']
add_field("c3pspec",function=_c3pspec)

def _c4pspec(field,data):
    return data['c4p ']
add_field("c4pspec",function=_c4pspec)

def _ospec(field,data):
    return data['o   ']
add_field("ospec",function=_ospec)

def _opspec(field,data):
    return data['op  ']
add_field("opspec",function=_opspec)

def _o2pspec(field,data):
    return data['o2p ']
add_field("o2pspec",function=_o2pspec)

def _o3pspec(field,data):
    return data['o3p ']
add_field("o3pspec",function=_o3pspec)

def _o4pspec(field,data):
    return data['o4p ']
add_field("o4pspec",function=_o4pspec)

def _o5pspec(field,data):
    return data['o5p ']
add_field("o5pspec",function=_o5pspec)

def _o6pspec(field,data):
    return data['o6p ']
add_field("o6pspec",function=_o6pspec)

def _o7pspec(field,data):
    return data['o7p ']
add_field("o7pspec",function=_o7pspec)

def _naspec(field,data):
    return data['na  ']
add_field("naspec",function=_naspec)

def _napspec(field,data):
    return data['nap ']
add_field("napspec",function=_napspec)

def _mgspec(field,data):
    return data['mg  ']
add_field("mgspec",function=_mgspec)

def _mgpspec(field,data):
    return data['mgp ']
add_field("mgpspec",function=_mgpspec)

def _mg2pspec(field,data):
    return data['mg2p']
add_field("mg2pspec",function=_mg2pspec)

def _elecspec(field,data):
    return data['elec']
add_field("elecspec",function=_elecspec)

def plot_it(data):
    fig = plt.figure()
    plt.clf

    plt.subplot(321)
    ax = plt.gca()
    ax.loglog(data[24][:],data[0][:],'r',lw=3,label=r'H')
    ax.loglog(data[24][:],data[1][:],'y',lw=3,label=r'H$^{+}$')
    x1,x2,y1,y2 = ax.axis()
    ax.axis((x1,x2,1.0e-5,1.0))
    ax.legend(prop={'size':6})

    plt.subplot(322)
    ax = plt.gca()
    ax.loglog(data[24][:],data[2][:],'r',lw=3,label=r'He')
    ax.loglog(data[24][:],data[3][:],'y',lw=3,label=r'He$^+$')
    ax.loglog(data[24][:],data[4][:],'g',lw=3,label=r'He$^{2+}$')
    x1,x2,y1,y2 = ax.axis()
    ax.axis((x1,x2,1.0e-5,1.0))
    ax.legend(prop={'size':6})

    plt.subplot(323)
    ax = plt.gca()
    ax.loglog(data[24][:],data[5][:],'r',lw=3,label=r'C')
    ax.loglog(data[24][:],data[6][:],'y',lw=3,label=r'C$^+$')
    ax.loglog(data[24][:],data[7][:],'g',lw=3,label=r'C$^{2+}$')
    ax.loglog(data[24][:],data[8][:],'m',lw=3,label=r'C$^{3+}$')
    ax.loglog(data[24][:],data[9][:],'b',lw=3,label=r'C$^{4+}$')
    x1,x2,y1,y2 = ax.axis()
    ax.axis((x1,x2,1.0e-10,1.0e-2))
    ax.legend(prop={'size':6})

    plt.subplot(324)
    ax = plt.gca()

    ax.loglog(data[24][:],data[10][:],'r',lw=3,label=r'O')
    ax.loglog(data[24][:],data[11][:],'y',lw=3,label=r'O$^{+}$')
    ax.loglog(data[24][:],data[12][:],'g',lw=3,label=r'O$^{2+}$')
    ax.loglog(data[24][:],data[13][:],'m',lw=3,label=r'O$^{3+}$')
    ax.loglog(data[24][:],data[14][:],'b',lw=3,label=r'O$^{4+}$')
    ax.loglog(data[24][:],data[15][:],'r',lw=3,label=r'O$^{5+}$')
    ax.loglog(data[24][:],data[16][:],'y',lw=3,label=r'O$^{6+}$')
    ax.loglog(data[24][:],data[17][:],'g',lw=3,label=r'O$^{7+}$')
    x1,x2,y1,y2 = ax.axis()
    ax.axis((x1,x2,1.0e-10,1.0e-2))
    ax.legend(prop={'size':6})

    plt.subplot(325)
    ax = plt.gca()
    ax.loglog(data[24][:],data[18][:],'r',lw=3,label=r'Na')
    ax.loglog(data[24][:],data[19][:],'y',lw=3,label=r'Na$^{+}$')
    x1,x2,y1,y2 = ax.axis()
    ax.axis((x1,x2,1.0e-15,1.0e-4))
    ax.legend(prop={'size':6})

    plt.subplot(326)
    ax = plt.gca()
    ax.loglog(data[24][:],data[20][:],'r',lw=3,label=r'Mg')
    ax.loglog(data[24][:],data[21][:],'y',lw=3,label=r'Mg$^+$')
    ax.loglog(data[24][:],data[22][:],'g',lw=3,label=r'Mg$^{2+}$')
    x1,x2,y1,y2 = ax.axis()
    ax.axis((x1,x2,1.0e-15,1.0e-2))
    ax.legend(prop={'size':6})

    plt.savefig('chem.eps')
    return 

def main():
    files = glob.glob('Chem_hdf5_chk_0[0-9][0-9][0-9]')
    files.sort()
    nfiles = len(files)
    nspec = 24
    data = np.zeros( (nspec+1,nfiles) )

    for i in range(nfiles):
        print 'WORKING ON FILE: ', files[i]
        pf = load(files[i])
        dd = pf.h.all_data()

        data[0 ][i] = dd.quantities['WeightedAverageQuantity']('hspec','CellVolume')
        data[1 ][i] = dd.quantities['WeightedAverageQuantity']('hpspec','CellVolume')
        data[2 ][i] = dd.quantities['WeightedAverageQuantity']('hespec','CellVolume')
        data[3 ][i] = dd.quantities['WeightedAverageQuantity']('hepspec','CellVolume')
        data[4 ][i] = dd.quantities['WeightedAverageQuantity']('heppspec','CellVolume')
        data[5 ][i] = dd.quantities['WeightedAverageQuantity']('cspec','CellVolume')
        data[6 ][i] = dd.quantities['WeightedAverageQuantity']('cpspec','CellVolume')
        data[7 ][i] = dd.quantities['WeightedAverageQuantity']('c2pspec','CellVolume')
        data[8 ][i] = dd.quantities['WeightedAverageQuantity']('c3pspec','CellVolume')
        data[9 ][i] = dd.quantities['WeightedAverageQuantity']('c4pspec','CellVolume')
        data[10][i] = dd.quantities['WeightedAverageQuantity']('ospec','CellVolume')
        data[11][i] = dd.quantities['WeightedAverageQuantity']('opspec','CellVolume')
        data[12][i] = dd.quantities['WeightedAverageQuantity']('o2pspec','CellVolume')
        data[13][i] = dd.quantities['WeightedAverageQuantity']('o3pspec','CellVolume')
        data[14][i] = dd.quantities['WeightedAverageQuantity']('o4pspec','CellVolume')
        data[15][i] = dd.quantities['WeightedAverageQuantity']('o5pspec','CellVolume')
        data[16][i] = dd.quantities['WeightedAverageQuantity']('o6pspec','CellVolume')
        data[17][i] = dd.quantities['WeightedAverageQuantity']('o7pspec','CellVolume')
        data[18][i] = dd.quantities['WeightedAverageQuantity']('naspec','CellVolume')
        data[19][i] = dd.quantities['WeightedAverageQuantity']('napspec','CellVolume')
        data[20][i] = dd.quantities['WeightedAverageQuantity']('mgspec','CellVolume')
        data[21][i] = dd.quantities['WeightedAverageQuantity']('mgpspec','CellVolume')
        data[22][i] = dd.quantities['WeightedAverageQuantity']('mg2pspec','CellVolume')
        data[23][i] = dd.quantities['WeightedAverageQuantity']('elecspec','CellVolume')
        data[24][i] = pf.current_time

    plot_it(data)


if __name__ == "__main__":
    main()
