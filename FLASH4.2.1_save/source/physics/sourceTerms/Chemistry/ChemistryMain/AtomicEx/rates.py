import numpy as np
import sys
import matplotlib.pyplot as plt

data = np.loadtxt('rates.txt',ndmin=2)

fig = plt.figure() 
plt.clf()
ii=0
for i in range(np.shape(data)[1]-1):
	label = 'Rate: %i'%(i)
	plt.loglog(data[:,-1],data[:,i],label=label)
	if( np.mod(i,5) == 0):
		plt.xlim([10,1.0e8])
		plt.ylim([1.0e-30,1.0e-10])
		plt.legend()
		ln = 'Rates_%s.pdf'%( str(ii).zfill(2) )
		plt.savefig(ln)
		plt.clf()
		ii = ii + 1

plt.xlim([10,1.0e8]) 
plt.ylim([1.0e-30,1.0e-10])
plt.legend()
ln = 'Rates_%s.pdf'%( str(ii).zfill(2) )
plt.savefig(ln)
plt.clf()
