!!****Cool_photo
!!
!!
!!
!! NAME
!!  Cool_photo
!!
!! SYNOPSIS
!!	Calculates the total photoionization heating rate including compton
!!  	Total cooling rate is returned as cout_p
!!
!! ARGUMENTS
!!
!!*****
subroutine Cool_photo(temp,rho,tindex,nin,cout_p)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_p
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: HIterm
real :: HeIterm, HeIIterm
real :: CIterm, CIIterm, CIIIterm, CIVterm, CVterm
real :: NIterm, NIIterm, NIIIterm, NIVterm, NVterm
real :: OIterm, OIIterm, OIIIterm, OIVterm, OVterm, OVIterm, OVIIterm
real :: NeIterm, NeIIterm, NeIIIterm, NeIVterm, NeVterm, NeVIterm, NeVIIterm, NeVIIIterm, NeIXterm, NeXterm
real :: NaIterm
real :: MgIterm, MgIIterm
real :: SiIterm, SiIIterm, SiIIIterm, SiIVterm, SiVterm
real :: FeIterm, FeIIterm, FeIIIterm, FeIVterm
real :: Compterm
real :: evtoerg

evtoerg = 1.60218e-12 !!Converts ev to ergs

cout_p   = 0.0e0
HIterm   = 0.0e0
HeIterm  = 0.0e0
HeIIterm = 0.0e0
CIterm   = 0.0e0
CIIterm  = 0.0e0
CIIIterm = 0.0e0
CIVterm  = 0.0e0
CVterm	 = 0.0e0
OIterm	 = 0.0e0
OIIterm	 = 0.0e0
OIIIterm = 0.0e0
OIVterm	 = 0.0e0
OVterm	 = 0.0e0
OVIterm	 = 0.0e0
OVIIterm = 0.0e0
NeIterm    = 0.0e0
NeIIterm   = 0.0e0
NeIIIterm  = 0.0e0
NeIVterm   = 0.0e0
NeVterm    = 0.0e0
NeVIterm   = 0.0e0
NeVIIterm  = 0.0e0
NeVIIIterm = 0.0e0
NeIXterm   = 0.0e0
NeXterm    = 0.0e0
NaIterm	 = 0.0e0
MgIterm	 = 0.0e0
MgIIterm = 0.0e0
SiIterm	 = 0.0e0
SiIIterm = 0.0e0
SiIIIterm= 0.0e0
SiIVterm = 0.0e0 
SiVterm	 = 0.0e0
FeIterm	 = 0.0e0
FeIIterm = 0.0e0
FeIIIterm= 0.0e0
FeIVterm = 0.0e0
Compterm = 0.0e0

HIterm    = photoh(1)  * nin(iH)
if(HIterm .ne. HIterm) HIterm = 1.0e-30
HeIterm   = photoh(2)  * nin(iHE)
if(HeIterm .ne. HeIterm) HeIterm = 1.0e-30
HeIIterm  = photoh(3)  * nin(iHEP)
if(HeIIterm .ne. HeIIterm) HeIIterm = 1.0e-30
CIterm    = photoh(4)  * nin(iC)
if(CIterm .ne. CIterm) CIterm = 1.0e-30
CIIterm   = photoh(5)  * nin(iCP)
if(CIIterm .ne. CIIterm) CIIterm = 1.0e-30
CIIIterm  = photoh(6)  * nin(iC2P)
if(CIIIterm .ne. CIIIterm) CIIIterm = 1.0e-30
CIVterm   = photoh(7)  * nin(iC3P)
if(CIVterm .ne. CIVterm) CIVterm = 1.0e-30
CVterm    = photoh(8)  * nin(iC4P)
if(CVterm .ne. CVterm) CVterm = 1.0e-30
NIterm    = photoh(9)  * nin(iN)
if(NIterm .ne. NIterm) NIterm = 1.0e-30
NIIterm   = photoh(10) * nin(iNP)
if(NIIterm .ne. NIIterm) NIIterm = 1.0e-30
NIIIterm  = photoh(11) * nin(iN2P)
if(NIIIterm .ne. NIIIterm) NIIIterm = 1.0e-30
NIVterm   = photoh(12) * nin(iN3P)
if(NIVterm .ne. NIVterm) NIVterm = 1.0e-30
NVterm    = photoh(13) * nin(iN4P)
if(NVterm .ne. NVterm) NVterm = 1.0e-30
OIterm    = photoh(14) * nin(iO)
if(OIterm .ne. OIterm) OIterm = 1.0e-30
OIIterm   = photoh(15) * nin(iOP)
if(OIIterm .ne. OIIterm) OIIterm = 1.0e-30
OIIIterm  = photoh(16) * nin(iO2P)
if(OIIIterm .ne. OIIIterm) OIIIterm = 1.0e-30
OIVterm   = photoh(17) * nin(iO3P)
if(OIVterm .ne. OIVterm) OIVterm = 1.0e-30
OVterm    = photoh(18) * nin(iO4P)
if(OVterm .ne. OVterm) OVterm = 1.0e-30
OVIterm   = photoh(19) * nin(iO5P)
if(OVIterm .ne. OVIterm) OVIterm = 1.0e-30
OVIIterm  = photoh(20) * nin(iO6P)
if(OVIIterm .ne. OVIIterm) OVIIterm = 1.0e-30
NeIterm	  = photoh(21) * nin(iNE)
if(NeIterm .ne. NeIterm) NeIterm = 1.0e-30
NeIIterm  = photoh(22) * nin(iNEP)
if(NeIIterm .ne. NeIIterm) NeIIterm = 1.0e-30
NeIIIterm = photoh(23) * nin(iNE2P)
if(NeIIIterm .ne. NeIIIterm) NeIIIterm = 1.0e-30
NeIVterm  = photoh(24) * nin(iNE3P)
if(NeIVterm .ne. NeIVterm) NeIVterm = 1.0e-30
NeVterm	  = photoh(25) * nin(iNE4P)
if(NeVterm .ne. NeVterm) NeVterm = 1.0e-30
NeVIterm  = photoh(26) * nin(iNE5P)
if(NeVIterm .ne. NeVIterm) NeVIterm = 1.0e-30
NeVIIterm = photoh(27) * nin(iNE6P)
if(NeVIIterm .ne. NeVIIterm) HIterm = 1.0e-30
NeVIIIterm= photoh(28) * nin(iNE7P)
if(NeVIIIterm .ne. NeVIIIterm) NeVIIIterm = 1.0e-30
NeIXterm  = photoh(29) * nin(iNE8P)  
if(NeIXterm .ne. NeIXterm) NeIXterm = 1.0e-30
NeXterm	  = photoh(30) * nin(iNE9P)
if(NeXterm .ne. NeXterm) NeXterm = 1.0e-30
NaIterm   = photoh(21) * nin(iNA)
if(NaIterm .ne. NaIterm) NaIterm = 1.0e-30
MgIterm   = photoh(22) * nin(iMG)
if(MgIterm .ne. MgIterm) MgIterm = 1.0e-30
MgIIterm  = photoh(23) * nin(iMGP)
if(MgIIterm .ne. MgIIterm) MgIIterm = 1.0e-30
SiIterm   = photoh(24) * nin(iSI)
if(SiIterm .ne. SiIterm) SiIterm = 1.0e-30
SiIIterm  = photoh(25) * nin(iSIP)
if(SiIIterm .ne. SiIIterm) SiIIterm = 1.0e-30
SiIIIterm = photoh(26) * nin(iSI2P)
if(SiIIIterm .ne. SiIIIterm) HIterm = 1.0e-30
SiIVterm  = photoh(27) * nin(iSI3P)
if(SiIVterm .ne. SiIVterm) SiIVterm = 1.0e-30
SiVterm   = photoh(28) * nin(iSI4P)
if(SiVterm .ne. SiVterm) SiVterm = 1.0e-30
FeIterm   = photoh(29) * nin(iFE)
if(FeIterm .ne. FeIterm) FeIterm = 1.0e-30
FeIIterm  = photoh(30) * nin(iFEP)
if(FeIIterm .ne. FeIIterm) FeIIterm = 1.0e-30
FeIIIterm = photoh(31) * nin(iFE2P)
if(FeIIIterm .ne. FeIIIterm) FeIIIterm = 1.0e-30
FeIVterm  = photoh(32) * nin(iFE3P)
if(FeIVterm .ne. FeIVterm) FeIVterm = 1.0e-30
Compterm  = photoh(33) * nin(iELEC)
if(Compterm .ne. Compterm) Compterm = 1.0e-30

cout_p = (HIterm + HeIterm + HeIIterm + CIterm + CIIterm + CIIIterm + CIVterm + CVterm + NIterm + NIIterm + NIIIterm + NIVterm + NVterm + OIterm + OIIterm + OIIIterm + OIVterm + OVterm + OVIterm + OVIIterm + NeIterm + NeIIterm + NeIIIterm + NeIVterm + NeVterm + NeVIterm + NeVIIterm + NeVIIIterm + NeIXterm + NeXterm + NaIterm + MgIterm + MgIIterm + SiIterm + SiIIterm + SiIIIterm + SiIVterm + SiVterm + FeIterm + FeIIterm + FeIIIterm + FeIVterm + Compterm) * evtoerg * chem_j21 / rho
if(cout_p .ne. cout_p) cout_p = 1.0e-30

return 
end subroutine Cool_photo
