!!****ih* source/physics/sourceTerms/Chemistry/GlobChem/chem_interface
!!
!! SYNOPSIS
!!
!!  use chem_interface
!!
!!  DESCRIPTION
!!
!!  This is the header file for the Chemistry module
!!
!!***

Module chem_interface

#include "Flash.h"
#include "constants.h"

!!
  interface
     subroutine chem_mapNetworkToSpecies(networkIn, specieOut)
       implicit none
       integer, intent(IN)   ::  networkIn
       integer, intent(OUT)  ::  specieOut
     end subroutine chem_mapNetworkToSpecies
  end interface


  !!NEED BURNER HERE
  !!AND ANY OTHER SPECIFICS

  interface
     subroutine chem_burner(tstep,temp,density,xIn,xOut,sdotRate,ei,counts,jcounts,mfrac,tback)
       implicit none
       real, intent(IN)				:: tstep,temp,density
       real, intent(OUT)			:: sdotRate
       real, intent(IN), dimension(NSPECIES)	:: xIn
       real, intent(OUT), dimension(NSPECIES)	:: xOut
       real, intent(INOUT)			:: ei
       real, intent(INOUT)			:: counts,jcounts
       real, intent(IN)				:: mfrac
       real, intent(OUT)			:: tback
     end subroutine chem_burner
  end interface

  interface 
     subroutine chem_azbar()
       implicit none
     end subroutine chem_azbar
  end interface

  interface
     subroutine chem_gift(ab,n1,n2)
       implicit none
       integer, INTENT(IN) :: n1,n2
       real, INTENT(INOUT) :: ab(n1,n2)
     end subroutine chem_gift
  end interface

! going to add my new fuctions for the dense matrix solvers

  interface
     subroutine ludcmp(a,n,np,indx,d)
       implicit none
       integer			:: n,np,indx(*)
       real			:: a(*), d
     end subroutine ludcmp
  end interface

  interface
     subroutine lubksb(a,n,np,indx,b)
       implicit none
       integer			:: n,np,indx(*)
       real			:: a(*), b(*)
     end subroutine lubksb
  end interface

  interface
    subroutine  leqs(a,b,n,np)
      implicit none
      integer			:: n,np
      real			:: a(*),b(*)
    end subroutine leqs
  end interface


  
end Module chem_interface
