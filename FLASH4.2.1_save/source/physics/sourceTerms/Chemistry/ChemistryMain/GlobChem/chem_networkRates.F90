!!****if* source/physics/Chemistry/GlobChem/chem_networkRates.F90
!!
!! NAME
!!
!! chem_networkRates
!!
!! SYNOPSIS
!!
!! chem_networkRates()
!!
!!
!! DESCRIPTION
!! 
!!   routine networkRates generates the raw reaction rates for 
!!  
!!
!!
!!   see the chem_initNetwork to see all the reactions used.
!!   There are a lot. about 85 reactions.
!!
!!***

subroutine chem_networkRates
   
   use Chemistry_data
   use Chemistry_dataEOS
   use chem_data		!!These are copied from bn_networkRates
				

   implicit none

#include "constants.h"
#include "Flash.h"

  integer	i
  real	lt1, lt2, lt3, lt4, lt5, lt6, lt7, lt8, lt9
  real   lnt1, lnt2, lnt3, lnt4, lnt5, lnt6, lnt7, lnt8, lnt9 
  real   te, mp
  real  term, T

!! Above is lt#, which is log10(Temp)^#, lnt# are ln(T)^#

!! zero the rates
  do i=1,nrat
     ratraw(i) = 0.0e0
  enddo
  do i =1,111
     zz(i) = 0.0e0
  enddo

!! Say what lt#'s are

  lt1 = log10(ctemp)
  lt2 = log10(ctemp)**2
  lt3 = log10(ctemp)**3
  lt4 = log10(ctemp)**4
  lt5 = log10(ctemp)**5
  lt6 = log10(ctemp)**6
  lt7 = log10(ctemp)**7
  lt8 = log10(ctemp)**8

!  print *, 'This is lt1: ', lt1
!  print *, 'This is lt2: ', lt2
!  print *, 'This is lt3: ', lt3
!  print *, 'This is lt4: ', lt4
!  print *, 'This is lt5: ', lt5
!  print *, 'This is lt6: ', lt6
!  print *, 'This is lt7: ', lt7
!  print *, 'This is lt8: ', lt8
  
 
!  do i=1,NSPECIES
!     print *, 'aion(',i,')=',aion(i)
!  enddo

  te = ctemp*0.00008617  !This is kb*T, in units of electron-volts
  T = ctemp
  lnt1 = log(te)
  lnt2 = log(te)**2
  lnt3 = log(te)**3
  lnt4 = log(te)**4
  lnt5 = log(te)**5
  lnt6 = log(te)**6 
  lnt7 = log(te)**7
  lnt8 = log(te)**8
  lnt9 = log(te)**9
 
!  print *, 'This is ctemp: ', ctemp
!  print *, 'This is te  : ', te
!  print *, 'This is lnt1: ', lnt1
!  print *, 'This is lnt2: ', lnt2
!  print *, 'This is lnt3: ', lnt3
!  print *, 'This is lnt4: ', lnt4
!  print *, 'This is lnt5: ', lnt5
!  print *, 'This is lnt6: ', lnt6
!  print *, 'This is lnt7: ', lnt7
!  print *, 'This is lnt8: ', lnt8
!  print *, 'This is lnt9: ', lnt9
!  print *, 'This is Den : ', denrate
  
 
  mp = 1.67262e-24 !!Mass of proton (used as nucleon) gms
  mp = 1/(6.02214e23) !!Changing this, want each 'rate' to be multiplied by Na, avogrado's number. easy way to do this

!  print *, 'This is ctemp: ', ctemp

  



!! Time for the rates, this is going to be fun. yay.

!! H + e- --> H- + gamma

   if(ctemp .gt. 6000.0) then
	term =10**(-16.4199 + 0.1998*lt2 - 0.005447*lt4 + 0.000040415*lt6)
   else
        term = 10**(-17.8450 + 0.762*lt1 + 0.1523*lt2 - 0.03274*lt3)

   endif
   ratraw(irhhm) = term * denrate / (mp) 
   zz(1) = ratraw(irhhm)

!! H- + H --> H2 + e
   term = 1.3e-9
 !!  term = 1/(5.4597e8+71239.0*ctemp) !!New rate from Cloudy.
   ratraw(irhmhh2) = term * denrate / (mp)
   zz(2) = ratraw(irhmhh2)

!! H + H+ --> H2+ + gamma
   term = 10**(-19.38 - 1.523*lt1 + 1.118*lt2 - 0.1269*lt3)
   ratraw(irhhph2p) = term * denrate / (mp)
   zz(3) = ratraw(irhhph2p)

!! H + H2+ --> H2 + H+
   term = 6.4e-10
   ratraw(irhh2ph2hp) = term * denrate / (mp)
   zz(4) = ratraw(irhh2ph2hp)

!! H- + H+ --> H + H
   term = 2.4e-6*ctemp**(-0.5)*(1.0+T/20000.0)
   ratraw(irhmhphh) = term * denrate / (mp)
   zz(5) = ratraw(irhmhphh)

!! H2+ + e --> H + H
   if(ctemp .gt. 617) then
     term = 1.32e-6*(ctemp)**(-0.76)
   else
     term = 1.0e-8
   endif
   ratraw(irh2phh) = term * denrate / (mp)
   zz(6) = ratraw(irh2phh)

!!H2 + H+ --> H2+ + H  !!THIS HAS TO HAVE LOG NOT LOG10
  if(ctemp .lt. 110.0) then
    term = 0.0e0;
  else if(ctemp .gt. 3.0e4) then
    term = (-3.3232183e-7 + 3.3735382e-7*log(3.0e4) - 1.4491368e-7*log(3.0e4)**2 &
          + 3.4172805e-8*log(3.0e4)**3- 4.7813720e-9*log(3.0e4)**4 + 3.9731542e-10*log(3.0e4)**5 &
	   -1.8171411e-11*log(3.0e4)**6 + 3.5311932e-13*log(3.0e4)**7)*exp(-21237.15/3.0e4)
  else
    term = (-3.3232183e-7 + 3.3735382e-7*log(ctemp) - 1.4491368e-7*log(ctemp)**2 &
          + 3.4172805e-8*log(ctemp)**3- 4.7813720e-9*log(ctemp)**4 + 3.9731542e-10*log(ctemp)**5 &
	   -1.8171411e-11*log(ctemp)**6 + 3.5311932e-13*log(ctemp)**7)*exp(-21237.15/ctemp)
  endif
  ratraw(irh2hph2ph) = term * denrate / (mp)
  zz(7) = ratraw(irh2hph2ph)

!!H2 + e --> H + H + e-
  if(ctemp .lt. 1100.0) then  !!LTE = 4050, v = 1100
    term = 0.0e0
  else
!!    term = 1.91e-9*(ctemp)**(0.136)*exp(-53407.1/ctemp) !!This is the LTE version
  term = 4.49e-8*(ctemp)**(0.11)*exp(-101858.0/ctemp)  !!This is the v=0 version
  endif
  ratraw(irh2hh) = term * denrate / (mp)
  zz(8) = ratraw(irh2hh)

!!H2 + H --> H + H + H
  if(ctemp .lt. 700.0) then  !!LTE = 500, V = 700
    term = 0.0e0
  else
!!    term = 3.52e-9*exp(-43900.0/ctemp) !!Again LTE, is this right?
    term = 6.67e-12*ctemp**(0.5)*exp(-(1+63593.0/ctemp)) !!v=0 version
  endif
  ratraw(irh2hhhh) = term * denrate / (mp )
  zz(9) = ratraw(irh2hhhh)

!!H2 + H2 --> H2 + H + H
  if(ctemp .lt. 750.0) then !!LTE = 600, V = 750
    term = 0.0e0
  else
!!    term = 1.3e-9*exp(-53300.0/ctemp) 
    term = 5.996e-30*ctemp**(4.1881)*exp(-54657.4/ctemp)/(1.0+6.761e-6*ctemp)**(5.6881) !!v=0 version
  endif
  ratraw(irh2h2h2hh) = term * denrate / (mp)
  zz(10) = ratraw(irh2h2h2hh)
 
!!H2 + He --> H + H + He
  if(ctemp .lt. 900.0) then !!LTE = 600, V = 900
    term = 0.0e0
  else  
!!    term = 10**(-2.729-1.75*lt1-23474.0/ctemp) !!LTE
    term = 10**(-27.029+3.801*lt1-29487.0/ctemp) !!v=0
  endif
  ratraw(irh2hehhhe) = term * denrate / (mp)
  zz(11) = ratraw(irh2hehhhe)

!!H + e- --> H+ + e- + e-
  if(ctemp .lt. 2800.0) then
    term = 0.0e0
  else
    term = exp(-3.271396786e1 + 1.35365560e1*lnt1 - 5.73932875e0*lnt2 &
           +1.56315498e0*lnt3 - 2.87705600e-1*lnt4 + 3.48255977e-2*lnt5 &
           -2.63197617e-3*lnt6 + 1.11954395e-4*lnt7 -2.03914985e-6*lnt8)
  endif
  ratraw(irhhp) = term * denrate / (mp)
  zz(12) = ratraw(irhhp)
  
!!  print *, 'Term is: ', term
!!H+ + e- --> H + gamma
!!   term = 1.269e-13*(315614.0/ctemp)**(1.503)*(1.0+(604625.0/ctemp)**0.470)**(-1.923) !! Trying case A

  term = 2.753e-14*(315614.0/ctemp)**(1.500)*(1.0+(115188.0/ctemp)**0.407)**(-2.242)  !! This is case B
  ratraw(irhph) = term * denrate / (mp)
  zz(13) = ratraw(irhph)

!!H- + e- --> H + e- + e-
  if(ctemp .lt. 100.0) then
    term = 0.0e0
  else
    term =exp(-1.801849334e1 + 2.36085220*lnt1 &
	- 2.82744300e-1*lnt2 + 1.62331664e-2*lnt3 &
        -3.36501203e-2*lnt4 + 1.17832978e-2*lnt5 - 1.65619470e-3*lnt6 &
        +1.06827520e-4*lnt7 - 2.63128581e-6*lnt8)
  endif
  ratraw(irhmh) = term *denrate / (mp)
  zz(14) = ratraw(irhmh)

!!H- + H --> H + H + e-
  if(te .gt. 0.1) then
    term = exp(-2.0372609e1 + 1.13944933e0*lnt1 - 1.4210135e-1*lnt2 &
           +8.4644554e-3*lnt3 - 1.4327641e-3*lnt4 + 2.0122503e-4*lnt5 &
           +8.6639632e-5*lnt6 - 2.5850097e-5*lnt7 + 2.4555012e-6*lnt8 &
           -8.0683825e-8*lnt9)
  else
    term = 2.5634e-9*te**(1.78186)
  endif
  ratraw(irhmhhh) = term * denrate / (mp)
  zz(15) = ratraw(irhmhhh)


!!H+ + H- --> H2+ + e-
  if(ctemp .gt. 8000.0) then
    term = 9.6e-7*(ctemp)**(-0.90)
  else
    term = 6.9e-9*(ctemp)**(-0.35)
  endif
  ratraw(irhphmh2p) = term * denrate / (mp)
  zz(16) = ratraw(irhphmh2p)
 
!!He + e- --> He+ + e- + e-
  if(ctemp .lt. 2800.0) then
    term = 0.0e0
  else
    term = exp(-4.409864886e1 + 2.391596563e1*lnt1 - 1.07532302e1*lnt2 &
         +3.05803875e0*lnt3 - 5.68511890e-1*lnt4 + 6.79539123e-2*lnt5 &
         -5.00905610e-3*lnt6 + 2.06723616e-4*lnt7 - 3.64916141e-6*lnt8)
  endif
  ratraw(irhehep) = term * denrate / (mp)
  zz(17) = ratraw(irhehep)

!!He+ + e- --> He++ + e- + e-
  if(ctemp .lt. 5500.0) then
    term = 0.0e0
  else
     term = exp(-6.87104099e1 + 4.393347633e1*lnt1 - 1.84806699e1*lnt2 &
         +4.70162649e0*lnt3 - 7.6924663e-1*lnt4 + 8.113042e-2*lnt5  &
         -5.32402063e-3*lnt6 + 1.97570531e-4*lnt7 - 3.16558106e-6*lnt8)
  endif
  ratraw(irhephepp) = term * denrate / (mp)
  zz(18) = ratraw(irhephepp)

!!He+ + e- --> He + gamma
   term = 0.32*(1.0e-11*(ctemp)**(-0.5)*(11.19 - 1.676*lt1 - 0.2852*lt2 + 0.04433*lt3)) !! This is case B
   term = term + 0.68*(1.0e-11*(ctemp)**(-0.5)*(12.72 - 1.615*lt1 - 0.3162*lt2 + 0.0493*lt3)) !! This is case A,
   term = term + 1.9e-3*ctemp**(-1.50)*exp(-473421.0/ctemp)*(1.0+0.3*exp(-94684/ctemp)) 
  

!!Trying CASE A. But according to the optical depth calc earlier, case B is the one to use. 
!!Using the formuation from eq.18 in the paper.
  ratraw(irhephe) = term * denrate / (mp)
  zz(19) = ratraw(irhephe)

!!He++ + e- --> He+ + gamma  again this case B
    term = 5.506e-14*(1262456.0/ctemp)**1.500*(1.0+(460752.0/ctemp)**0.407)**(-2.242) !! CASE B
!!  term = 2.538e-13*(1262456.0/ctemp)**1.503*(1.0+(2418500.0/ctemp)**0.407)**(-1.923) !! CASE A
!!Again case b should be the correct one. trying case a for cloudy.
  ratraw(irhepphep) = term * denrate / (mp)
  zz(20) = ratraw(irhepphep)

!!H- + H2+ --> H2 + H
  term = 1.4e-7*(ctemp/300.0)**(-0.5)
  ratraw(irhmh2ph2h) = term * denrate / (mp)
  zz(21) = ratraw(irhmh2ph2h)

!!H- + H2+ --> H + H + H
  term = 1.4e-7*(ctemp/300.0)**(-0.5)
  ratraw(irhmh2phhh) = term * denrate / (mp)
  zz(22) = ratraw(irhmh2phhh)

!!H2 + e- --> H- + H
  if(ctemp .lt. 500.0) then
    term = 0.0e0
  else
    term = 2.7e-8*(ctemp)**(-1.27)*exp(-43000.0/ctemp)
  endif
  ratraw(irh2hmh) = term * denrate / (mp)
  zz(23) = ratraw(irh2hmh)

!!H2 + He+ --> He + H + H+
  term = 3.7e-14*exp(35.0/ctemp)
  ratraw(irh2hephehhp) = term * denrate / (mp)
  zz(24) = ratraw(irh2hephehhp)

!!H2 + He+ --> H2+ He
  term = 7.2e-15
  ratraw(irh2heph2phe) = term * denrate / (mp)
  zz(25) = ratraw(irh2heph2phe)

!!He+ + H --> He + H+
  term = 1.2e-15 * (ctemp/300.0)**(0.25)
  ratraw(irhephhehp) = term * denrate / (mp)
  zz(26) = ratraw(irhephhehp)

!!He + H+ --> He+ + H
  if(ctemp .gt. 10000.0) then
   term =  4.0e-37*ctemp**4.74
  else if(ctemp .lt. 1500.0) then
   term = 0.0e0
  else
   term = 1.26e-9*ctemp**(-0.75)*exp(-127500.0/ctemp)
  endif
  ratraw(irhehpheph) = term * denrate / (mp)
  zz(27) = ratraw(irhehpheph)

!!He+ + H- --> He + H
  term = 2.32e-7*(ctemp/300.0)**(-0.52)*exp(ctemp/22400.0)
  ratraw(irhephmheh) = term * denrate / (mp) 
  zz(28) = ratraw(irhephmheh)

!!He + H- --> He + H + e-
  if(ctemp .lt. 240.0) then
    term = 0.0e0
  else
    term = 4.1e-17*ctemp**2.0*exp(-19870.0/ctemp)
  endif
  ratraw(irhehmheh) = term * denrate / (mp) 
  zz(29) = ratraw(irhehmheh)

!!D+ + e- --> D + gamma
  ratraw(irdpd) = ratraw(irhph)
  zz(33) = ratraw(irdpd)

!!D + H+ --> H + D+
  if( ctemp .gt. 200000.0) then
    term = 3.44e-10*ctemp**(0.35)
  else
    term = 2.0e-10*ctemp**(0.402)*exp(-37.1/ctemp) - 3.31e-17*ctemp**(1.48)
  endif
  ratraw(irdhphdp) = term * denrate / (mp)
  zz(34) = ratraw(irdhphdp)

!!H + D+ --> D + H+
  term = 2.06e-10*ctemp**0.396*exp(-33.0/ctemp) + 2.03e-9*ctemp**(-0.332)
  ratraw(irhdpdhp) = term * denrate / (mp)
  zz(35) = ratraw(irhdpdhp)

!!H + D --> HD + gamma
  if(ctemp .gt. 200.0) then
    term = 1.0e-25*exp(507.207-370.889*log(ctemp) + 104.854*log(ctemp)**2.0 &
           -14.4192*log(ctemp)**3.0 + 0.971469*log(ctemp)**4.0 - &
            0.0258076*log(ctemp)**5.0)
  elseif (ctemp .lt. 10.0) then
    term = 0.0
  else
    term = 1.0e-25*exp(2.80202 - 6.63697*log(ctemp) + 4.75619*log(ctemp)**2.0 &
           -1.39325*log(ctemp)**3.0 + 0.178259*log(ctemp)**4.0 &
           -0.00817097*log(ctemp)**5.0)
  endif
!!  print *, 'Term is: ', term
  ratraw(irhdhd) = term * denrate / (mp)
  zz(36) = ratraw(irhdhd)

!!H2 + D --> HD + H
  if(ctemp .gt. 2000.0) then
    term = 3.17e-10*exp(-5207.0/ctemp)
  else
    term = 10**(-56.4737 + 5.88886*lt1 + 7.19692*lt2 + 2.25069*lt3 &
           -2.16903*lt4 +0.317887*lt5)
  endif
  ratraw(irh2dhdh) = term * denrate / (mp)
  zz(37) = ratraw(irh2dhdh)

!!HD+ + H --> HD + H+
  ratraw(irhdphhdhp) = ratraw(irhh2ph2hp)
  zz(38) = ratraw(irhdphhdhp)

!!H2 + D+ --> HD + H+
  term = 1.0e-9*(0.417 + 0.846*lt1 - 0.137*lt2)
  ratraw(irh2dphdhp) = term * denrate / (mp)
  zz(39) = ratraw(irh2dphdhp)

!!HD + H --> H2 + D
  if(ctemp .gt. 200.0) then 
    term = 5.25e-11*exp(-4430.0/ctemp + 173900.0/ctemp**2.0)
  else if(ctemp .lt. 50.0) then
    term = 0.0e0
  else
    term = 5.25e-11*exp(-4430.0/ctemp)
  endif
  ratraw(irhdhh2d) = term * denrate / (mp)
  zz(40) = ratraw(irhdhh2d)

!!HD + H+ --> H2 + D+
  term = 1.1e-9*exp(-488.0/ctemp)
  ratraw(irhdhph2dp) = term * denrate / (mp)
  zz(41) = ratraw(irhdhph2dp)

!!D + H+ --> HD+ + gamma
  term = 3.9e-19*(ctemp/300.0)**(1.8)*exp(20.0/ctemp)
  ratraw(irdhpdhp) = term * denrate / (mp) 
  zz(42) = ratraw(irdhpdhp)

!!H + D+ --> HD+ + gamma
  ratraw(irhdphdp) = ratraw(irdhpdhp) !!same as above, so this is ok
  zz(43) = ratraw(irhdphdp)

!!HD+ + e- --> H + D
  term = 7.2e-8*ctemp**(-0.5)
  ratraw(irhdphd) = term * denrate / (mp)
  zz(44) = ratraw(irhdphd)

!!D + e- --> D+ + e- + e-
  ratraw(irddp) = ratraw(irhhp)
  zz(45) = ratraw(irddp)

!!He+ + D --> D+ + He
  term = 1.1e-15*(ctemp/300.0)**(0.25)
  ratraw(irhepddphe) = term * denrate / (mp)
  zz(46) = ratraw(irhepddphe)

!!He + D+ --> D + He+
  if(ctemp .gt. 10000.0) then
    term = 5.9e-37*ctemp**(4.74)
  else if(ctemp .lt. 1500.0) then
    term = 0.0e0
  else
    term = 1.85e-9*ctemp**(-0.75)*exp(-127500.0/ctemp)
  endif
  ratraw(irhedpdhep) = term * denrate / (mp)
  zz(47) = ratraw(irhedpdhep)

!!H2+ + D --> HD+ + H
  term = 1.07e-9*(ctemp/300.0)**(0.062)*exp(-ctemp/41400.0)
  ratraw(irh2pdhdph) = term * denrate / (mp)
  zz(48) = ratraw(irh2pdhdph)

!!HD+ + D --> HD + D+
  ratraw(irhdpdhddp) = ratraw(irhh2ph2hp)
  zz(49) = ratraw(irhdpdhddp)

!!HD+ + H --> H2+ + D
  term = 1.0e-9*exp(-154.0/ctemp)
  ratraw(irhdphh2pd) = term * denrate / (mp)
  zz(50) = ratraw(irhdphh2pd)

!!D + e- --> D- + gamma
  ratraw(irddm) = ratraw(irhhm) 
  zz(51) = ratraw(irddm)

!!H + D- --> D + H-
  term = 6.4e-9*(ctemp/300.0)**(0.41)
  ratraw(irhdmdhm) = term * denrate / (mp)
  zz(52) = ratraw(irhdmdhm)

!!D + H- --> H + D-
  ratraw(irdhmhdm) = ratraw(irhdmdhm) !!Same as above, we are ok
  zz(53) = ratraw(irdhmhdm)

!!D + H- --> HD + e-
  ratraw(irdhmhd) = 0.5*ratraw(irhmhh2) 
  zz(54) = ratraw(irdhmhd)

!!H + D- --> HD + e-
  ratraw(irhdmhd) = 0.5*ratraw(irhmhh2) 
  zz(55) = ratraw(irhdmhd)

!!HD + e- --> H + D-
  if(ctemp .lt. 500.0) then
    term = 0.0e0
  else
    term = 1.35e-9*ctemp**(-1.27)*exp(-43000.0/ctemp)
  endif
  ratraw(irhdhdm) = term * denrate / (mp)
  zz(57) = ratraw(irhdhdm)

!!HD + e- --> D + H-
  ratraw(irhddhm) = ratraw(irhdhdm) !! same as above, we are ok
  zz(58) = ratraw(irhddhm)

!!H+ + D- --> HD+ + e-
  term = 1.1e-9*(ctemp/300.0)**(-0.4)
  ratraw(irhpdmhdp) = term * denrate / (mp)
  zz(60) = ratraw(irhpdmhdp)

!!D+ + H- --> HD+ + e-
  ratraw(irdphmdhp) = ratraw(irhpdmhdp)
  zz(61) = ratraw(irdphmdhp)

!!D- + e- --> D + e- + e-
  ratraw(irdmd) = ratraw(irhmh)
  zz(63) = ratraw(irdmd)

!!D- + H --> D + H + e-
  ratraw(irdmhdh) = ratraw(irhmhhh) 
  zz(64) = ratraw(irdmhdh)

!!D- + He --> D + He + e-
  if(ctemp .lt. 250.0)then
    term = 0.0e0
  else
    term = 1.5e-17*ctemp**2.0*exp(-19870.0/ctemp)
  endif
  ratraw(irdmhedhe) = term * denrate / (mp)
  zz(65) = ratraw(irdmhedhe)

!!D+ + H- --> D + H
  ratraw(irdphmdh) = ratraw(irhmhphh) 
  zz(66) = ratraw(irdphmdh)

!!H+ + D- --> D + H
  ratraw(irhpdmdh) = ratraw(irhmhphh) 
  zz(67) = ratraw(irhpdmdh)
 
!!D+ + D- --> D + D
  ratraw(irdpdmdd) = ratraw(irhmhphh)
  zz(68) = ratraw(irdpdmdd)

!!H2+ + D- --> H2 + D
  term = 1.70e-7*(ctemp/300.0)**(-0.5)
  ratraw(irh2pdmh2d) = term * denrate / (mp)
  zz(69) = ratraw(irh2pdmh2d)

!!H2+ + D- --> H + H + D
  ratraw(irh2pdmhhd) = ratraw(irh2pdmh2d) !!Same as above, we are ok
  zz(70) = ratraw(irh2pdmhhd)

!!HD+ + H- --> HD + H
  term = 1.5e-7*(ctemp/300.0)**(-0.5)
  ratraw(irhdphmhdh) = term * denrate / (mp)
  zz(71) = ratraw(irhdphmhdh)

!!HD+ + H- --> D + H + H
  ratraw(irhdphmdhh) = ratraw(irhdphmhdh) !!Same as above
  zz(72) = ratraw(irhdphmdhh)

!!HD+ + D- --> HD + D
  term = 1.9e-7*(ctemp/300.0)**(-0.5)
  ratraw(irhdpdmhdd) = term * denrate / (mp)
  zz(73) = ratraw(irhdpdmhdd)

!!HD+ + D- --> D + H + D
  term = 1.9e-7*(ctemp/300.0)**(-0.5)
  ratraw(irhdpdmdhd) = term * denrate / (mp)
  zz(74) = ratraw(irhdpdmdhd)

!!He+ + D- --> He + D
  term = 3.03e-7*(ctemp/300.0)**(-0.52)*exp(ctemp/22400.0)
  ratraw(irhepdmhed) = term * denrate / (mp)
  zz(79) = ratraw(irhepdmhed)

!!D + H2+ --> H2 + D+
  ratraw(irdh2ph2dp) = ratraw(irhh2ph2hp) 
  zz(81) = ratraw(irdh2ph2dp)

!!H2+ + D --> HD + H+
  ratraw(irh2pdhdhp) = 1.0e-9 * denrate/ (mp)
  zz(82) = ratraw(irh2pdhdhp)

!!HD+ + H --> H2 + D+
  ratraw(irhdphh2dp) = 1.0e-9 * denrate / (mp)
  zz(83) = ratraw(irhdphh2dp)
  
!!H2 + D+ --> H2+ + D
  ratraw(irh2dph2pd) = ratraw(irh2hph2ph)
  zz(90) = ratraw(irh2dph2pd)

!!H2 + D+ --> HD+ + H
  if(ctemp .lt. 230.0) then
    term = 0.0e0
  else
    term = (1.04e-9 + 9.52e-9*(ctemp/10000.0) - 1.81e-9*(ctemp/10000.0)**2)*exp(-21000.0/ctemp)
  endif
  ratraw(irh2dphdph) = term * denrate / (mp)
  zz(91) = ratraw(irh2dphdph)

!!HD + H+ --> HD+ + H
  ratraw(irhdhphdph) = ratraw(irh2hph2ph)
  zz(92) = ratraw(irhdhphdph)

!!HD + H+ --> H2P + D
  if(ctemp .lt. 230.0) then
    term = 0.0e0
  else
    term = 1.0e-9*exp(-21600.0/ctemp)
  endif
  ratraw(irhdhph2pd) =  term * denrate / (mp)
  zz(93) = ratraw(irhdhph2pd)

!!HD + D+ --> HD+ + D
  ratraw(irhddphdpd) = ratraw(irh2hph2ph)
  zz(94) = ratraw(irhddphdpd)

!!HD + He+ --> HD+ + He
  ratraw(irhdhephdphe) = ratraw(irh2heph2phe)
  zz(101) = ratraw(irhdhephdphe)

!!HD + He+ --> He + H+ + D
  term = 1.85e-14*exp(35.0/ctemp)
  ratraw(irhdhephehpd) = term * denrate / (mp)
  zz(102) = ratraw(irhdhephehpd)

!!HD + He+ --> He + H + D+
  term = 1.85e-14*exp(35.0/ctemp)
  ratraw(irhdhephehdp) = term * denrate / (mp)
  zz(103) = ratraw(irhdhephehdp)

!!HD + H --> H + D + H, authors say this is like #8, im using LTE
  ratraw(irhdhhdh) = ratraw(irh2hh) 
  zz(108) = ratraw(irhdhhdh)

!!HD + H2 --> H + D + H2, authors say this is like *9,  LTE
  ratraw(irhdh2hdh2) = ratraw(irh2hhhh)
  zz(109) = ratraw(irhdh2hdh2)

!!HD + He --> H + D +He. authors say this is like #10, LTE
  ratraw(irhdhehdhe) = ratraw(irh2h2h2hh) 
  zz(110) = ratraw(irhdhehdhe)

!!HD + e- --> H + D + e-, using LTE
  if(ctemp .lt. 1000.0) then !! LTE = 600, V = 1000
    term = 0.0e0
  else
!!    term = 1.04e-9*ctemp**(0.218)*exp(-53070.7/ctemp)  !!LTE
    term = 5.09e-9*ctemp**(0.128)*exp(-103258.0/ctemp) !! v=0
  endif
  ratraw(irhdhde) = term * denrate / (mp)
  zz(111) = ratraw(irhdhde)

!!H- + gamma --> H + e-
  term = 1.36e-11*j21
  ratraw(irbp1) = term 

!!D- + gamma --> D + e-
  term = 1.36e-11*j21
  ratraw(irbp2) = term

!!H2+ + gamma --> H + H+
  term = 4.11e-12*j21
  ratraw(irbp3) = term

!!HD+ + gamma --> H + D+
  term = 2.05e-12*j21
  ratraw(irbp4) = term

!!HD+ + gamma --> H+ + D
  term = 2.0e-12*j21
  ratraw(irbp5) = term

!!H2 + gamma --> H + H
  term = 1.3e-12*j21*fshh2
  ratraw(irbp7) = term

!!HD + gamma --> H + D
  term = 1.45e-12*j21*fshhd
  ratraw(irbp8) = term

!!  print *, 'This is temp: ' , ctemp
!!  do i = 1,nrat
!!   print *, 'ratraw(',i,')=', ratraw(i)
!!  enddo
 
!!  stop


!!Try to look at rates
!!  open(10,file="rates", access='append')
!!  print *, 'In chem_networkRates'
!!  print *, 'ctemp: ', ctemp, '  denrate: ', denrate  

!!  do i = 1,111
!!     write(10,"(e13.5)",advance="no") zz(i) 
!!  enddo
 
!!     write(10,"(e8.3)", advance="no") zz
!!     write(10,"(A)", advance="no") ' '
!!     write(10,"(f10.3)",advance="no") ctemp
!!    write(10,"(A)") ' '
!!  close(10)




  return

  end subroutine chem_networkRates
