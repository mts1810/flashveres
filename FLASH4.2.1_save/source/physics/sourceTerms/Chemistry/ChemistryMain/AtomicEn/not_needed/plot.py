import numpy as np
import matplotlib.pyplot as plt
from pylab import *
rc('axes',linewidth=2)
from matplotlib.ticker import FuncFormatter

def mjrform(x,pos):
    return "{0}".format(int(np.log10(x)))

fd = np.loadtxt('flash_cie.txt')
id = np.loadtxt('ionization_state_comb.dat',skiprows=1)

ymin = 5.0e-6
ymax = 9.0
xmin = 5.0e3
xmax = 2.0e7

##Plot Hydrogen First
fig = plt.figure() #(figsize=(16.0,12.0))
plt.clf()
plt.subplot(431)
ax = plt.gca()
ax.loglog(id[:,0],id[:,1],c='red' ,lw=3,ls='-' ,label='HI')
ax.loglog(id[:,0],id[:,2],c='blue',lw=3,ls='--',label='HII')
ax.loglog(fd[:,0],fd[:,1],c='red' ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,2],c='blue',ls='None',marker='*',ms=10,lw=3)

ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
ax.text(0.5,0.90,r'F$_{\rm {H}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.subplots_adjust(hspace=0,wspace=0)
plt.setp(ax.get_xticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')

##Helium
plt.subplot(432)
ax = plt.gca()
ax.loglog(id[:,0],id[:,3],c='red'  ,lw=3,ls='-' ,label='HeI')
ax.loglog(id[:,0],id[:,4],c='blue' ,lw=3,ls='--',label='HeII')
ax.loglog(id[:,0],id[:,5],c='green',lw=3,ls='-.' ,label='HeIII')
ax.loglog(fd[:,0],fd[:,3],c='red'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,4],c='blue' ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,5],c='green',ls='None',marker='*',ms=10,lw=3)

ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
#plt.ylabel(r'F$_{\rm {He}}$',fontsize=12,fontweight='bold',rotation='horizontal')
ax.text(0.5,0.90,r'F$_{\rm {He}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.subplots_adjust(hspace=0,wspace=0)
plt.setp(ax.get_xticklabels(),visible=False)
plt.setp(ax.get_yticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
 
##Carbon
plt.subplot(433)
ax = plt.gca()
ax.loglog(id[:,0],id[:,6] ,c='red'    ,lw=3,ls='-'  ,label='CI')
ax.loglog(id[:,0],id[:,7] ,c='blue'   ,lw=3,ls='--' ,label='CII')
ax.loglog(id[:,0],id[:,8] ,c='green'  ,lw=3,ls='-.' ,label='CIII')
ax.loglog(id[:,0],id[:,9] ,c='magenta',lw=3,ls=':'  ,label='CIV')
ax.loglog(id[:,0],id[:,10] ,c='navy',lw=3,ls=':'  ,label='CV')
ax.loglog(id[:,0],id[:,11],c='darkred'   ,lw=3,ls='--',dashes=(20,10),label='CVI')
ax.loglog(fd[:,0],fd[:,6] ,c='red'    ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,7] ,c='blue'   ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,8] ,c='green'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,9] ,c='magenta',ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,10],c='navy'   ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,11],c='darkred'   ,ls='None',marker='*',ms=10,lw=3)
ax.set_xlim([xmin,xmax])
ax.set_ylim([ymin,ymax])
ax.text(0.5,0.90,r'F$_{\rm {C}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.subplots_adjust(hspace=0,wspace=0)
plt.setp(ax.get_xticklabels(),visible=False)
plt.setp(ax.get_yticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')


##Nitrogen
plt.subplot(434)
ax = plt.gca()
ax.loglog(id[:,0],id[:,12],c='red'    ,lw=3,ls='-'  ,label='NI')
ax.loglog(id[:,0],id[:,13],c='blue'   ,lw=3,ls='--' ,label='NII')
ax.loglog(id[:,0],id[:,14],c='green'  ,lw=3,ls='-.' ,label='NIII')
ax.loglog(id[:,0],id[:,15],c='magenta',lw=3,ls='-'  ,label='NIV')
ax.loglog(id[:,0],id[:,16],c='navy'   ,lw=3,ls='--' ,label='NV')
ax.loglog(id[:,0],id[:,17],c='darkred',lw=3,ls='-.' ,label='NVI')

ax.loglog(fd[:,0],fd[:,35],c='red'    ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,36],c='blue'   ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,37],c='green'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,38],c='magenta',ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,39],c='navy'   ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,40],c='darkred',ls='None',marker='*',ms=10,lw=3)
ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
ax.xaxis.set_major_formatter(FuncFormatter(mjrform))
plt.xlabel(r'Log$_{10}$(T) [K]')
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
ax.text(0.5,0.90,r'F$_{\rm {N}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.subplots_adjust(hspace=0,wspace=0)
#plt.setp(ax.get_xticklabels(),visible=False)
#plt.setp(ax.get_yticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')

##Oxygen
plt.subplot(435)
ax = plt.gca()
ax.loglog(id[:,0],id[:,18],c='red'      ,lw=3,ls='-'  ,label='OI')
ax.loglog(id[:,0],id[:,19],c='blue'     ,lw=3,ls='--' ,label='OII')
ax.loglog(id[:,0],id[:,20],c='green'    ,lw=3,ls='-.' ,label='OIII')
ax.loglog(id[:,0],id[:,21],c='magenta'  ,lw=3,ls=':'  ,label='OIV')
ax.loglog(id[:,0],id[:,22],c='navy'     ,lw=3,ls='-'  ,label='OV')
ax.loglog(id[:,0],id[:,23],c='darkred'  ,lw=3,ls='--' ,label='OVI')
ax.loglog(id[:,0],id[:,24],c='darkviolet' ,lw=3,ls='-.' ,label='OVII')
ax.loglog(id[:,0],id[:,25],c='darkgreen',lw=3,ls=':'  ,label='OVIII')
ax.loglog(fd[:,0],fd[:,12],c='red'      ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,13],c='blue'     ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,14],c='green'    ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,15],c='magenta'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,16],c='navy'     ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,17],c='darkred'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,18],c='darkviolet' ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,19],c='darkgreen',ls='None',marker='*',ms=10,lw=3)
ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
ax.set_xlim([xmin,xmax])
ax.set_ylim([ymin,ymax])
ax.text(0.5,0.90,r'F$_{\rm {O}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.setp(ax.get_xticklabels(),visible=False)
plt.setp(ax.get_yticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')

##Neon
plt.subplot(436)
ax = plt.gca()
ax.loglog(id[:,0],id[:,26],c='red'       ,lw=3,ls='-'  ,label='NeI')
ax.loglog(id[:,0],id[:,27],c='blue'      ,lw=3,ls='--' ,label='NeII')
ax.loglog(id[:,0],id[:,28],c='green'     ,lw=3,ls='-.' ,label='NeIII')
ax.loglog(id[:,0],id[:,29],c='magenta'   ,lw=3,ls=':'  ,label='NeIV')
ax.loglog(id[:,0],id[:,30],c='navy'      ,lw=3,ls='-'  ,label='NeV')
ax.loglog(id[:,0],id[:,31],c='darkred'   ,lw=3,ls='--' ,label='NeVI')
ax.loglog(id[:,0],id[:,32],c='darkviolet',lw=3,ls='-.' ,label='NeVII')
ax.loglog(id[:,0],id[:,33],c='darkgreen' ,lw=3,ls=':'  ,label='NeVIII')
ax.loglog(id[:,0],id[:,34],c='darkblue'  ,lw=3,ls='-.' ,label='NeIX')
ax.loglog(id[:,0],id[:,35],c='darkorange',lw=3,ls=':'  ,label='NeX')


ax.loglog(fd[:,0],fd[:,20],c='red'      ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,21],c='blue'     ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,22],c='green'    ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,23],c='magenta'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,24],c='navy'     ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,25],c='darkred'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,26],c='darkviolet' ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,27],c='darkgreen',ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,28],c='darkblue' ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,29],c='darkorange',ls='None',marker='*',ms=10,lw=3)

ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
ax.set_xlim([xmin,xmax])
ax.set_ylim([ymin,ymax])
ax.text(0.5,0.90,r'F$_{\rm {Ne}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.setp(ax.get_xticklabels(),visible=False)
plt.setp(ax.get_yticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')

##Sodium
plt.subplot(437)
ax = plt.gca()
ax.loglog(id[:,0],id[:,36],c='red' ,lw=3,ls='-'  ,label='NaI')
ax.loglog(id[:,0],id[:,37],c='blue',lw=3,ls='--' ,label='NaII')
ax.loglog(fd[:,0],fd[:,30],c='red' ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,31],c='blue',ls='None',marker='*',ms=10,lw=3)
ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
ax.xaxis.set_major_formatter(FuncFormatter(mjrform))
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
ax.text(0.5,0.90,r'F$_{\rm {Na}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.setp(ax.get_xticklabels(),visible=False)
#plt.setp(ax.get_yticklabels(),visible=False)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')


##Magnesium
plt.subplot(438)
ax = plt.gca()
ax.loglog(id[:,0],id[:,38],c='red'  ,lw=3,ls='-'  ,label='MgI')
ax.loglog(id[:,0],id[:,39],c='blue' ,lw=3,ls='--' ,label='MgII')
ax.loglog(id[:,0],id[:,40],c='green',lw=3,ls='-.' ,label='MgIII')
ax.loglog(fd[:,0],fd[:,32],c='red'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,33],c='blue' ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,34],c='green',ls='None',marker='*',ms=10,lw=3)
ax.text(0.5,0.90,r'F$_{\rm {Mg}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
ax.xaxis.set_major_formatter(FuncFormatter(mjrform))
plt.xlabel(r'Log$_{10}$(T) [K]')
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
plt.subplots_adjust(hspace=0,wspace=0)
#plt.setp(ax.get_xticklabels(),visible=False)
plt.setp(ax.get_yticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')

##Silicon
plt.subplot(439)
ax = plt.gca()
ax.loglog(id[:,0],id[:,41],c='red'  ,lw=3,ls='-'  ,label='SII')
ax.loglog(id[:,0],id[:,42],c='blue' ,lw=3,ls='--' ,label='SIII')
ax.loglog(id[:,0],id[:,43],c='green',lw=3,ls='-.' ,label='SIIII')
ax.loglog(id[:,0],id[:,44],c='magenta'  ,lw=3,ls='-'  ,label='SIIV')
ax.loglog(id[:,0],id[:,45],c='navy' ,lw=3,ls='--' ,label='SIV')
ax.loglog(id[:,0],id[:,46],c='darkred' ,lw=3,ls='--' ,label='SIVI')

ax.loglog(fd[:,0],fd[:,41],c='red'    ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,42],c='blue'   ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,43],c='green'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,44],c='magenta',ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,45],c='navy'   ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,46],c='darkred'   ,ls='None',marker='*',ms=10,lw=3)

ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
ax.xaxis.set_major_formatter(FuncFormatter(mjrform))
plt.xlabel(r'Log$_{10}$(T) [K]')
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
ax.text(0.5,0.90,r'F$_{\rm {Si}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.subplots_adjust(hspace=0,wspace=0)
#plt.setp(ax.get_xticklabels(),visible=False)
plt.setp(ax.get_yticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')


##Iron
plt.subplot(4,3,10)
ax = plt.gca()
ax.loglog(id[:,0],id[:,47],c='red'  ,lw=3,ls='-'  ,label='FEI')
ax.loglog(id[:,0],id[:,48],c='blue' ,lw=3,ls='--' ,label='FEII')
ax.loglog(id[:,0],id[:,49],c='green',lw=3,ls='-.' ,label='FEIII')
ax.loglog(id[:,0],id[:,50],c='magenta'  ,lw=3,ls='-'  ,label='FEIV')
ax.loglog(id[:,0],id[:,51],c='navy'  ,lw=3,ls='-'  ,label='FEV')

ax.loglog(fd[:,0],fd[:,47],c='red'    ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,48],c='blue'   ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,49],c='green'  ,ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,50],c='magenta',ls='None',marker='*',ms=10,lw=3)
ax.loglog(fd[:,0],fd[:,51],c='navy',ls='None',marker='*',ms=10,lw=3)


ax.yaxis.set_major_formatter(FuncFormatter(mjrform))
ax.xaxis.set_major_formatter(FuncFormatter(mjrform))
plt.xlabel(r'Log$_{10}$(T) [K]')
plt.xlim([xmin,xmax])
plt.ylim([ymin,ymax])
ax.text(0.5,0.90,r'F$_{\rm {Fe}}$',fontsize=12,fontweight='bold',rotation='horizontal',transform=ax.transAxes)
plt.subplots_adjust(hspace=0,wspace=0)
#plt.setp(ax.get_xticklabels(),visible=False)
#plt.setp(ax.get_yticklabels(),visible=False)
ax.xaxis.set_tick_params(width=2)
ax.yaxis.set_tick_params(width=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontweight('bold')



#plt.tight_layout()
plt.savefig('Ion_ex_fracs.pdf')
