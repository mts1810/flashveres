!!****Cool_sodium
!!
!!
!!
!! NAME
!!  Cool_sodium
!!
!! SYNOPSIS
!!	Calculates the cooling due to sodium
!!  	Total cooling rate is returned as cout_na
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_sodium(temp,rho,tindex,nin,cout_na)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_na
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: na0term, na1term, nahiterm

!! This will calculate the cooing from hydrogen
!! We are linearaly interpolating along the cooling functions

cout_na = 0.0e0
na0term = 0.0e0
na1term = 0.0e0
nahiterm = 0.0e0

!!Na0 first. All of these are n_ion * n_elec * CF / rho

if(tindex == clength) then
   na0term  = coolna0(clength)
   na1term  = coolna1(clength)
   nahiterm = coolnahi(clength)
else if (tindex == 1) then
   na0term  = coolna0(1)
   na1term  = coolna1(1)
   nahiterm = coolnahi(1)
else
   na0term  = coolna0(tindex)  + (coolna0(tindex+1)-coolna0(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex))) 
   na1term  = coolna1(tindex)  + (coolna1(tindex+1)-coolna1(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex))) 
   nahiterm = coolnahi(tindex) + (coolnahi(tindex+1)-coolnahi(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
endif

na0term  = na0term * nin(iNA)   * nin(iELEC) 
if(na0term .ne. na0term) na0term = 1.0e-30
na1term  = na1term * nin(iNAP)  * nin(iELEC)
if(na1term .ne. na1term) na1term = 1.0e-30
nahiterm = nahiterm * nin(iNA2P) * nin(iELEC) 
if(nahiterm .ne. nahiterm) nahiterm = 1.0e-30

cout_na = (na0term+na1term+nahiterm)/rho

!!print *, 'ninH: ', nin(iH), ' ninHP: ', nin(iHP), 'ninE: ', nin(iELEC)
!!print *, 'h0: ', h0term, ' h1: ', h1term
!!print *, 'cout_h: ', cout_h, ' rho: ', rho

return 
end subroutine Cool_sodium
