import numpy as np
import sys

def mapit(wanted,stname,lxname):
	iwant = -10
	for i in xrange(len(lxname)):
		if( wanted == stname[i] ):
			iwant = i
			break
	if(iwant == -10):
		sys.exit('ERROR IN MAPIT')
	return lxname[iwant]

def write_rxn(line,ms):
	rxn_number = line[0]
	num_r = int(line[1])
	num_p = int(line[2])
	if(num_p == 1):
		ln = str(rxn_number).zfill(3) + "  &  " + (mapit(line[3],stname,lxname)).ljust(ms) + "  +  " + (mapit(line[4],stname,lxname)).ljust(ms) + "  \\ra  " + (mapit(line[5],stname,lxname)).ljust(ms) + "     " + "".ljust(ms)                             + "     " + "".ljust(ms)                             + "    &    & "
	elif(num_p == 2):
		ln = str(rxn_number).zfill(3) + "  &  " + (mapit(line[3],stname,lxname)).ljust(ms) + "  +  " + (mapit(line[4],stname,lxname)).ljust(ms) + "  \\ra  " + (mapit(line[5],stname,lxname)).ljust(ms) + "  +  " + (mapit(line[6],stname,lxname)).ljust(ms) + "     " + "".ljust(ms)                             + "    &    & "
	elif(num_p == 3):
		ln = str(rxn_number).zfill(3) + "  &  " + (mapit(line[3],stname,lxname)).ljust(ms) + "  +  " + (mapit(line[4],stname,lxname)).ljust(ms) + "  \\ra  " + (mapit(line[5],stname,lxname)).ljust(ms) + "  +  " + (mapit(line[6],stname,lxname)).ljust(ms) + "  +  " + (mapit(line[7],stname,lxname)).ljust(ms) + "    &    & "
	else:
		sys.exit('Something has gone wrong')
	print ln


stname = ['H','HP'     ,'HE','HEP'     ,'HE2P'     ,'C','CP'     ,'C2P'     ,'C3P'     ,'C4P'     ,'C5P'     ,'N','NP'     ,'N2P'     ,'N3P'     ,'N4P'     ,'N5P'     ,'N6P'     ,'O','OP'     ,'O2P'     ,'O3P'     ,'O4P'     ,'O5P'    ,'O6P'      ,'O7P'     ,'NE','NEP'     ,'NE2P'     ,'NE3P'     ,'NE4P'     ,'NE5P'     ,'NE6P'     ,'NE7P'     ,'NE8P'     ,'NE9P'     ,'NA','NAP'     ,'NA2P'     ,'MG','MGP'     ,'MG2P'     ,'MG3P'     ,'SI','SIP'     ,'SI2P'     ,'SI3P'     ,'SI4P'     ,'SI5P'     ,'S','SP'     ,'S2P'     ,'S3P'     ,'S4P'     ,'CA','CAP'     ,'CA2P'     ,'CA3P'     ,'CA4P'     ,'FE','FEP'     ,'FE2P'     ,'FE3P'     ,'FE4P'     ,'E'      ,'UV']
lxname = ['H','H$^{+}$','He','He$^{+}$','He$^{2+}$','C','C$^{+}$','C$^{2+}$','C$^{3+}$','C$^{4+}$','C$^{5+}$','N','N$^{+}$','N$^{2+}$','N$^{3+}$','N$^{4+}$','N$^{5+}$','N$^{6+}$','O','O$^{+}$','O$^{2+}$','O$^{3+}$','O$^{4+}$','O$^{5+}$','O$^{6+}$','O$^{7+}$','Ne','Ne$^{+}$','Ne$^{2+}$','Ne$^{3+}$','Ne$^{4+}$','Ne$^{5+}$','Ne$^{6+}$','Ne$^{7+}$','Ne$^{8+}$','Ne$^{9+}$','Na','Na$^{+}$','Na$^{2+}$','Mg','Mg$^{+}$','Mg$^{2+}$','Mg$^{3+}$','Si','Si$^{+}$','Si$^{2+}$','Si$^{3+}$','Si$^{4+}$','Si$^{5+}$','S','S$^{+}$','S$^{2+}$','S$^{3+}$','S$^{4+}$','Ca','Ca$^{+}$','Ca$^{2+}$','Ca$^{3+}$','Ca$^{4+}$','Fe','Fe$^{+}$','Fe$^{2+}$','Fe$^{3+}$','Fe$^{4+}$','e$^{-}$','$\gamma$']

ms = 0
for i in xrange(len(lxname)):
	lsize = len(lxname[i])
	if lsize > ms:
		ms = lsize
file = 'input.dat'
file = open(file,'r')
for line in file.readlines():
	ln = (line.strip()).split()
	write_rxn(ln,ms)
