!!****Cool_Silicon
!!
!!
!!
!! NAME
!!  Cool_helium
!!
!! SYNOPSIS
!!	Calculates the cooling due to carbon
!!  	Total cooling rate is returned as cout_si
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_silicon(temp,rho,tindex,nin,cout_si)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_si
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: si0term, si1term, si2term, si3term, si4term, sihiterm

!! This will calculate the cooing from hydrogen
!! We are linearaly interpolating along the cooling functions

cout_si = 0.0e0
si0term = 0.0e0
si1term = 0.0e0
si2term = 0.0e0
si3term = 0.0e0
si4term = 0.0e0
sihiterm = 0.0e0

!!Si0 first. All of these are n_ion * n_elec * CF / rho
!!Si4 has been altered to include contribution from higher ionization states


if(tindex == clength) then
   si0term = coolsi0(clength)
   si1term = coolsi1(clength)
   si2term = coolsi2(clength)
   si3term = coolsi3(clength)
   si4term = coolsi4(clength)
   sihiterm = coolsihi(clength)
else if (tindex == 1) then
   si0term = coolsi0(1)
   si1term = coolsi1(1)
   si2term = coolsi2(1)
   si3term = coolsi3(1)
   si4term = coolsi4(1)
   sihiterm = coolsihi(1)
else
   si0term  = coolsi0(tindex)  + (coolsi0(tindex+1) - coolsi0(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   si1term  = coolsi1(tindex)  + (coolsi1(tindex+1) - coolsi1(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   si2term  = coolsi2(tindex)  + (coolsi2(tindex+1) - coolsi2(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   si3term  = coolsi3(tindex)  + (coolsi3(tindex+1) - coolsi3(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   si4term  = coolsi4(tindex)  + (coolsi4(tindex+1) - coolsi4(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   sihiterm = coolsihi(tindex) + (coolsihi(tindex+1)- coolsihi(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
endif

si0term  = si0term  * nin(iSI)   * nin(iELEC) 
if(si0term .ne. si0term) si0term = 1.0e-30
si1term  = si1term  * nin(iSIP)  * nin(iELEC) 
if(si1term .ne. si1term) si1term = 1.0e-30
si2term  = si2term  * nin(iSI2P) * nin(iELEC) 
if(si2term .ne. si2term) si2term = 1.0e-30
si3term  = si3term  * nin(iSI3P) * nin(iELEC) 
if(si3term .ne. si3term) si3term = 1.0e-30
si4term  = si4term  * nin(iSI4P) * nin(iELEC)
if(si4term .ne. si4term) si4term = 1.0e-30
sihiterm = sihiterm * nin(iSI5P) * nin(iELEC) 
if(sihiterm .ne. sihiterm) sihiterm = 1.0e-30

cout_si = (si0term+si1term+si2term+si3term+si4term+sihiterm)/rho

return 
end subroutine Cool_silicon
