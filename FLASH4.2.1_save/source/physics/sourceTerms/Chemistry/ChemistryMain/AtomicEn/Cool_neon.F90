!!****Cool_neon
!!
!!
!!
!! NAME
!!  Cool_neon
!!
!! SYNOPSIS
!!	Calculates the cooling due to neon
!!  	Total cooling rate is returned as cout_ne
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_neon(temp,rho,tindex,nin,cout_ne)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_ne
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: ne0term, ne1term, ne2term, ne3term, ne4term, ne5term
real :: ne6term, ne7term, ne8term, nehiterm, tempterm

!! This will calculate the cooing from hydrogen
!! We are linearaly interpolating along the cooling functions

cout_ne = 0.0e0
ne0term = 0.0e0
ne1term = 0.0e0
ne2term = 0.0e0
ne3term = 0.0e0
ne4term = 0.0e0
ne5term = 0.0e0
ne6term = 0.0e0
ne7term = 0.0e0
ne8term = 0.0e0
nehiterm = 0.0e0
tempterm = 0.0e0

!!Ne0 first. All of these are n_ion * n_elec * CF / rho
!!O7 has been altered to include contribution from higher ionization states


if(tindex == clength) then
   ne0term = coolne0(clength)
   ne1term = coolne1(clength)
   ne2term = coolne2(clength)
   ne3term = coolne3(clength)
   ne4term = coolne4(clength)
   ne5term = coolne5(clength)
   ne6term = coolne6(clength)
   ne7term = coolne7(clength)
   ne8term = coolne8(clength)
   nehiterm = coolnehi(clength)
else if (tindex == 1) then
   ne0term = coolne0(1)
   ne1term = coolne1(1)
   ne2term = coolne2(1)
   ne3term = coolne3(1)
   ne4term = coolne4(1)
   ne5term = coolne5(1)
   ne6term = coolne6(1)
   ne7term = coolne7(1)
   ne8term = coolne8(1)
   nehiterm = coolnehi(1)
else
   tempterm = ((temp-cooltemp(tindex))/(cooltemp(tindex+1)-cooltemp(tindex)))
   ne0term  = coolne0(tindex)  + (coolne0(tindex+1)  - coolne0(tindex))*tempterm
   ne1term  = coolne1(tindex)  + (coolne1(tindex+1)  - coolne1(tindex))*tempterm
   ne2term  = coolne2(tindex)  + (coolne2(tindex+1)  - coolne2(tindex))*tempterm
   ne3term  = coolne3(tindex)  + (coolne3(tindex+1)  - coolne3(tindex))*tempterm
   ne4term  = coolne4(tindex)  + (coolne4(tindex+1)  - coolne4(tindex))*tempterm
   ne5term  = coolne5(tindex)  + (coolne5(tindex+1)  - coolne5(tindex))*tempterm
   ne6term  = coolne6(tindex)  + (coolne6(tindex+1)  - coolne6(tindex))*tempterm
   ne7term  = coolne7(tindex)  + (coolne7(tindex+1)  - coolne7(tindex))*tempterm
   ne8term  = coolne8(tindex)  + (coolne8(tindex+1)  - coolne8(tindex))*tempterm
   nehiterm = coolnehi(tindex) + (coolnehi(tindex+1) - coolnehi(tindex))*tempterm
endif

ne0term  =  ne0term  * nin(iNE)   * nin(iELEC)
if(ne0term .ne. ne0term) ne0term = 1.0e-30
ne1term  =  ne1term  * nin(iNEP)  * nin(iELEC)
if(ne1term .ne. ne1term) ne1term = 1.0e-30
ne2term  =  ne2term  * nin(iNE2P) * nin(iELEC)
if(ne2term .ne. ne2term) ne2term = 1.0e-30
ne3term  =  ne3term  * nin(iNE3P) * nin(iELEC)
if(ne3term .ne. ne3term) ne3term = 1.0e-30
ne4term  =  ne4term  * nin(iNE4P) * nin(iELEC)
if(ne4term .ne. ne4term) ne4term = 1.0e-30
ne5term  =  ne5term  * nin(iNE5P) * nin(iELEC)
if(ne5term .ne. ne5term) ne5term = 1.0e-30
ne6term  =  ne6term  * nin(iNE6P) * nin(iELEC)
if(ne6term .ne. ne6term) ne6term = 1.0e-30
ne7term  =  ne7term  * nin(iNE7P) * nin(iELEC)
if(ne7term .ne. ne7term) ne7term = 1.0e-30
ne8term  =  ne8term  * nin(iNE8P) * nin(iELEC)
if(ne8term .ne. ne8term) ne8term = 1.0e-30
nehiterm =  nehiterm * nin(iNE9P) * nin(iELEC)
if(nehiterm .ne. nehiterm) nehiterm = 1.0e-30

cout_ne = (ne0term+ne1term+ne2term+ne3term+ne4term+ne5term+ne6term+ne7term+ne8term+nehiterm)/rho

return 
end subroutine Cool_neon
