!!****Cool_nitrogen
!!
!!
!!
!! NAME
!!  Cool_helium
!!
!! SYNOPSIS
!!	Calculates the cooling due to carbon
!!  	Total cooling rate is returned as cout_n
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_nitrogen(temp,rho,tindex,nin,cout_n)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_n
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: n0term, n1term, n2term, n3term, n4term, nhiterm

!! This will calculate the cooing from hydrogen
!! We are linearaly interpolating along the cooling functions

cout_n = 0.0e0
n0term = 0.0e0
n1term = 0.0e0
n2term = 0.0e0
n3term = 0.0e0
n4term = 0.0e0
nhiterm = 0.0e0

!!N0 first. All of these are n_ion * n_elec * CF / rho
!!N5 has been altered to include contribution from higher ionization states


if(tindex == clength) then
   n0term = cooln0(clength)
   n1term = cooln1(clength)
   n2term = cooln2(clength)
   n3term = cooln3(clength)
   n4term = cooln4(clength)
   nhiterm = coolnhi(clength)
else if (tindex == 1) then
   n0term = cooln0(1)
   n1term = cooln1(1)
   n2term = cooln2(1)
   n3term = cooln3(1)
   n4term = cooln4(1)
   nhiterm = coolnhi(1)
else
   n0term  = cooln0(tindex)  + (cooln0(tindex+1) - cooln0(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   n1term  = cooln1(tindex)  + (cooln1(tindex+1) - cooln1(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   n2term  = cooln2(tindex)  + (cooln2(tindex+1) - cooln2(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   n3term  = cooln3(tindex)  + (cooln3(tindex+1) - cooln3(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   n4term  = cooln4(tindex)  + (cooln4(tindex+1) - cooln4(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   nhiterm = coolnhi(tindex) + (coolnhi(tindex+1)- coolnhi(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
endif

n0term  = n0term  * nin(iN)   * nin(iELEC) 
if(n0term .ne. n0term) n0term = 1.0e-30
n1term  = n1term  * nin(iNP)  * nin(iELEC) 
if(n1term .ne. n1term) n1term = 1.0e-30
n2term  = n2term  * nin(iN2P) * nin(iELEC) 
if(n2term .ne. n2term) n2term = 1.0e-30
n3term  = n3term  * nin(iN3P) * nin(iELEC) 
if(n3term .ne. n3term) n3term = 1.0e-30
n4term  = n4term  * nin(iN4P) * nin(iELEC) 
if(n4term .ne. n4term) n4term = 1.0e-30
nhiterm = nhiterm * nin(iN5P) * nin(iELEC) 
if(nhiterm .ne. nhiterm) nhiterm = 1.0e-30

cout_n = (n0term+n1term+n2term+n3term+n4term+nhiterm)/rho
if(cout_n .ne. cout_n) cout_n = 1.0e-30
return 
end subroutine Cool_nitrogen
