!!****Chemisty_networkRates
!!
!!
!!
!! NAME
!!  Chemistry_networkRates
!!
!! SYNOPSIS
!!	Define the rates for every reaction
!!
!! ARGUMENTS
!!	
!!  Density
!!  Temperature
!!
!!*****

subroutine Chemistry_networkRates(ctemp,dens,ys)

use Chemistry_data

implicit none

#include "constants.h"
#include "Flash.h"

!! Declare what comes in
real :: ctemp, dens
real, dimension(NSPECIES) :: ys

integer i

real :: te,na,t4,naterm,term,tep,tevp,narate
real :: rrate, drrate, vrate

te  = ctemp*8.6173388e-5  !! In terms of ev
tep = ctemp**(-3.0/2.0)
tevp = te**(-3.0/2.0)
t4  = ctemp/1.0e4 
na = 6.02213e23
naterm = na*dens
narate = na*dens
term = 0.0e0
rrate = 0.0e0
drrate = 0.0e0
vrate = 0.0e0



do i=1,ireaction
   ratraw(i) = 1.0e-30
enddo

!!R001 He+ + e- --> He
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(5.235e-11,0.6988,7.301,4.475e6,0.0829,1.682e5,ctemp,rrate)
call Badnell_DR5(5.966e-4,1.613e-4,-2.223e-5,0.0e0,0.0e0,4.556e5,5.552e5,8.982e5,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR001) = (rrate + drrate) * naterm
!!ratraw(iR001) =  ( exp(-chem_tauhe) * (1.0e-11*ctemp**(-0.5)*(12.72-1.615*log10(ctemp)-0.3162*log10(ctemp)**2+0.0493*log10(ctemp)**3)) + (1.0 - exp(-chem_tauhe))*(1.0e-11*ctemp**(-0.5)*(11.19-1.676*log10(ctemp)-0.2852*log10(ctemp)**2+0.04433*log10(ctemp)**3))) * naterm

!!R002 C+ + e- --> C
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(2.995e-9,0.7849,6.67e-3,1.943e6,0.1597,4.955e4,ctemp,rrate)
call Badnell_DR5(1.320e-4,1.536e-3,0.0e0,0.0e0,0.0e0,7.767e4,1.445e5,0.0e0,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR002) = (rrate + drrate) * naterm

!!R003 C2+ + e- --> C+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(2.067e-9,0.8012,1.643e-1,2.172e6,0.0427,6.341e4,ctemp,rrate)
call Badnell_DR5(9.146e-6,5.045e-4,3.742e-3,0.0e0,0.0e0,6.485e3,1.052e5,1.458e5,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR003) = (rrate + drrate) * naterm

!!R004 C3+ + e- --> C2+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(1.12e-10,0.6737,1.115e2,5.938e6,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(2.556e-5,2.222e-3,1.076e-3,1.635e-4,2.500e-3,3.329e3,8.181e4,1.216e5,6.096e5,3.295e6,ctemp,drrate)
ratraw(iR004) = (rrate + drrate) * naterm

!!R005 C4+ + e- --> C3+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(4.798e-11,0.4834,1.355e3,1.872e7,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(2.646e-3,1.762e-2,-7.843e-4,0.0e0,0.0e0,2.804e6,3.485e6,4.324e6,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR005) = (rrate + drrate) * naterm

!!R006 C5+ + e- --> C4+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(2.044e-10,0.6742,2.647e2,2.773e7,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(1.426e-3,3.046e-2,8.373e-4,0.0e0,0.0e0,3.116e6,4.075e6,5.749e6,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR006) = (rrate + drrate) * naterm

!!R007 O+ + e- --> O
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(6.622e-11,0.6109,4.136e0,4.216e6,0.4093,8.770e4,ctemp,rrate)
call Badnell_DR5(1.263e-4,1.263e-4,1.263e-4,1.263e-4,1.263e-4,1.715e5,1.715e5,1.715e5,1.715e5,1.715e5,ctemp,drrate)
ratraw(iR007) = (rrate + drrate) * naterm

!!R008 O2+ + e- --> O+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(2.096e-9,0.7668,1.602e-1,4.377e6,0.1070,1.392e5,ctemp,rrate)
call Badnell_DR8ev(4.223e-2,1.879e-2,5.227e-2,0.0e0,8.415e0,3.113e2,6.3e0,2.885e1,1.513e-2,2.719e-2,3.3e-1,0.0e0,7.312e0,1.954e1,3.136e1,3.429e1,ctemp,drrate)
ratraw(iR008) = (rrate + drrate) * naterm

!!R009 O3+ + e- --> O2+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(2.501e-9,0.7844,5.235e-1,4.470e6,0.0447,1.642e5,ctemp,rrate)
call Badnell_DR5(7.373e-5,6.993e-3,3.848e-3,0.0e0,0.0e0,2.421e4,2.156e5,5.280e5,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR009) = (rrate + drrate) * naterm

!!R010 O4+ + e- --> O3+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(3.955e-9,0.7813,6.82e-1,5.076e6,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(1.925e-5,1.246e-4,4.556e-4,1.083e-2,9.868e-4,8.552e2,1.640e4,6.560e4,2.229e5,8.160e5,ctemp,drrate)
ratraw(iR010) = (rrate + drrate) * naterm

!!R011 O5+ + e- --> O4+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(1.724e-10,0.6556,3.372e2,1.030e7,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(3.966e-4,5.938e-3,2.352e-3,9.375e-3,4.261e-3,4.611e4,1.416e5,8.650e5,6.112e6,6.112e6,ctemp,drrate)
ratraw(iR011) = (rrate + drrate) * naterm

!!R012 O6+ + e- -- O5+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(8.193e-11,0.5165,2.392e3,2.487e7,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(6.135e-2,1.968e-4,0.0e0,0.0e0,0.0e0,6.113e6,3.656e7,0.0e0,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR012) = (rrate + drrate) * naterm

!!R013 O7+ + e- --> O6+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(2.652e-10,0.6705,5.842e2,4.559e7,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(4.925e-3,5.837e-2,1.359e-3,0.0e0,0.0e0,5.44e6,7.170e6,1.152e7,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR013) = (rrate + drrate) * naterm

!!R014 Mg+ + e- --> Mg
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(5.452e-11,0.6845,5.637e0,1.551e6,0.3945,8.360e5,ctemp,rrate)
call Badnell_DR5(3.871e-8,4.732e-7,1.599e-3,2.628e-5,0.0e0,8.415e3,1.682e4,5.0e4,2.759e5,0.0e0,ctemp,drrate)
ratraw(iR014) = (rrate + drrate) * naterm

!!R015 Mg2+ + e- --> Mg+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(1.345e-11,0.1074,7.877e2,7.925e7,0.4631,5.027e5,ctemp,rrate)
call Badnell_DR8ev(2.033e1,9.120e1,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,4.382e1,5.224e1,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR015) = (rrate + drrate) * naterm

!!R016 Na+ + e- --> Na
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(5.095e-12,0.0e0,3.546e2,2.310e6,0.9395,4.297e5,ctemp,rrate)
call Badnell_DR8ev(2.359e0,1.60e1,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,2.626e1,2.943e1,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR016) = (rrate + drrate) * naterm

!!R017 H+ + e- --> H
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(8.318e-11,0.7472,2.965,7.001e5,0.0e0,0.0e0,ctemp,rrate)
ratraw(iR017) = (rrate + drrate) * naterm
!!ratraw(iR016) = (exp(-chem_tauh) * (1.269e-13 * (315614.0/ctemp)**(1.503) * ( 1.0 + (604625.0/ctemp)**(0.470)) **(-1.923)) + (1.0 - exp(-chem_tauh)) * (2.753e-14*(315614.0/ctemp)**(1.50)*(1.0 + (115188.0/ctemp)**(0.407))**(-2.242))) * naterm

!!R018 H + e- --> H+ + 2e-
vrate = 0.0e0
call Voronov(13.6,0.0,2.91e-8,0.232,0.39,0.4,20.0,ctemp,vrate)
ratraw(iR018) = vrate * narate

!!R019 He + e- --> He+ + 2e-
vrate = 0.0e0
call Voronov(24.6,0.0,1.75e-8,0.180,0.35,0.4,20.0,ctemp,vrate)
ratraw(iR019) = vrate * narate

!!R020 He+ + e- --> He2+ + e2-
vrate = 0.0e0
call Voronov(54.4,1.0,2.05e-9,0.265,0.25,3.0,20.0,ctemp,vrate)
ratraw(iR020) = vrate * narate

!!R021 C + e- --> C+ + 2e-
vrate = 0.0e0
call Voronov(11.3,0.0,6.85e-8,0.193,0.25,0.4,20.0,ctemp,vrate)
ratraw(iR021) = vrate * narate

!!R022 C+ + e- --> C2+ + 2e-
vrate = 0.0e0
call Voronov(24.4,1.0,1.86e-8,0.286,0.24,0.4,20.0,ctemp,vrate)
ratraw(iR022) = vrate * narate

!!R023 C2+ + e- --> C3+ + 2e-
vrate = 0.0e0
call Voronov(47.9,1.0,6.35e-9,0.427,0.21,2.0,20.0,ctemp,vrate)
ratraw(iR023) = vrate * narate

!!R024 C3+ + e- --> C4+ + 2e-
vrate = 0.0e0
call Voronov(64.5,1.0,1.50e-9,0.416,0.13,3.0,20.0,ctemp,vrate)
ratraw(iR024) = vrate * narate

!!R025 C4+ + e- --> C5+ + 2e-
vrate = 0.0e0
call Voronov(392.1,1.0,2.99e-10,0.666,0.02,20.0,20.0,ctemp,vrate)
ratraw(iR025) = vrate * narate

!!R026 O + e- --> O+ + 2e-
vrate = 0.0e0
call Voronov(13.6,0.0,3.59e-8,0.073,0.34,0.4,20.0,ctemp,vrate)
ratraw(iR026) = vrate * narate

!!R027 O+ + e- --> O2+ + 2e-
vrate = 0.0e0
call Voronov(35.1,1.0,1.39e-8,0.212,0.22,2.0,20.0,ctemp,vrate)
ratraw(iR027) = vrate * narate

!!R028 O2+ + e- --> O3+ + 2e-
vrate = 0.0e0
call Voronov(54.9,1.0,9.31e-9,0.270,0.27,3.0,20.0,ctemp,vrate)
ratraw(iR028) = vrate * narate

!!R029 O3+ + e- --> O4+ + 2e-
vrate = 0.0e0
call Voronov(77.4,0.0,1.02e-8,0.614,0.27,4.0,20.0,ctemp,vrate)
ratraw(iR029) = vrate * narate

!!R030 O4+ + e- --> O5+ + 2e-
vrate = 0.0e0
call Voronov(113.9,1.0,2.19e-9,0.630,0.17,5.0,20.0,ctemp,vrate)
ratraw(iR030) = vrate * narate

!!R031 O5+ + e- --> O6+ + 2e-
vrate = 0.0e0
call Voronov(138.1,0.0,1.95e-9,0.360,0.54,7.0,20.0,ctemp,vrate)
ratraw(iR031) = vrate * narate

!!R032 O6+ + e- --> O7+ + 2e-
vrate = 0.0e0
call Voronov(739.3,0.0,2.12e-10,0.396,0.35,30.0,20.0,ctemp,vrate)
ratraw(iR032) = vrate * narate

!!R033 Mg + e- --> Mg+ + 2e-
vrate = 0.0e0
call Voronov(7.6,0.0,6.21e-7,0.592,0.39,0.4,20.0,ctemp,vrate)
ratraw(iR033) = vrate * narate

!!R034 Mg+ + e- --> Mg2+ + 2e-
vrate = 0.0e0
call Voronov(15.2,0.0,1.92e-8,0.0027,0.85,0.4,20.0,ctemp,vrate)
ratraw(iR034) = vrate * narate

!!R035 Na + e- --> Na+ + 2e-
vrate = 0.0e0
call Voronov(5.1,1.0,1.01e-7,0.275,0.23,0.4,20.0,ctemp,vrate)
ratraw(iR035) = vrate*narate

!!R036 HE2+ + e- --> He+
ratraw(iR036) = 2.538e-13*(1262456.0/ctemp)**(1.503) * ( 1.0 + (2418500.0/ctemp)**(0.470) ) **(-1.923) * naterm
!!ratraw(iR036) = ( exp(-chem_tauhep)*(2.538e-13*(1262456.0/ctemp)**(1.503) * ( 1.0 + (2418500.0/ctemp)**(0.470) ) **(-1.923)) + (1.0-exp(-chem_tauhep))*(5.506e-14*(1262456.0/ctemp)**1.500*(1.0+(460753.0/ctemp)**(0.407))**(-2.242))) * naterm

!!R037 He+ + H --> He + H+
ratraw(iR037) = 1.2e-15*(ctemp/300.0)**(0.25) * naterm

!!R038 He + H+ --> He+ + H+
if(ctemp .gt. 1.0e4) then
   ratraw(iR038) = 4.0e-37*ctemp**(4.74) * naterm
else
   ratraw(iR038) = 1.26e-9*ctemp**(-0.75)*exp(-127500.0/ctemp) * naterm
endif

!!R039 N+ + e- --> N
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(6.387e-10,0.7308,9.467e-2,2.954e6,0.2440,6.739e4,ctemp,rrate)
call Badnell_DR8ev(2.678e-2,1.614e-2,7.742e-2,0.0e0,2.182e0,8.809e1,9.754e-1,3.906e0,6.280e-3,1.362e-2,1.820e-1,0.0e0,5.818e0,1.510e1,1.836e1,2.054e1,ctemp,drrate)
ratraw(iR039) = (rrate + drrate) * naterm

!!R040 N2+ + e- --> N+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(2.410e-9,0.7948,1.231e-1,3.016e6,0.0774,1.016e5,ctemp,rrate)
call Badnell_DR5(2.852e-6,2.527e-3,2.660e-3,3.831e-5,0.0e0,1.138e4,1.568e5,2.734e5,2.207e6,0.0e0,ctemp,drrate)
ratraw(iR040) = (rrate + drrate) * naterm

!!R041 N3+ + e- --> N2+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(7.923e-10,0.7768,3.750e0,3.468e6,0.0223,7.206e4,ctemp,rrate)
call Badnell_DR5(5.943e-5,6.386e-4,7.050e-3,1.711e-5,0.0e0,9.140e3,1.027e5,1.928e5,2.824e6,0.0e0,ctemp,drrate)
ratraw(iR041) = (rrate + drrate) * naterm

!!R042 N4+ + e- --> N3+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(1.533e-10,0.6682,1.823e2,7.751e6,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(9.033e-5,4.470e-3,8.358e-4,2.673e-3,2.557e-3,1.462e4,1.106e5,4.548e5,4.077e6,5.355e6,ctemp,drrate)
ratraw(iR042) = (rrate + drrate) * naterm

!!R043 N5+ + e- --> N4+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(6.245e-11,0.4985,1.957e3,2.177e7,0.0e0,0.0e0,ctemp,rrate)
call Badnell_DR5(5.761e-3,3.434e-2,-1.660e-3,0.0e0,0.0e0,3.860e6,4.883e6,6.259e6,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR043) = (rrate + drrate) * naterm

!!R044 Si+ + e- --> Si
rrate = 0.0e0
drrate = 0.0e0
!!call Badnell_RR( NO RATE!?!
call Badnell_DR6(3.408e-8,1.913e-7,1.679e-7,7.523e-7,8.386e-5,4.083e-3,2.431e1,1.293e2,4.272e2,3.729e3,5.514e4,1.295e5,ctemp,drrate)
ratraw(iR044) = (rrate + drrate) * naterm

!!R045 Si2+ + e- --> Si+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(1.964e-10,0.6287,7.712e0,2.951e7,0.1523,4.804e5,ctemp,rrate)
call Badnell_DR5(2.930e-6,2.803e-6,9.023e-5,6.909e-3,2.582e-5,1.162e2,5.721e3,3.477e4,1.176e5,3.505e6,ctemp,drrate)
ratraw(iR045) = (rrate + drrate) * naterm

!!R046 Si3+ + e- --> Si2+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(6.739e-11,0.4931,2.166e2,4.491e7,0.1667,9.046e5,ctemp,rrate)
call Badnell_DR5(3.819e-6,2.421e-5,2.283e-4,8.604e-3,2.617e-3,3.802e3,1.280e4,5.953e4,1.026e5,1.154e6,ctemp,drrate)
ratraw(iR046) = (rrate + drrate) * naterm

!!R047 Si4+ + e- --> Si3+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(5.134e-11,0.3678,1.009e3,8.514e7,0.1646,1.084e6,ctemp,rrate)
call Badnell_DR8ev(2.408e2,1.023e3,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,8.674e1,1.104e2,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR047) = (rrate + drrate) * naterm

!!R048 Si5+ + e- --> Si4+
rrate = 0.0e0
drrate = 0.0e0
call Badnell_RR(2.468e-10,0.6113,1.649e2,3.231e7,0.0636,9.837e5,ctemp,rrate)
call Badnell_DR8ev(4.858e-2,2.016e-1,6.506e-2,3.515e0,5.414e2,1.137e3,1.033e3,0.0e0,1.893e-2,2.233e-1,7.974e-1,1.230e1,4.773e1,1.127e2,1.541e2,0.0e0,ctemp,drrate)
ratraw(iR048) = (rrate + drrate) * naterm

!!R049 Fe+ + e- --> Fe
rrate = 0.0e0
drrate = 0.0e0
call Shull_RR(1.42e-13,8.91e-1,ctemp,rrate)
call Mazzotta_DR(1.7609e-10,8.0041e-11,0.0e0,0.0e0,5.12,12.90,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR049) = (rrate + drrate) * naterm

!!R050 Fe2+ + e- --> Fe+
rrate = 0.0e0
drrate = 0.0e0
call Shull_RR(1.02e-12,8.43e-1,ctemp,rrate)
call Mazzotta_DR(1.8409e-9,2.1611e-9,0.0e0,0.0e0,16.70,31.40,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR050) = (rrate + drrate) * naterm

!!R051 Fe3+ + e- --> Fe2+
rrate = 0.0e0
drrate = 0.0e0
call Shull_RR(3.32e-12,7.46e-1,ctemp,rrate)
call Mazzotta_DR(1.2006e-8,3.7619e-9,0.0e0,0.0e0,28.60,52.10,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR051) = (rrate + drrate) * naterm

!!R052 Fe4+ + e- --> Fe3+
rrate = 0.0e0
drrate = 0.0e0
call Shull_RR(7.80e-12,6.82e-1,ctemp,rrate)
call Mazzotta_DR(3.0416e-8,1.2807e-8,0.0e0,0.0e0,37.30,67.40,0.0e0,0.0e0,ctemp,drrate)
ratraw(iR052) = (rrate + drrate) * naterm

!!R053 N + e- --> N+ + 2e-
vrate = 0.0e0
call Voronov(14.5,0.0,4.82e-8,0.0652,0.42,0.4,20.0,ctemp,vrate)
ratraw(iR053) = vrate * narate

!!R054 N+ + e- --> N2+ + 2e-
vrate = 0.0e0
call Voronov(29.6,0.0,2.98e-8,0.310,0.30,2.0,20.0,ctemp,vrate)
ratraw(iR054) = vrate * narate

!!R055 N2+ + e- --> N3+ + 2e-
vrate = 0.0e0
call Voronov(47.5,1.0,8.10e-9,0.350,0.24,2.0,20.0,ctemp,vrate)
ratraw(iR055) = vrate * narate

!!R056 N3+ + e- --> N4+ + 2e-
vrate = 0.0e0
call Voronov(77.5,1.0,3.71e-9,0.549,0.18,4.0,20.0,ctemp,vrate)
ratraw(iR056) = vrate * narate

!!R057 N4+ + e- --> N5+ + 2e-
vrate = 0.0e0
call Voronov(97.9,0.0,1.51e-9,0.0167,0.74,4.0,20.0,ctemp,vrate)
ratraw(iR057) = vrate * narate

!!R058 Si + e- --> Si+ + 2e-
vrate = 0.0e0
call Voronov(8.2,1.0,1.88e-7,0.376,0.25,0.4,20.0,ctemp,vrate)
ratraw(iR058) = vrate * narate

!!R059 Si+ + e- --> Si2+ + 2e-
vrate = 0.0e0
call Voronov(16.4,1.0,6.43e-8,0.632,0.20,0.4,20.0,ctemp,vrate)
ratraw(iR059) = vrate * narate

!!R060 Si2+ + e- --> Si3+ + 2e-
vrate = 0.0e0
call Voronov(33.5,1.0,2.01e-8,0.473,0.22,2.0,20.0,ctemp,vrate)
ratraw(iR060) = vrate * narate

!!R061 Si3+ + e- --> Si4+ + 2e-
vrate = 0.0e0
call Voronov(54.0,1.0,4.94e-9,0.172,0.23,3.0,20.0,ctemp,vrate)
ratraw(iR061) = vrate * narate

!!R062 Si4+ + e- --> Si5+ + 2e-
vrate = 0.0e0
call Voronov(166.8,1.0,1.76e-9,0.102,0.31,7.0,20.0,ctemp,vrate)
ratraw(iR062) = vrate * narate

!!R063 Fe + e- --> Fe+ + 2e-
vrate = 0.0e0
call Voronov(7.9,0.0,2.52e-7,0.701,0.25,0.4,20.0,ctemp,vrate)
ratraw(iR063) = vrate * narate

!!R064 Fe+ + e- --> Fe2+ + 2e-
vrate = 0.0e0
call Voronov(16.2,1.0,2.21e-8,0.033,0.45,0.4,20.0,ctemp,vrate)
ratraw(iR064) = vrate * narate

!!R065 Fe2+ + e- --> Fe3+ + 2e-
vrate = 0.0e0
call Voronov(30.6,0.0,4.10e-8,0.366,0.17,2.0,20.0,ctemp,vrate)
ratraw(iR065) = vrate * narate

!!R066 Fe3+ + e- --> Fe4+ + 2e-
vrate = 0.0e0
call Voronov(54.8,0.0,3.53e-8,0.243,0.39,3.0,20.0,ctemp,vrate)
ratraw(iR066) = vrate * narate

!!R067 H + gamma --> H+ + e-
ratraw(iR067) = photoc(1) * chem_j21

!!R068 He + gamma --> He+ + e-
ratraw(iR068) = photoc(2) * chem_j21

!!R069 He+ + gamma --> He++ + e-
ratraw(iR069) = photoc(3) * chem_j21

!!R070 C + gamma --> C+ + e-
ratraw(iR070) = photoc(4) * chem_j21

!!R071 C+ + gamma --> C2+ + e-
ratraw(iR071) = photoc(5) * chem_j21

!!R072 C2+ + gamma --> C3+ + e-
ratraw(iR072) = photoc(6) * chem_j21

!!R073 C3+ + gamma --> C4+ + e-
ratraw(iR073) = photoc(7) * chem_j21

!!R074 C4+ + gamma --> C5+ + e-
ratraw(iR074) = photoc(8) * chem_j21

!!R075 N + gamma --> N+ + e-
ratraw(iR075) = photoc(9) * chem_j21

!!R076 N+ + gamma --> N2+ + e-
ratraw(iR076) = photoc(10) * chem_j21

!!R077 N2+ + gamma --> N3+ + e-
ratraw(iR077) = photoc(11) * chem_j21

!!R078 N3+ + gamma --> N4+ + e-
ratraw(iR078) = photoc(12) * chem_j21

!!R079 N4+ + gamma --> N5+ + e-
ratraw(iR079) = photoc(13) * chem_j21

!!R080 O + gamma --> O+ + e-
ratraw(iR080) = photoc(14) * chem_j21

!!R081 O+ + gamma --> O2+ + e-
ratraw(iR081) = photoc(15) * chem_j21

!!R082 O2+ + gamma --> O3+ + e-
ratraw(iR082) = photoc(16) * chem_j21

!!R083 O3+ + gamma --> O4+ + e-
ratraw(iR083) = photoc(17) * chem_j21

!!R084 O4+ + gamma --> O5+ + e-
ratraw(iR084) = photoc(18) * chem_j21

!!R085 O5+ + gamma --> O6+ + e-
ratraw(iR085) = photoc(19) * chem_j21

!!R086 O6+ + gamma --> O7+ + e-
ratraw(iR086) = photoc(20) * chem_j21

!!R087 Na + gamma --> Na+ + e-
ratraw(iR087) = photoc(21) * chem_j21

!!R088 MG + gamma --> MG+ + e-
ratraw(iR088) = photoc(22) * chem_j21

!!R089 MG+ + gamma --> MG2+ + e-
ratraw(iR089) = photoc(23) * chem_j21 

!!R090 Si + gamma --> Si+ + e-
ratraw(iR090) = photoc(24) * chem_j21

!!R091 Si+ + gamma --> Si2+ + e-
ratraw(iR091) = photoc(25) * chem_j21

!!R092 Si2+ + gamma --> Si3+ + e-
ratraw(iR092) = photoc(26) * chem_j21

!!R093 Si3+ + gamma --> Si4+ + e-
ratraw(iR093) = photoc(27) * chem_j21

!!R094 Si4+ + gamma --> Si5+ + e-
ratraw(iR094) = photoc(28) * chem_j21

!!R095 Fe + gamma --> Fe+ + e-
ratraw(iR095) = photoc(29) * chem_j21

!!R096 Fe+ + gamma --> Fe2+ + e-
ratraw(iR096) = photoc(30) * chem_j21

!!R097 Fe2+ + gamma --> Fe3+ + e-
ratraw(iR097) = photoc(31) * chem_j21

!!R098 Fe3+ + gamma --> Fe4+ + e-
ratraw(iR098) = photoc(32) * chem_j21

if(chem_doct .eq. 1) then  !!Include Charge Transfer Rates
   !!R099 H + He2+ --> H+ + He+
   vrate = 0.0e0
   call Kingdon_CT(1.0e-5,0.0e0,0.0e0,0.0e0,1.0e3,1.0e7,ctemp,vrate)
   ratraw(iR099) = vrate * narate

   !!R100 H + C+ --> H+ + C
   vrate = 0.0e0
   call Kingdon_CT(4.88e-7,3.25,-1.12,-0.21,5.5e3,1.0e5,ctemp,vrate)
   ratraw(iR100) = vrate * narate

   !!R101 H + C2+ --> H+ + C+
   vrate = 0.0e0
   call Kingdon_CT(1.67e-4,2.79,304.72,-4.07,5.0e3,5.0e4,ctemp,vrate)
   ratraw(iR101) = vrate * narate

   !!R102 H + C3+ --> H+ + C2+
   vrate = 0.0e0
   call Kingdon_CT(3.25,0.21,0.19,-3.29,1.0e3,1.0e5,ctemp,vrate)
   ratraw(iR102) = vrate * narate

   !!R103 H + C4+ --> H+ + C3+
   vrate = 0.0e0
   call Kingdon_CT(332.46,-0.11,-0.995,-1.58e-3,1.0e1,1.0e5,ctemp,vrate)
   ratraw(iR103) = vrate * narate

   !!R104 H + N+ --> H+ + N
   vrate = 0.0e0
   call Kingdon_CT(1.01e-3,-0.29,-0.92,-8.38,1.0e2,5.0e4,ctemp,vrate)
   ratraw(iR104) = vrate * narate

   !!R105 H + N2+ --> H+ + N+
   vrate = 0.0e0
   call Kingdon_CT(3.05e-1,0.60,2.65,-0.93,1.0e3,1.0e5,ctemp,vrate)
   ratraw(iR105) = vrate * narate

   !!R106 H + N3+ --> H+ + N2+
   vrate = 0.0e0
   call Kingdon_CT(4.54,0.57,-0.65,-0.89,1.0e1,1.0e5,ctemp,vrate)
   ratraw(iR106) = vrate * narate

   !!R107 H + N4+ --> H+ + N3+
   vrate = 0.0e0
   call Kingdon_CT(2.95,0.55,-0.39,-1.07,1.0e3,1.0e6,ctemp,vrate)
   ratraw(iR107) = vrate * narate

   !!R108 H + O+ --> H+ + O
   vrate = 0.0e0
   call Kingdon_CT(1.04,3.15e-2,-0.61,-9.73,1.0e1,1.0e4,ctemp,vrate)
   ratraw(iR108) = vrate * narate

   !!R109 H + O2+ --> H+ + O+
   vrate = 0.0e0
   call Kingdon_CT(1.04,0.27,2.02,-5.92,1.0e2,1.0e5,ctemp,vrate)
   ratraw(iR109) = vrate * narate
   
   !!R110 H + O3+ --> H+ + O2+
   vrate = 0.0e0
   call Kingdon_CT(3.98,0.26,0.56,-2.62,1.0e3,5.0e4,ctemp,vrate)
   ratraw(iR110) = vrate * narate

   !!R111 H + O4+ --> H+ + O3+
   vrate = 0.0e0
   call Kingdon_CT(2.52e-1,0.63,2.08,-4.16,1.0e3,3.0e4,ctemp,vrate)
   ratraw(iR111) = vrate * narate
   
   !!R112 H + Mg2+ --> H+ + Mg+
   vrate = 0.0e0
   call Kingdon_CT(8.58e-5,2.49e-3,2.93e-2,-4.33,1.0e3,3.0e4,ctemp,vrate)
   ratraw(iR112) = vrate * narate

   !!R113 H + Si2+ --> H+ + Si+
   vrate = 0.0e0
   call Kingdon_CT(6.77,7.36e-2,-0.43,-0.11,5.0e2,1.0e5,ctemp,vrate)
   ratraw(iR113) = vrate * narate

   !!R114 H + Si3+ --> H+ + Si2+
   vrate = 0.0e0
   call Kingdon_CT(4.90e-1,-8.74e-2,-0.36,-0.79,1.0e3,3.0e4,ctemp,vrate)
   ratraw(iR114) = vrate * narate

   !!R115 H + Si4+ --> H+ + Si3+
   vrate = 0.0e0
   call Kingdon_CT(7.58,0.37,1.06,-4.09,1.0e3,5.0e4,ctemp,vrate)
   ratraw(iR115) = vrate * narate

   !!R116 H + Fe2+ --> H+ + Fe+
   vrate = 0.0e0
   call Kingdon_CT(1.26,7.72e-2,-0.41,-7.31,1.0e3,1.0e5,ctemp,vrate)
   ratraw(iR116) = vrate * narate

   !!R117 H + Fe3+ --> H+ + Fe2+
   vrate = 0.0e0
   call Kingdon_CT(3.42,0.51,-2.06,-8.99,1.0e3,1.0e5,ctemp,vrate)
   ratraw(iR117) = vrate * narate

   !!R118 H + Fe4+ --> H+ + Fe3+
   vrate = 0.0e0
   call Kingdon_CT(14.60,3.57e-2,-0.92,-0.37,1.0e3,3.0e4,ctemp,vrate)
   ratraw(iR118) = vrate * narate

   !!R119 H+ + C --> H + C+
   vrate = 0.0e0
   call Kingdon_CT5(1.07e-6,3.15,176.43,-4.29,0.0,1.0e3,1.0e5,ctemp,vrate)
   ratraw(iR119) = vrate * narate

   !!R120 H+ + N --> H + N+
   vrate = 0.0e0
   call Kingdon_CT5(4.55e-3,-0.29,-0.92,-8.38,1.086,1.0e2,5.0e4,ctemp,vrate)
   ratraw(iR120) = vrate * narate

   !!R121 H+ + O --> H + O+
   vrate = 0.0e0
   call Kingdon_CT5(7.40e-2,0.47,24.37,-0.74,0.023,1.0e1,1.0e4,ctemp,vrate)
   ratraw(iR121) = vrate * narate

   !!R122 H+ + Na --> H + Na+
   vrate = 0.0e0
   call Kingdon_CT5(3.34e-6,9.31,2632.31,-3.04,0.0e0,1.0e3,2.0e4,ctemp,vrate)
   ratraw(iR122) = vrate * narate

   !!R123 H+ + Mg --> H + Mg+
   vrate = 0.0e0
   call Kingdon_CT5(9.76e-3,3.14,55.54,-1.12,0.0e0,5.0e3,3.0e4,ctemp,vrate)
   ratraw(iR123) = vrate * narate

   !!R124 H+ + Mg+ --> H + Mg2+
   vrate = 0.0e0
   call Kingdon_CT5(7.60e-5,0.0e0,-1.97,-4.32,1.670,1.0e4,3.0e5,ctemp,vrate)
   ratraw(iR124) = vrate * narate

   !!R125 H+ + Si --> H + Si+
   vrate = 0.0e0
   call Kingdon_CT5(0.92e-3,1.15,0.80,-0.24,0.0e0,1.0e3,2.0e5,ctemp,vrate)
   ratraw(iR125) = vrate * narate

   !!R126 H+ + Si+ --> H + Si2+
   vrate = 0.0e0
   call Kingdon_CT5(2.26,7.36e-2,-0.43,-0.11,3.031,2.0e3,1.0e5,ctemp,vrate)
   ratraw(iR126) = vrate * narate

   !!R127 H+ + Fe --> H + Fe+
   vrate = 0.0e0
   call Kingdon_CT5(5.40,0.0e0,0.0e0,0.0e0,0.0e0,1.0e1,1.0e4,ctemp,vrate)
   ratraw(iR127) = vrate * narate
   
   !!R128 H+ + Fe+ --> H + Fe2+
   vrate = 0.0e0
   call Kingdon_CT5(2.10,7.72e-2,-0.41,-7.31,3.005,1.0e4,1.0e5,ctemp,vrate)
   ratraw(iR128) = vrate * narate

   !!R129 He + C3+ --> He+ + C2+
   vrate = 0.0e0
   call Kingdon_CT(1.12,0.42,-0.69,-0.34,1.0e3,1.0e7,ctemp,vrate)
   ratraw(iR129) = vrate * narate
   
   !!R130 He + C4+ --> He+ + C3+
   vrate = 0.0e0
   if(ctemp .gt. 1.0e3 .and. ctemp .le. 1.0e4) then
      call Kingdon_CT(3.12e-7,-7.37e-2,3.50e1,2.40,1.0e3,1.0e4,ctemp,vrate)
   else if(ctemp .gt. 1.0e4 .and. ctemp .le. 3.5e5) then
      call Kingdon_CT(1.49e-5,2.73,5.93,-8.74e-2,1.0e4,3.5e5,ctemp,vrate)
   else if(ctemp .gt. 3.5e5 .and. ctemp .le. 1.0e7) then
      call Kingdon_CT(5.80e-2,0.73,-0.86,-9.60e-3,3.5e5,1.0e7,ctemp,vrate)
   else
      vrate = 0.0e0
   endif
   ratraw(iR130) = vrate * narate

   !!R131 He + N2+ --> He+ + N+
   vrate = 0.0e0
   if(ctemp .gt. 1.0e3 .and. ctemp .le. 4.0e4) then
      call Kingdon_CT(4.84e-1,0.92,2.37,-1.02e1,1.0e3,4.0e4,ctemp,vrate)
   else if(ctemp .gt. 4.0e4 .and. ctemp .le. 1.0e7) then
      call Kingdon_CT(3.17,0.20,-0.72,-4.81e-2,4.0e4,1.0e7,ctemp,vrate)
   else
      vrate = 0.0e0
   endif
   ratraw(iR131) = vrate * narate

   !!R132 He + N3+ --> He+ + N2+
   vrate = 0.0e0
   call Kingdon_CT(2.05,0.23,-0.72,-0.19,1.0e3,1.0e7,ctemp,vrate)
   ratraw(iR132) = vrate * narate

   !!R133 He + N4+ --> He+ + N3+
   vrate = 0.0e0
   if(ctemp .gt. 1.0e3 .and. ctemp .le. 9.0e4) then
      call Kingdon_CT(1.26e-2,1.55,1.12e1,-7.82,1.0e3,9.0e4,ctemp,vrate)
   else if(ctemp .gt. 9.0e4 .and. ctemp .le. 1.0e7) then
      call Kingdon_CT(3.75e-1,0.54,-0.82,-2.07e-2,9.0e4,1.0e7,ctemp,vrate)
   else
      vrate = 0.0e0
   endif
   ratraw(iR133) = vrate * narate   
   
   !!R134 He + O2+ --> He+ + O+
   vrate = 0.0e0
   if(ctemp .gt. 1.0e3 .and. ctemp .le. 5.0e4) then
      call Kingdon_CT(7.10e-3,2.60,8.99,-0.78,1.0e3,5.0e4,ctemp,vrate)
   else if(ctemp .gt. 5.0e4 .and. ctemp .le. 1.0e7) then
      call Kingdon_CT(6.21e-1,0.53,-0.66,-2.22e-2,5.0e4,1.0e7,ctemp,vrate)
   else
      vrate = 0.0e0
   endif
   ratraw(iR134) = vrate * narate

   !!R135 He + O3+ --> He+ + O2+
   vrate = 0.0e0
   call Kingdon_CT(1.12,0.42,-0.71,-1.98e-2,1.0e3,1.0e7,ctemp,vrate)
   ratraw(iR135) = vrate * narate

   !!R136 He + O4+ --> He+ + O3+
   vrate = 0.0e0
   call Kingdon_CT(9.97e-1,0.40,-0.46,-0.35,1.0e3,1.0e7,ctemp,vrate)
   ratraw(iR136) = vrate * narate

   !!R137 He + Si3+ --> He+ + Si2+
   vrate = 0.0e0
   call Kingdon_CT(1.03,0.60,-0.61,-1.42,1.0e2,1.0e6,ctemp,vrate)
   ratraw(iR137) = vrate * narate
   
   !!R138 He + Si4+ --> He+ + Si3+
   vrate = 0.0e0
   call Kingdon_CT(5.75e-1,0.93,1.33,-0.29,1.0e3,5.0e5,ctemp,vrate)
   ratraw(iR138) = vrate * narate
   
   !!R139 He + Fe3+ --> He+ + Fe2+
   vrate = 0.0e0
   call Kingdon_CT(3.05e-2,0.0e0,0.0e0,0.0e0,1.0e3,1.0e7,ctemp,vrate)
   ratraw(iR139) = vrate * narate

   !!R140 He + Fe4+ --> He+ + Fe3+
   vrate = 0.0e0
   call Kingdon_CT(1.38,0.43,1.12,-0.16,1.0e3,1.0e7,ctemp,vrate)
   ratraw(iR140) = vrate * narate

   !!R141 He+ + Si --> He+ + Si+
   vrate = 0.0e0
   call Kingdon_CT(1.30,0.00,0.00,0.00,1.0e1,1.0e4,ctemp,vrate)
   ratraw(iR141) = vrate * narate

   !!R142 Fe + C+ --> C + Fe+
   vrate = 0.0e0
   call KIDA_CT(2.6e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR142) = vrate * narate

   !!R143 Mg + C+ --> C + Mg+
   vrate = 0.0e0
   call KIDA_CT(1.1e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR143) = vrate * narate
   
   !!R144 Na + C+ --> C + Na+
   vrate = 0.0e0
   call KIDA_CT(1.1e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR144) = vrate * narate

   !!R145 Si + C+ --> C + Si+
   vrate = 0.0e0
   call KIDA_CT(2.1e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR145) = vrate * narate
   
   !!R146 Na + Fe+ --> Fe + Na+
   vrate = 0.0e0
   call KIDA_CT(1.0e-11,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR146) = vrate * narate

   !!R147 Na + Mg+ --> Mg + Na+
   vrate = 0.0e0
   call KIDA_CT(1.0e-11,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR147) = vrate * narate

   !!R148 Fe + N+ --> N + Fe+
   vrate = 0.0e0
   call KIDA_CT(1.5e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR148) = vrate * narate

   !!R149 Mg + N+ --> N + Mg+
   vrate = 0.0e0
   call KIDA_CT(1.2e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR149) = vrate * narate

   !!R150 Fe + O+ --> O + Fe+
   vrate = 0.0e0
   call KIDA_CT(2.9e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR150) = vrate * narate

   !!R151 Fe + Si+ --> Si + Fe+
   vrate = 0.0e0
   call KIDA_CT(1.9e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR151) = vrate * narate

   !!R152 Mg + Si+ --> Si + Mg+
   vrate = 0.0e0
   call KIDA_CT(2.9e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR152) = vrate * narate
   
   !!R153 Na + Si+ --> Si + Na+
   vrate = 0.0e0
   call KIDA_CT(2.7e-9,0.0e0,0.0e0,10.0,280.0,ctemp,vrate)
   ratraw(iR153) = vrate * narate

endif !!Closes Charge Transfer Rates

!do i=1,ireaction
!   if(ratraw(i) .lt. 1.0e-30) then
!      ratraw(i) = 1.0e-30
!   endif
   !!print *, 'i: ', i, ' rate: ', ratraw(i)
!enddo
!write(*,'(154(1pe20.12))') ratraw,ctemp

return
end subroutine Chemistry_networkRates


subroutine Badnell_RR(A,B,TZ,TO,C,T2,temp,rrate)

implicit none
real, intent(IN)	:: A,B,TZ,TO,C,T2,temp
real, intent(OUT)	:: rrate
real 			:: BT, rate, xx1, xx2

BT = B + C * exp(-T2/temp)
xx1 = sqrt(temp/TZ)
xx2 = sqrt(temp/TO)

rrate = A * ( xx1*(1.0+xx1)**(1.0-BT)*(1.0+xx2)**(1.0+BT) )**(-1.0)
return
end subroutine

subroutine Badnell_DR5(c1,c2,c3,c4,c5,e1,e2,e3,e4,e5,temp,rrate)

implicit none
real, intent(IN)	:: c1,c2,c3,c4,c5,e1,e2,e3,e4,e5,temp
real, intent(OUT)	:: rrate
real  			:: temp_term

temp_term = 1.0/(temp**(1.50))
rrate = temp_term * (c1*exp(-e1/temp)+c2*exp(-e2/temp)+c3*exp(-e3/temp)+c4*exp(-e4/temp)+c5*exp(-e5/temp))

return
end subroutine

subroutine Badnell_DR6(c1,c2,c3,c4,c5,c6,e1,e2,e3,e4,e5,e6,temp,rrate)

implicit none
real, intent(IN)	:: c1,c2,c3,c4,c5,c6,e1,e2,e3,e4,e5,e6,temp
real, intent(OUT)	:: rrate
real  			:: temp_term

temp_term = 1.0/(temp**(1.50))
rrate = temp_term * (c1*exp(-e1/temp)+c2*exp(-e2/temp)+c3*exp(-e3/temp)+c4*exp(-e4/temp)+c5*exp(-e5/temp)+c6*exp(-e6/temp))

return
end subroutine

subroutine Badnell_DR8ev(c1,c2,c3,c4,c5,c6,c7,c8,e1,e2,e3,e4,e5,e6,e7,e8,temp,rrate)

implicit none
real, intent(IN)	:: c1,c2,c3,c4,c5,c6,c7,c8,e1,e2,e3,e4,e5,e6,e7,e8,temp
real, intent(OUT)	:: rrate
real  			:: temp_term, tev

tev = temp * 8.6173388e-5
temp_term = 1.0/(tev**(1.50))

rrate = 1.0e-11 * temp_term * (c1*exp(-e1/tev)+c2*exp(-e2/tev)+c3*exp(-e3/tev)+c4*exp(-e4/tev)+c5*exp(-e5/tev)+c6*exp(-e6/tev)+c7*exp(-e7/tev)+c8*exp(-e8/tev))

return
end subroutine

subroutine Voronov(dE,P,A,X,K,Tmin,Tmax,temp,vrate)

implicit none
real, intent(IN)	:: dE,P,A,X,K,Tmin,Tmax,temp
real, intent(OUT)	:: vrate
real  			:: tev,U,Uh,Tmaxx

tev = temp * 8.6173388e-5
U = dE/tev
Uh = U**0.5
Tmaxx = Tmax * 1.0e3

if( tev .gt. Tmin .and. tev .lt. Tmaxx) then
    vrate = A * ((1.0 + P*Uh)/(X+U))*(U**K)*exp(-U)
else
    vrate = 1.0e-30
endif

return 
end subroutine

subroutine Gu_DR(c1,c2,c3,c4,c5,c6,c7,c8,e1,e2,e3,e4,e5,e6,e7,e8,temp,grate)
implicit none
real, intent(IN)	:: c1,c2,c3,c4,c5,c6,c7,c8,e1,e2,e3,e4,e5,e6,e7,e8,temp
real, intent(OUT)	:: grate
real  			:: tev

grate = 0.0e0
tev = temp*8.6173388e-5

grate = c1*(e1/tev)**(1.5)*exp(-e1/tev) + c2*(e2/tev)**(1.5)*exp(-e2/tev) + c3*(e3/tev)**(1.5)*exp(-e3/tev) + c4*(e4/tev)**(1.5)*exp(-e4/tev) + c5*(e5/tev)**(1.5)*exp(-e5/tev) + c6*(e6/tev)**(1.5)*exp(-e6/tev) + c7*(e7/tev)**(1.5)*exp(-e7/tev) + c8*(e8/tev)**(1.5)*exp(-e8/tev)

return 
end subroutine

subroutine Mazzotta_DR(c1,c2,c3,c4,e1,e2,e3,e4,temp,mrate)
implicit none
real, intent(IN)	:: c1,c2,c3,c4,e1,e2,e3,e4,temp
real, intent(OUT)	:: mrate
real  			:: tev,temp_term

mrate = 0.0e0
tev = temp*8.6173388e-5
temp_term = 1.0/(tev**(1.5))

mrate = temp_term * ( c1*exp(-e1/tev) + c2*exp(-e2/tev) + c3*exp(-e3/tev) + c4*exp(-e4/tev) )
return
end subroutine

subroutine Shull_RR(arad,xrad,temp,srate)
implicit none
real, intent(IN)        :: arad,xrad,temp
real, intent(OUT)       :: srate

srate  = 0.0e0
srate = arad * (temp/1.0e4)**(-xrad)
return
end subroutine

subroutine Kingdon_CT(A,B,C,D,tlo,thi,ctemp,krate)
implicit none
real, intent(IN)        :: A,B,C,D,tlo,thi,ctemp
real, intent(OUT)       :: krate
real  			:: t4

t4 = min(max(ctemp,tlo),thi)
t4 = t4/1.0e4
krate = A*(t4**B)*(1.0+C*exp(D*t4))*1.0e-9

return
end subroutine

subroutine Kingdon_CT5(A,B,C,D,E,tlo,thi,ctemp,krate)
implicit none
real, intent(IN)        :: A,B,C,D,E,tlo,thi,ctemp
real, intent(OUT)       :: krate
real  			:: t4

t4 = min(max(ctemp,tlo),thi)
t4 = t4/1.0e4

krate = A*(t4**B)*(1.0+C*exp(D*t4))*1.0e-9 !!*exp(-E*1.0e4/ctemp)*1.0e-9
return
end subroutine

subroutine KIDA_CT(A,B,C,tlo,thi,ctemp,krate)
implicit none
real, intent(IN)        :: A, B, C, tlo, thi, ctemp
real, intent(OUT)       :: krate
real  			:: t4
t4 = ctemp / 1.0e4

if(ctemp .gt. tlo .and. ctemp .lt. thi) then
   krate = A*(ctemp/300.)**B*exp(-C/ctemp)
else
   krate = 0.0e0
endif
return
end subroutine
