!!****Chemisty_networkDJ
!!
!!
!!
!! NAME
!!  Chemistry_networkDJ
!!
!! SYNOPSIS
!!	Define the Jacobian for this network
!!
!! ARGUMENTS
!!	
!!  Y's : current composition
!!  dfdy: Jacobian Elements, there are a lot
!!
!!*****

subroutine Chemistry_networkDJ(tt,ys,dfdy,nlog,nphys)

use Chemistry_data

implicit none

#include "constants.h"
#include "Flash.h"


real    :: tt
integer :: nlog, nphys
!real, dimension(NSPECIES) :: ys
!real, dimension(NSPECIES,NSPECIES) :: dfdy
real, dimension(nphys,nphys) :: dfdy
real, dimension(nphys) :: ys

real, dimension(NSPECIES,NSPECIES) :: ttemp

integer ie,i,j
ie = iELEC

do i=1,nphys !!NSPECIES
   do j=1,nphys !!NSPECIES
      dfdy(i,j) = 0.0e0
   enddo
enddo

!print *, ''
!print *, 'IN CHEM_NETWORKDJ'
!do i=1,NSPECIES
!   print *, 'i: ', i, ' spec: ', ys(i)
!enddo

do i=1,nphys !!NSPECIES
   ys(i) = min(1.0e0, max(ys(i),1.0e-30))
enddo

dfdy(iH,iH)    = -ratraw(iR018)*ys(iELEC)-ratraw(iR037)*ys(iHEP)-ratraw(iR067)-ratraw(iR099)*ys(iHEPP)-ratraw(iR100)*ys(iCP)-ratraw(iR101)*ys(iC2P)-ratraw(iR102)*ys(iC3P)-ratraw(iR103)*ys(iC4P)-ratraw(iR104)*ys(iNP)-ratraw(iR105)*ys(iN2P)-ratraw(iR106)*ys(iN3P)-ratraw(iR107)*ys(iN4P)-ratraw(iR108)*ys(iOP)-ratraw(iR109)*ys(iO2P)-ratraw(iR110)*ys(iO3P)-ratraw(iR111)*ys(iO4P)-ratraw(iR112)*ys(iMG2P)-ratraw(iR113)*ys(iSI2P)-ratraw(iR114)*ys(iSI3P)-ratraw(iR115)*ys(iSI4P)-ratraw(iR116)*ys(iFE2P)-ratraw(iR117)*ys(iFE3P)-ratraw(iR118)*ys(iFE4P)
dfdy(iH,iHP)   =  ratraw(iR017)*ys(iELEC)+ratraw(iR038)*ys(iHE)+ratraw(iR067)+ratraw(iR119)*ys(iC)+ratraw(iR120)*ys(iN)+ratraw(iR121)*ys(iO)+ratraw(iR122)*ys(iNA)+ratraw(iR123)*ys(iMG)+ratraw(iR124)*ys(iMGP)+ratraw(iR125)*ys(iSI)+ratraw(iR126)*ys(iSIP)+ratraw(iR127)*ys(iFE)+ratraw(iR128)*ys(iFEP)
dfdy(iH,iHE)   =  ratraw(iR038)*ys(iHP)
dfdy(iH,iHEP)  = -ratraw(iR037)*ys(iH)
dfdy(iH,iHEPP) = -ratraw(iR099)*ys(iH)
dfdy(iH,iC)    =  ratraw(iR119)*ys(iHP)
dfdy(iH,iCP)   = -ratraw(iR100)*ys(iH)
dfdy(iH,iC2P)  = -ratraw(iR101)*ys(iH)
dfdy(iH,iC3P)  = -ratraw(iR102)*ys(iH)
dfdy(iH,iC4P)  = -ratraw(iR103)*ys(iH) 
dfdy(iH,iN)    =  ratraw(iR120)*ys(iHP)
dfdy(iH,iNP)   = -ratraw(iR104)*ys(iH)
dfdy(iH,iN2P)  = -ratraw(iR105)*ys(iH)
dfdy(iH,iN3P)  = -ratraw(iR106)*ys(iH)
dfdy(iH,iN4P)  = -ratraw(iR107)*ys(iH)
dfdy(iH,iO)    =  ratraw(iR121)*ys(iHP)
dfdy(iH,iOP)   = -ratraw(iR108)*ys(iH)
dfdy(iH,iO2P)  = -ratraw(iR109)*ys(iH)
dfdy(iH,iO3P)  = -ratraw(iR110)*ys(iH)
dfdy(iH,iO4P)  = -ratraw(iR111)*ys(iH)
dfdy(iH,iNA)   =  ratraw(iR122)*ys(iHP) 
dfdy(iH,iMG)   =  ratraw(iR123)*ys(iHP)
dfdy(iH,iMGP)  =  ratraw(iR124)*ys(iHP)
dfdy(iH,iMG2P) = -ratraw(iR112)*ys(iH)
dfdy(iH,iSI)   =  ratraw(iR125)*ys(iHP)
dfdy(iH,iSIP)  =  ratraw(iR126)*ys(iHP)
dfdy(iH,iSI2P) = -ratraw(iR113)*ys(iH)
dfdy(iH,iSI3P) = -ratraw(iR114)*ys(iH)
dfdy(iH,iSI4P) = -ratraw(iR115)*ys(iH)
dfdy(iH,iFE)   =  ratraw(iR127)*ys(iHP)
dfdy(iH,iFEP)  =  ratraw(iR128)*ys(iHP)
dfdy(iH,iFE2P) = -ratraw(iR116)*ys(iH)
dfdy(iH,iFE3P) = -ratraw(iR117)*ys(iH)
dfdy(iH,iFE4P) = -ratraw(iR118)*ys(iH)
dfdy(iH,iELEC) =  ratraw(iR017)*ys(iHP)-ratraw(iR018)*ys(iH)+ratraw(iR067)

dfdy(iHP,iH)   =  ratraw(iR018)*ys(iELEC)+ratraw(iR037)*ys(iHEP)+ratraw(iR099)*ys(iHEPP)+ratraw(iR100)*ys(iCP)+ratraw(iR101)*ys(iC2P)+ratraw(iR102)*ys(iC3P)+ratraw(iR103)*ys(iC4P)+ratraw(iR104)*ys(iNP)+ratraw(iR105)*ys(iN2P)+ratraw(iR106)*ys(iN3P)+ratraw(iR107)*ys(iN4P)+ratraw(iR108)*ys(iOP)+ratraw(iR109)*ys(iO2P)+ratraw(iR110)*ys(iO3P)+ratraw(iR111)*ys(iO4P)+ratraw(iR112)*ys(iMG2P)+ratraw(iR113)*ys(iSI2P)+ratraw(iR114)*ys(iSI3P)+ratraw(iR115)*ys(iSI4P)+ratraw(iR116)*ys(iFE2P)+ratraw(iR117)*ys(iFE3P)+ratraw(iR118)*ys(iFE4P)
dfdy(iHP,iHP)  = -ratraw(iR017)*ys(iELEC)-ratraw(iR038)*ys(iHE)-ratraw(iR119)*ys(iC)-ratraw(iR120)*ys(iN)-ratraw(iR121)*ys(iO)-ratraw(iR122)*ys(iNA)-ratraw(iR123)*ys(iMG)-ratraw(iR124)*ys(iMGP)-ratraw(iR125)*ys(iSI)-ratraw(iR126)*ys(iSIP)-ratraw(iR127)*ys(iFE)-ratraw(iR128)*ys(iFEP)
dfdy(iHP,iHE)  = -ratraw(iR038)*ys(iHP)
dfdy(iHP,iHEP) =  ratraw(iR037)*ys(iH)
dfdy(iHP,iHEPP)=  ratraw(iR099)*ys(iH)
dfdy(iHP,iC)   = -ratraw(iR119)*ys(iHP)
dfdy(iHP,iCP)  =  ratraw(iR100)*ys(iH)
dfdy(iHP,iC2P) =  ratraw(iR101)*ys(iH)
dfdy(iHP,iC3P) =  ratraw(iR102)*ys(iH)
dfdy(iHP,iC4P) =  ratraw(iR103)*ys(iH)
dfdy(iHP,iN)   = -ratraw(iR120)*ys(iHP)
dfdy(iHP,iNP)  =  ratraw(iR104)*ys(iH)
dfdy(iHP,iN2P) =  ratraw(iR105)*ys(iH)
dfdy(iHP,iN3P) =  ratraw(iR106)*ys(iH)
dfdy(iHP,iN4P) =  ratraw(iR107)*ys(iH)
dfdy(iHP,iO)   = -ratraw(iR121)*ys(iHP)
dfdy(iHP,iOP)  =  ratraw(iR108)*ys(iH)
dfdy(iHP,iO2P) =  ratraw(iR109)*ys(iH)
dfdy(iHP,iO3P) =  ratraw(iR110)*ys(iH)
dfdy(iHP,iO4P) =  ratraw(iR111)*ys(iH)
dfdy(iHP,iNA)  = -ratraw(iR122)*ys(iHP)
dfdy(iHP,iMG)  = -ratraw(iR123)*ys(iHP)
dfdy(iHP,iMGP) = -ratraw(iR124)*ys(iHP)
dfdy(iHP,iMG2P)=  ratraw(iR112)*ys(iH)
dfdy(iHP,iSI)  = -ratraw(iR125)*ys(iHP)
dfdy(iHP,iSIP) = -ratraw(iR126)*ys(iHP)
dfdy(iHP,iSI2P)=  ratraw(iR113)*ys(iH)
dfdy(iHP,iSI3P)=  ratraw(iR114)*ys(iH)
dfdy(iHP,iSI4P)=  ratraw(iR115)*ys(iH)
dfdy(iHP,iFE)  = -ratraw(iR127)*ys(iHP)
dfdy(iHP,iFEP) = -ratraw(iR128)*ys(iHP)
dfdy(iHP,iFE2P)=  ratraw(iR116)*ys(iH)
dfdy(iHP,iFE3P)=  ratraw(iR117)*ys(iH)
dfdy(iHP,iFE4P)=  ratraw(iR118)*ys(iH)
dfdy(iHP,iELEC)= -ratraw(iR017)*ys(iHP)+ratraw(iR018)*ys(iH)

dfdy(iHE,iH)   =  ratraw(iR037)*ys(iHEP)
dfdy(iHE,iHP)  = -ratraw(iR038)*ys(iHE)
dfdy(iHE,iHE)  = -ratraw(iR019)*ys(iELEC)-ratraw(iR038)*ys(iHP)-ratraw(iR068)-ratraw(iR129)*ys(iC3P)-ratraw(iR130)*ys(iC4P)-ratraw(iR131)*ys(iN2P)-ratraw(iR132)*ys(iN3P)-ratraw(iR133)*ys(iN4P)-ratraw(iR134)*ys(iO2P)-ratraw(iR135)*ys(iO3P)-ratraw(iR136)*ys(iO4P)-ratraw(iR137)*ys(iSI3P)-ratraw(iR138)*ys(iSI4P)-ratraw(iR139)*ys(iFE3P)-ratraw(iR140)*ys(iFE4P)
dfdy(iHE,iHEP) =  ratraw(iR001)*ys(iELEC)+ratraw(iR037)*ys(iH)+ratraw(iR068)+ratraw(iR141)*ys(iSI)
dfdy(iHE,iC3P) = -ratraw(iR129)*ys(iHE)
dfdy(iHE,iC4P) = -ratraw(iR130)*ys(iHE) 
dfdy(iHE,iN2P) = -ratraw(iR131)*ys(iHE)
dfdy(iHE,iN3P) = -ratraw(iR132)*ys(iHE)
dfdy(iHE,iN4P) = -ratraw(iR133)*ys(iHE)
dfdy(iHE,iO2P) = -ratraw(iR134)*ys(iHE)
dfdy(iHE,iO3P) = -ratraw(iR135)*ys(iHE)
dfdy(iHE,iO4P) = -ratraw(iR136)*ys(iHE)
dfdy(iHE,iSI)  =  ratraw(iR141)*ys(iHEP)
dfdy(iHE,iSI3P)= -ratraw(iR137)*ys(iHE)
dfdy(iHE,iSI4P)= -ratraw(iR138)*ys(iHE)
dfdy(iHE,iFE3P)= -ratraw(iR139)*ys(iHE)
dfdy(iHE,iFE4P)= -ratraw(iR140)*ys(iHE)
dfdy(iHE,iELEC)=  ratraw(iR001)*ys(iHEP)-ratraw(iR019)*ys(iHE)+ratraw(iR068)

dfdy(iHEP,iH)   = -ratraw(iR037)*ys(iHEP)+ratraw(iR099)*ys(iHEPP)
dfdy(iHEP,iHP)  =  ratraw(iR038)*ys(iHE)
dfdy(iHEP,iHE)  =  ratraw(iR019)*ys(iELEC)+ratraw(iR038)*ys(iHP)+ratraw(iR129)*ys(iC3P)+ratraw(iR130)*ys(iC4P)+ratraw(iR131)*ys(iN2P)+ratraw(iR132)*ys(iN3P)+ratraw(iR133)*ys(iN4P)+ratraw(iR134)*ys(iO2P)+ratraw(iR135)*ys(iO3P)+ratraw(iR136)*ys(iO4P)+ratraw(iR137)*ys(iSI3P)+ratraw(iR138)*ys(iSI4P)+ratraw(iR139)*ys(iFE3P)+ratraw(iR140)*ys(iFE4P)
dfdy(iHEP,iHEP) = -ratraw(iR001)*ys(iELEC)-ratraw(iR020)*ys(iELEC)-ratraw(iR037)*ys(iH)-ratraw(iR069)-ratraw(iR141)*ys(iSI)
dfdy(iHEP,iHEPP)= ratraw(iR036)*ys(iELEC)+ratraw(iR069)+ratraw(iR099)*ys(iH)
dfdy(iHEP,iC3P) =  ratraw(iR129)*ys(iHE)
dfdy(iHEP,iC4P) =  ratraw(iR130)*ys(iHE)
dfdy(iHEP,iN2P) =  ratraw(iR131)*ys(iHE)
dfdy(iHEP,iN3P) =  ratraw(iR132)*ys(iHE)
dfdy(iHEP,iN4P) =  ratraw(iR133)*ys(iHE)
dfdy(iHEP,iO2P) =  ratraw(iR134)*ys(iHE)
dfdy(iHEP,iO3P) =  ratraw(iR135)*ys(iHE)
dfdy(iHEP,iO4P) =  ratraw(iR136)*ys(iHE)
dfdy(iHEP,iSI)  = -ratraw(iR141)*ys(iHEP)
dfdy(iHEP,iSI3P)=  ratraw(iR137)*ys(iHE)
dfdy(iHEP,iSI4P)=  ratraw(iR138)*ys(iHE)
dfdy(iHEP,iFE3P)=  ratraw(iR139)*ys(iHE)
dfdy(iHEP,iFE4P)=  ratraw(iR140)*ys(iHE)
dfdy(iHEP,iELEC)= -ratraw(iR001)*ys(iHEP)+ratraw(iR019)*ys(iHE)-ratraw(iR020)*ys(iHEP)+ratraw(iR036)*ys(iHEPP)+ratraw(iR069)

dfdy(iHEPP,iH)    = -ratraw(iR099)*ys(iHEPP)
dfdy(iHEPP,iHEP)  =  ratraw(iR020)*ys(iELEC)
dfdy(iHEPP,iHEPP) = -ratraw(iR036)*ys(iELEC)-ratraw(iR099)*ys(iH)
dfdy(iHEPP,iELEC) =  ratraw(iR020)*ys(iHEP)-ratraw(iR036)*ys(iHEPP)

dfdy(iC,iH)       =  ratraw(iR100)*ys(iCP)
dfdy(iC,iHP)	  = -ratraw(iR119)*ys(iC)
dfdy(iC,iC)	  = -ratraw(iR021)*ys(iELEC)-ratraw(iR070)-ratraw(iR119)*ys(iHP)
dfdy(iC,iCP)	  =  ratraw(iR002)*ys(iELEC)+ratraw(iR070)+ratraw(iR100)*ys(iH)+ratraw(iR142)*ys(iFE)+ratraw(iR143)*ys(iMG)+ratraw(iR144)*ys(iNA)+ratraw(iR145)*ys(iSI)
dfdy(iC,iNA)	  =  ratraw(iR144)*ys(iCP) 
dfdy(iC,iMG)	  =  ratraw(iR143)*ys(iCP)
dfdy(iC,iSI)	  =  ratraw(iR145)*ys(iCP)
dfdy(iC,iFE)	  =  ratraw(iR142)*ys(iCP)
dfdy(iC,iELEC)	  =  ratraw(iR002)*ys(iCP)-ratraw(iR021)*ys(iC)+ratraw(iR070)

dfdy(iCP,iH)    = -ratraw(iR100)*ys(iCP)+ratraw(iR101)*ys(iC2P)
dfdy(iCP,iHP)	=  ratraw(iR119)*ys(iC)
dfdy(iCP,iC)	=  ratraw(iR021)*ys(iELEC)+ratraw(iR119)*ys(iHP)
dfdy(iCP,iCP)	= -ratraw(iR002)*ys(iELEC)-ratraw(iR022)*ys(iELEC)-ratraw(iR071)-ratraw(iR100)*ys(iH)-ratraw(iR142)*ys(iFE)-ratraw(iR143)*ys(iMG)-ratraw(iR144)*ys(iNA)-ratraw(iR145)*ys(iSI)
dfdy(iCP,iC2P)	=  ratraw(iR003)*ys(iELEC)+ratraw(iR071)+ratraw(iR101)*ys(iH)
dfdy(iCP,iNA)	= -ratraw(iR144)*ys(iCP) 
dfdy(iCP,iMG)	= -ratraw(iR143)*ys(iCP)
dfdy(iCP,iSI)	= -ratraw(iR145)*ys(iCP)
dfdy(iCP,iFE)	= -ratraw(iR142)*ys(iCP)
dfdy(iCP,iELEC) = -ratraw(iR002)*ys(iCP)+ratraw(iR003)*ys(iC2P)+ratraw(iR021)*ys(iC)-ratraw(iR022)*ys(iCP)+ratraw(iR071)

dfdy(iC2P,iH)   = -ratraw(iR101)*ys(iC2P)+ratraw(iR102)*ys(iC3P)
dfdy(iC2P,iHE)	=  ratraw(iR129)*ys(iC3P)
dfdy(iC2P,iCP)	=  ratraw(iR022)*ys(iELEC)
dfdy(iC2P,iC2P) = -ratraw(iR003)*ys(iELEC)-ratraw(iR023)*ys(iELEC)-ratraw(iR072)-ratraw(iR101)*ys(iH)
dfdy(iC2P,iC3P) =  ratraw(iR004)*ys(iELEC)+ratraw(iR072)+ratraw(iR102)*ys(iH)+ratraw(iR129)*ys(iHE)
dfdy(iC2P,iELEC)= -ratraw(iR003)*ys(iC2P)+ratraw(iR004)*ys(iC3P)+ratraw(iR022)*ys(iCP)-ratraw(iR023)*ys(iC2P)+ratraw(iR072)

dfdy(iC3P,iH)   = -ratraw(iR102)*ys(iC3P)+ratraw(iR103)*ys(iC4P)
dfdy(iC3P,iHE)  = -ratraw(iR129)*ys(iC3P)+ratraw(iR130)*ys(iC4P)
dfdy(iC3P,iC2P)	=  ratraw(iR023)*ys(iELEC)
dfdy(iC3P,iC3P)	= -ratraw(iR004)*ys(iELEC)-ratraw(iR024)*ys(iELEC)-ratraw(iR073)-ratraw(iR102)*ys(iH)-ratraw(iR129)*ys(iHE)
dfdy(iC3P,iC4P)	=  ratraw(iR005)*ys(iELEC)+ratraw(iR073)+ratraw(iR103)*ys(iH)+ratraw(iR130)*ys(iHE)
dfdy(iC3P,iELEC)= -ratraw(iR004)*ys(iC3P)+ratraw(iR005)*ys(iC4P)+ratraw(iR023)*ys(iC2P)-ratraw(iR024)*ys(iC3P)+ratraw(iR073)

dfdy(iC4P,iH)   = -ratraw(iR103)*ys(iC4P)
dfdy(iC4P,iHE)	= -ratraw(iR130)*ys(iC4P)
dfdy(iC4P,iC3P) =  ratraw(iR024)*ys(iELEC)
dfdy(iC4P,iC4P) = -ratraw(iR005)*ys(iELEC)-ratraw(iR025)*ys(iELEC)-ratraw(iR074)-ratraw(iR103)*ys(iH)-ratraw(iR130)*ys(iHE)
dfdy(iC4P,iC5P) =  ratraw(iR006)*ys(iELEC)+ratraw(iR074)
dfdy(iC4P,iELEC)= -ratraw(iR005)*ys(iC4P)+ratraw(iR006)*ys(iC5P)+ratraw(iR024)*ys(iC3P)-ratraw(iR025)*ys(iC4P)+ratraw(iR074)

dfdy(iC5P,iC4P) =  ratraw(iR025)*ys(iELEC)
dfdy(iC5P,iC5P) = -ratraw(iR006)*ys(iELEC)
dfdy(iC5P,iELEC)= -ratraw(iR006)*ys(iC5P)+ratraw(iR025)*ys(iC4P)

dfdy(iN,iH)     =  ratraw(iR104)*ys(iNP)
dfdy(iN,iHP)	= -ratraw(iR120)*ys(iN)
dfdy(iN,iN)	= -ratraw(iR053)*ys(iELEC)-ratraw(iR075)-ratraw(iR120)*ys(iHP)
dfdy(iN,iNP)	=  ratraw(iR039)*ys(iELEC)+ratraw(iR075)+ratraw(iR104)*ys(iH)+ratraw(iR148)*ys(iFE)+ratraw(iR149)*ys(iMG)
dfdy(iN,iMG)	=  ratraw(iR149)*ys(iNP)
dfdy(iN,iFE)	=  ratraw(iR148)*ys(iNP)
dfdy(iN,iELEC)  =  ratraw(iR039)*ys(iNP)-ratraw(iR053)*ys(iN)+ratraw(iR075)

dfdy(iNP,iH)    = -ratraw(iR104)*ys(iNP)+ratraw(iR105)*ys(iN2P)
dfdy(iNP,iHP)	=  ratraw(iR120)*ys(iN)
dfdy(iNP,iHE)	=  ratraw(iR131)*ys(iN2P)
dfdy(iNP,iN)	=  ratraw(iR053)*ys(iELEC)+ratraw(iR120)*ys(iHP)
dfdy(iNP,iNP)	= -ratraw(iR039)*ys(iELEC)-ratraw(iR054)*ys(iELEC)-ratraw(iR076)-ratraw(iR104)*ys(iH)-ratraw(iR148)*ys(iFE)-ratraw(iR149)*ys(iMG)
dfdy(iNP,iN2P)	=  ratraw(iR040)*ys(iELEC)+ratraw(iR076)+ratraw(iR105)*ys(iH)+ratraw(iR131)*ys(iHE)
dfdy(iNP,iMG)	= -ratraw(iR149)*ys(iNP)
dfdy(iNP,iFE)	= -ratraw(iR148)*ys(iNP)

dfdy(iNP,iELEC) = -ratraw(iR039)*ys(iNP)+ratraw(iR040)*ys(iN2P)+ratraw(iR053)*ys(iN)-ratraw(iR054)*ys(iNP)+ratraw(iR076)
dfdy(iN2P,iH)	= -ratraw(iR105)*ys(iN2P)+ratraw(iR106)*ys(iN3P)
dfdy(iN2P,iHE)	= -ratraw(iR131)*ys(iN2P)+ratraw(iR132)*ys(iN3P)
dfdy(iN2P,iNP)	=  ratraw(iR054)*ys(iELEC)
dfdy(iN2P,iN2P)	= -ratraw(iR040)*ys(iELEC)-ratraw(iR055)*ys(iELEC)-ratraw(iR077)-ratraw(iR105)*ys(iH)-ratraw(iR131)*ys(iHE)
dfdy(iN2P,iN3P)	=  ratraw(iR041)*ys(iELEC)+ratraw(iR077)+ratraw(iR106)*ys(iH)+ratraw(iR132)*ys(iHE)
dfdy(iN2P,iELEC)= -ratraw(iR040)*ys(iN2P)+ratraw(iR041)*ys(iN3P)+ratraw(iR054)*ys(iNP)-ratraw(iR055)*ys(iN2P)+ratraw(iR077)

dfdy(iN3P,iH)   = -ratraw(iR106)*ys(iN3P)+ratraw(iR107)*ys(iN4P) 
dfdy(iN3P,iHE)	= -ratraw(iR132)*ys(iN3P)+ratraw(iR133)*ys(iN4P)
dfdy(iN3P,iN2P) =  ratraw(iR055)*ys(iELEC)
dfdy(iN3P,iN3P) = -ratraw(iR041)*ys(iELEC)-ratraw(iR056)*ys(iELEC)-ratraw(iR078)-ratraw(iR106)*ys(iH)-ratraw(iR132)*ys(iHE)
dfdy(iN3P,iN4P) =  ratraw(iR042)*ys(iELEC)+ratraw(iR078)+ratraw(iR107)*ys(iH)+ratraw(iR133)*ys(iHE)
dfdy(iN3P,iELEC)= -ratraw(iR041)*ys(iN3P)+ratraw(iR042)*ys(iN4P)+ratraw(iR055)*ys(iN2P)-ratraw(iR056)*ys(iN3P)+ratraw(iR078)

dfdy(iN4P,iH)   = -ratraw(iR107)*ys(iN4P)
dfdy(iN4P,iHE)  = -ratraw(iR133)*ys(iN4P)
dfdy(iN4P,iN3P) =  ratraw(iR056)*ys(iELEC)
dfdy(iN4P,iN4P) = -ratraw(iR042)*ys(iELEC)-ratraw(iR057)*ys(iELEC)-ratraw(iR079)-ratraw(iR107)*ys(iH)-ratraw(iR133)*ys(iHE)
dfdy(iN4P,iN5P) =  ratraw(iR043)*ys(iELEC)+ratraw(iR079)
dfdy(iN4P,iELEC)= -ratraw(iR042)*ys(iN4P)+ratraw(iR043)*ys(iN5P)+ratraw(iR056)*ys(iN3P)-ratraw(iR057)*ys(iN4P)+ratraw(iR079)

dfdy(iN5P,iN4P) =  ratraw(iR057)*ys(iELEC)
dfdy(iN5P,iN5P) = -ratraw(iR043)*ys(iELEC)
dfdy(iN5P,iELEC)= -ratraw(iR043)*ys(iN5P)+ratraw(iR057)*ys(iN4P)

dfdy(iO,iH)     =  ratraw(iR108)*ys(iOP)
dfdy(iO,iHP)	= -ratraw(iR121)*ys(iO)
dfdy(iO,iO)	= -ratraw(iR026)*ys(iELEC)-ratraw(iR080)-ratraw(iR121)*ys(iHP)
dfdy(iO,iOP)	=  ratraw(iR007)*ys(iELEC)+ratraw(iR080)+ratraw(iR108)*ys(iH)+ratraw(iR150)*ys(iFE)
dfdy(iO,iFE)	=  ratraw(iR150)*ys(iOP)
dfdy(iO,iELEC)	=  ratraw(iR007)*ys(iOP)-ratraw(iR026)*ys(iO)+ratraw(iR080)

dfdy(iOP,iH)    = -ratraw(iR108)*ys(iOP)+ratraw(iR109)*ys(iO2P)
dfdy(iOP,iHP)	=  ratraw(iR121)*ys(iO)
dfdy(iOP,iHE)	=  ratraw(iR134)*ys(iO2P)
dfdy(iOP,iO)	=  ratraw(iR026)*ys(iELEC)+ratraw(iR121)*ys(iHP)
dfdy(iOP,iOP)	= -ratraw(iR007)*ys(iELEC)-ratraw(iR027)*ys(iELEC)-ratraw(iR081)-ratraw(iR108)*ys(iH)-ratraw(iR150)*ys(iFE)
dfdy(iOP,iO2P)	=  ratraw(iR008)*ys(iELEC)+ratraw(iR081)+ratraw(iR109)*ys(iH)+ratraw(iR134)*ys(iHE)
dfdy(iOP,iFE)	= -ratraw(iR150)*ys(iOP)
dfdy(iOP,iELEC)	= -ratraw(iR007)*ys(iOP)+ratraw(iR008)*ys(iO2P)+ratraw(iR026)*ys(iO)-ratraw(iR027)*ys(iOP)+ratraw(iR081)

dfdy(iO2P,iH)   = -ratraw(iR109)*ys(iO2P)+ratraw(iR110)*ys(iO3P)
dfdy(iO2P,iHE)	= -ratraw(iR134)*ys(iO2P)+ratraw(iR135)*ys(iO3P)
dfdy(iO2P,iOP)	=  ratraw(iR027)*ys(iELEC)
dfdy(iO2P,iO2P)	= -ratraw(iR008)*ys(iELEC)-ratraw(iR028)*ys(iELEC)-ratraw(iR082)-ratraw(iR109)*ys(iH)-ratraw(iR134)*ys(iHE)
dfdy(iO2P,iO3P)	=  ratraw(iR009)*ys(iELEC)+ratraw(iR082)+ratraw(iR110)*ys(iH)+ratraw(iR135)*ys(iHE)
dfdy(iO2P,iELEC)= -ratraw(iR008)*ys(iO2P)+ratraw(iR009)*ys(iO3P)+ratraw(iR027)*ys(iOP)-ratraw(iR028)*ys(iO2P)+ratraw(iR082)

dfdy(iO3P,iH)   = -ratraw(iR110)*ys(iO3P)+ratraw(iR111)*ys(iO4P)
dfdy(iO3P,iHE)	= -ratraw(iR135)*ys(iO3P)+ratraw(iR136)*ys(iO4P)
dfdy(iO3P,iO2P) =  ratraw(iR028)*ys(iELEC)
dfdy(iO3P,iO3P) = -ratraw(iR009)*ys(iELEC)-ratraw(iR029)*ys(iELEC)-ratraw(iR083)-ratraw(iR110)*ys(iH)-ratraw(iR135)*ys(iHE)
dfdy(iO3P,iO4P) =  ratraw(iR010)*ys(iELEC)+ratraw(iR083)+ratraw(iR111)*ys(iH)+ratraw(iR136)*ys(iHE)
dfdy(iO3P,iELEC)= -ratraw(iR009)*ys(iO3P)+ratraw(iR010)*ys(iO4P)+ratraw(iR028)*ys(iO2P)-ratraw(iR029)*ys(iO3P)+ratraw(iR083)

dfdy(iO4P,iH)   = -ratraw(iR111)*ys(iO4P)
dfdy(iO4P,iHE)  = -ratraw(iR136)*ys(iO4P)
dfdy(iO4P,iO3P) =  ratraw(iR029)*ys(iELEC)
dfdy(iO4P,iO4P) = -ratraw(iR010)*ys(iELEC)-ratraw(iR030)*ys(iELEC)-ratraw(iR084)-ratraw(iR111)*ys(iH)-ratraw(iR136)*ys(iHE)
dfdy(iO4P,iO5P) =  ratraw(iR011)*ys(iELEC)+ratraw(iR084)
dfdy(iO4P,iELEC)= -ratraw(iR010)*ys(iO4P)+ratraw(iR011)*ys(iO5P)+ratraw(iR029)*ys(iO3P)-ratraw(iR030)*ys(iO4P)+ratraw(iR084)

dfdy(iO5P,iO4P) =  ratraw(iR030)*ys(iELEC)
dfdy(iO5P,iO5P) = -ratraw(iR011)*ys(iELEC)-ratraw(iR031)*ys(iELEC)-ratraw(iR085)
dfdy(iO5P,iO6P) =  ratraw(iR012)*ys(iELEC)+ratraw(iR085)
dfdy(iO5P,iELEC)= -ratraw(iR011)*ys(iO5P)+ratraw(iR012)*ys(iO6P)+ratraw(iR030)*ys(iO4P)-ratraw(iR031)*ys(iO5P)+ratraw(iR085)

dfdy(iO6P,iO5P) =  ratraw(iR031)*ys(iELEC)
dfdy(iO6P,iO6P) = -ratraw(iR012)*ys(iELEC)-ratraw(iR032)*ys(iELEC)-ratraw(iR086)
dfdy(iO6P,iO7P) =  ratraw(iR013)*ys(iELEC)+ratraw(iR086)
dfdy(iO6P,iELEC)= -ratraw(iR012)*ys(iO6P)+ratraw(iR013)*ys(iO7P)+ratraw(iR031)*ys(iO5P)-ratraw(iR032)*ys(iO6P)+ratraw(iR086)

dfdy(iO7P,iO6P) =  ratraw(iR032)*ys(iELEC)
dfdy(iO7P,iO7P) = -ratraw(iR013)*ys(iELEC)
dfdy(iO7P,iELEC)= -ratraw(iR013)*ys(iO7P)+ratraw(iR032)*ys(iO6P)
 
dfdy(iNA,iHP)   = -ratraw(iR122)*ys(iNA)
dfdy(iNA,iCP)	= -ratraw(iR144)*ys(iNA)
dfdy(iNA,iNA)	= -ratraw(iR035)*ys(iELEC)-ratraw(iR087)-ratraw(iR122)*ys(iHP)-ratraw(iR144)*ys(iCP)-ratraw(iR146)*ys(iFEP)-ratraw(iR147)*ys(iMGP)-ratraw(iR153)*ys(iSIP)
dfdy(iNA,iNAP)	=  ratraw(iR016)*ys(iELEC)+ratraw(iR087) 
dfdy(iNA,iMGP)	= -ratraw(iR147)*ys(iNA)
dfdy(iNA,iSIP)	= -ratraw(iR153)*ys(iNA)
dfdy(iNA,iFEP)	= -ratraw(iR146)*ys(iNA)
dfdy(iNA,iELEC) =  ratraw(iR016)*ys(iNAP)-ratraw(iR035)*ys(iNA)+ratraw(iR087)

dfdy(iNAP,iHP)  =  ratraw(iR122)*ys(iNA)
dfdy(iNAP,iCP)  =  ratraw(iR144)*ys(iNA) 
dfdy(iNAP,iNA)  =  ratraw(iR035)*ys(iELEC)+ratraw(iR122)*ys(iHP)+ratraw(iR144)*ys(iCP)+ratraw(iR146)*ys(iFEP)+ratraw(iR147)*ys(iMGP)+ratraw(iR153)*ys(iSIP)
dfdy(iNAP,iNAP) = -ratraw(iR016)*ys(iELEC)
dfdy(iNAP,iMGP) =  ratraw(iR147)*ys(iNA)
dfdy(iNAP,iSIP) =  ratraw(iR153)*ys(iNA)
dfdy(iNAP,iFEP) =  ratraw(iR146)*ys(iNA) 
dfdy(iNAP,iELEC)= -ratraw(iR016)*ys(iNAP)+ratraw(iR035)*ys(iNA)

dfdy(iMG,iHP)   = -ratraw(iR123)*ys(iMG)
dfdy(iMG,iCP)	= -ratraw(iR143)*ys(iMG)
dfdy(iMG,iNP)	= -ratraw(iR149)*ys(iMG)
dfdy(iMG,iNA)	=  ratraw(iR147)*ys(iMGP)
dfdy(iMG,iMG)	= -ratraw(iR033)*ys(iELEC)-ratraw(iR088)-ratraw(iR123)*ys(iHP)-ratraw(iR143)*ys(iCP)-ratraw(iR149)*ys(iNP)-ratraw(iR152)*ys(iSIP)
dfdy(iMG,iMGP)	=  ratraw(iR014)*ys(iELEC)+ratraw(iR088)+ratraw(iR147)*ys(iNA)
dfdy(iMG,iSIP)	= -ratraw(iR152)*ys(iMG)
dfdy(iMG,iELEC) =  ratraw(iR014)*ys(iMGP)-ratraw(iR033)*ys(iMG)+ratraw(iR088)

dfdy(iMGP,iH)   =  ratraw(iR112)*ys(iMG2P)
dfdy(iMGP,iHP)	=  ratraw(iR123)*ys(iMG)-ratraw(iR124)*ys(iMGP)
dfdy(iMGP,iCP)	=  ratraw(iR143)*ys(iMG)
dfdy(iMGP,iNP)	=  ratraw(iR149)*ys(iMG)
dfdy(iMGP,iNA)	= -ratraw(iR147)*ys(iMGP)
dfdy(iMGP,iMG)	=  ratraw(iR033)*ys(iELEC)+ratraw(iR123)*ys(iHP)+ratraw(iR143)*ys(iCP)+ratraw(iR149)*ys(iNP)+ratraw(iR152)*ys(iSIP)
dfdy(iMGP,iMGP)	= -ratraw(iR014)*ys(iELEC)-ratraw(iR034)*ys(iELEC)-ratraw(iR089)-ratraw(iR124)*ys(iHP)-ratraw(iR147)*ys(iNA)
dfdy(iMGP,iMG2P)=  ratraw(iR015)*ys(iELEC)+ratraw(iR089)+ratraw(iR112)*ys(iH)
dfdy(iMGP,iSIP) =  ratraw(iR152)*ys(iMG)
dfdy(iMGP,iELEC)= -ratraw(iR014)*ys(iMGP)+ratraw(iR015)*ys(iMG2P)+ratraw(iR033)*ys(iMG)-ratraw(iR034)*ys(iMGP)+ratraw(iR089)

dfdy(iMG2P,iH)    = -ratraw(iR112)*ys(iMG2P)
dfdy(iMG2P,iHP)   =  ratraw(iR124)*ys(iMGP)
dfdy(iMG2P,iMGP)  =  ratraw(iR034)*ys(iELEC)+ratraw(iR124)*ys(iHP)
dfdy(iMG2P,iMG2P) = -ratraw(iR015)*ys(iELEC)-ratraw(iR112)*ys(iH)
dfdy(iMG2P,iELEC) = -ratraw(iR015)*ys(iMG2P)+ratraw(iR034)*ys(iMGP)

dfdy(iSI,iHP)     = -ratraw(iR125)*ys(iSI)
dfdy(iSI,iHEP)	  = -ratraw(iR141)*ys(iSI)
dfdy(iSI,iCP)	  = -ratraw(iR145)*ys(iSI)
dfdy(iSI,iNA)	  =  ratraw(iR153)*ys(iSIP)
dfdy(iSI,iMG)	  =  ratraw(iR152)*ys(iSIP)
dfdy(iSI,iSI)	  = -ratraw(iR058)*ys(iELEC)-ratraw(iR090)-ratraw(iR125)*ys(iHP)-ratraw(iR141)*ys(iHEP)-ratraw(iR145)*ys(iCP)
dfdy(iSI,iSIP)	  =  ratraw(iR044)*ys(iELEC)+ratraw(iR090)+ratraw(iR151)*ys(iFE)+ratraw(iR152)*ys(iMG)+ratraw(iR153)*ys(iNA)
dfdy(iSI,iFE)	  =  ratraw(iR151)*ys(iSIP)
dfdy(iSI,iELEC)   =  ratraw(iR044)*ys(iSIP)-ratraw(iR058)*ys(iSI)+ratraw(iR090)

dfdy(iSIP,iH)     =  ratraw(iR113)*ys(iSI2P)
dfdy(iSIP,iHP)	  =  ratraw(iR125)*ys(iSI)-ratraw(iR126)*ys(iSIP)
dfdy(iSIP,iCP)	  =  ratraw(iR145)*ys(iSI)
dfdy(iSIP,iNA)	  = -ratraw(iR153)*ys(iSIP)
dfdy(iSIP,iMG)	  = -ratraw(iR152)*ys(iSIP)
dfdy(iSIP,iSI)	  =  ratraw(iR058)*ys(iELEC)+ratraw(iR125)*ys(iHP)+ratraw(iR145)*ys(iCP)
dfdy(iSIP,iSIP)	  = -ratraw(iR044)*ys(iELEC)-ratraw(iR059)*ys(iELEC)-ratraw(iR091)-ratraw(iR126)*ys(iHP)-ratraw(iR151)*ys(iFE)-ratraw(iR152)*ys(iMG)-ratraw(iR153)*ys(iNA)
dfdy(iSIP,iSI2P)  =  ratraw(iR045)*ys(iELEC)+ratraw(iR091)+ratraw(iR113)*ys(iH)
dfdy(iSIP,iFE)	  = -ratraw(iR151)*ys(iSIP)
dfdy(iSIP,iELEC)  = -ratraw(iR044)*ys(iSIP)+ratraw(iR045)*ys(iSI2P)+ratraw(iR058)*ys(iSI)-ratraw(iR059)*ys(iSIP)+ratraw(iR091)

dfdy(iSI2P,iH)    = -ratraw(iR113)*ys(iSI2P)+ratraw(iR114)*ys(iSI3P)
dfdy(iSI2P,iHP)	  =  ratraw(iR126)*ys(iSIP)
dfdy(iSI2P,iHE)	  =  ratraw(iR137)*ys(iSI3P)
dfdy(iSI2P,iHEP)  =  ratraw(iR141)*ys(iSI)
dfdy(iSI2P,iSI)	  =  ratraw(iR141)*ys(iHEP)
dfdy(iSI2P,iSIP)  =  ratraw(iR059)*ys(iELEC)+ratraw(iR126)*ys(iHP)
dfdy(iSI2P,iSI2P) = -ratraw(iR045)*ys(iELEC)-ratraw(iR060)*ys(iELEC)-ratraw(iR092)-ratraw(iR113)*ys(iH)
dfdy(iSI2P,iSI3P) =  ratraw(iR046)*ys(iELEC)+ratraw(iR092)+ratraw(iR114)*ys(iH)+ratraw(iR137)*ys(iHE)
dfdy(iSI2P,iELEC) = -ratraw(iR045)*ys(iSI2P)+ratraw(iR046)*ys(iSI3P)+ratraw(iR059)*ys(iSIP)-ratraw(iR060)*ys(iSI2P)+ratraw(iR092)
dfdy(iSI3P,iH)	  = -ratraw(iR114)*ys(iSI3P)+ratraw(iR115)*ys(iSI4P)
 
dfdy(iSI3P,iHE)   = -ratraw(iR137)*ys(iSI3P)+ratraw(iR138)*ys(iSI4P)
dfdy(iSI3P,iSI2P) =  ratraw(iR060)*ys(iELEC)
dfdy(iSI3P,iSI3P) = -ratraw(iR046)*ys(iELEC)-ratraw(iR061)*ys(iELEC)-ratraw(iR093)-ratraw(iR114)*ys(iH)-ratraw(iR137)*ys(iHE)
dfdy(iSI3P,iSI4P) =  ratraw(iR047)*ys(iELEC)+ratraw(iR093)+ratraw(iR115)*ys(iH)+ratraw(iR138)*ys(iHE)
dfdy(iSI3P,iELEC) = -ratraw(iR046)*ys(iSI3P)+ratraw(iR047)*ys(iSI4P)+ratraw(iR060)*ys(iSI2P)-ratraw(iR061)*ys(iSI3P)+ratraw(iR093)

dfdy(iSI4P,iH)    = -ratraw(iR115)*ys(iSI4P)
dfdy(iSI4P,iHE)	  = -ratraw(iR138)*ys(iSI4P)
dfdy(iSI4P,iSI3P) =  ratraw(iR061)*ys(iELEC)
dfdy(iSI4P,iSI4P) = -ratraw(iR047)*ys(iELEC)-ratraw(iR062)*ys(iELEC)-ratraw(iR094)-ratraw(iR115)*ys(iH)-ratraw(iR138)*ys(iHE)
dfdy(iSI4P,iSI5P) =  ratraw(iR048)*ys(iELEC)+ratraw(iR094)
dfdy(iSI4P,iELEC) = -ratraw(iR047)*ys(iSI4P)+ratraw(iR048)*ys(iSI5P)+ratraw(iR061)*ys(iSI3P)-ratraw(iR062)*ys(iSI4P)+ratraw(iR094)

dfdy(iSI5P,iSI4P) =  ratraw(iR062)*ys(iELEC)
dfdy(iSI5P,iSI5P) = -ratraw(iR048)*ys(iELEC)
dfdy(iSI5P,iELEC) = -ratraw(iR048)*ys(iSI5P)+ratraw(iR062)*ys(iSI4P)

dfdy(iFE,iHP)     = -ratraw(iR127)*ys(iFE)
dfdy(iFE,iCP)	  = -ratraw(iR142)*ys(iFE)
dfdy(iFE,iNP)	  = -ratraw(iR148)*ys(iFE)
dfdy(iFE,iOP)	  = -ratraw(iR150)*ys(iFE)
dfdy(iFE,iNA)	  =  ratraw(iR146)*ys(iFEP)
dfdy(iFE,iSIP)	  = -ratraw(iR151)*ys(iFE)
dfdy(iFE,iFE)	  = -ratraw(iR063)*ys(iELEC)-ratraw(iR095)-ratraw(iR127)*ys(iHP)-ratraw(iR142)*ys(iCP)-ratraw(iR148)*ys(iNP)-ratraw(iR150)*ys(iOP)-ratraw(iR151)*ys(iSIP)
dfdy(iFE,iFEP)	  =  ratraw(iR049)*ys(iELEC)+ratraw(iR095)+ratraw(iR146)*ys(iNA)
dfdy(iFE,iELEC)	  =  ratraw(iR049)*ys(iFEP)-ratraw(iR063)*ys(iFE)+ratraw(iR095)

dfdy(iFEP,iH)     =  ratraw(iR116)*ys(iFE2P)
dfdy(iFEP,iHP)	  =  ratraw(iR127)*ys(iFE)-ratraw(iR128)*ys(iFEP)
dfdy(iFEP,iCP)	  =  ratraw(iR142)*ys(iFE)
dfdy(iFEP,iNP)	  =  ratraw(iR148)*ys(iFE)
dfdy(iFEP,iOP)	  =  ratraw(iR150)*ys(iFE)
dfdy(iFEP,iNA)	  = -ratraw(iR146)*ys(iFEP)
dfdy(iFEP,iSIP)	  =  ratraw(iR151)*ys(iFE)
dfdy(iFEP,iFE)	  =  ratraw(iR063)*ys(iELEC)+ratraw(iR127)*ys(iHP)+ratraw(iR142)*ys(iCP)+ratraw(iR148)*ys(iNP)+ratraw(iR150)*ys(iOP)+ratraw(iR151)*ys(iSIP)
dfdy(iFEP,iFEP)	  = -ratraw(iR049)*ys(iELEC)-ratraw(iR064)*ys(iELEC)-ratraw(iR096)-ratraw(iR128)*ys(iHP)-ratraw(iR146)*ys(iNA)
dfdy(iFEP,iFE2P)  =  ratraw(iR050)*ys(iELEC)+ratraw(iR096)+ratraw(iR116)*ys(iH)
dfdy(iFEP,iELEC)  = -ratraw(iR049)*ys(iFEP)+ratraw(iR050)*ys(iFE2P)+ratraw(iR063)*ys(iFE)-ratraw(iR064)*ys(iFEP)+ratraw(iR096)

dfdy(iFE2P,iH)	  = -ratraw(iR116)*ys(iFE2P)+ratraw(iR117)*ys(iFE3P)
dfdy(iFE2P,iHP)	  =  ratraw(iR128)*ys(iFEP)
dfdy(iFE2P,iHE)	  =  ratraw(iR139)*ys(iFE3P)
dfdy(iFE2P,iFEP)  =  ratraw(iR064)*ys(iELEC)+ratraw(iR128)*ys(iHP)
dfdy(iFE2P,iFE2P) = -ratraw(iR050)*ys(iELEC)-ratraw(iR065)*ys(iELEC)-ratraw(iR097)-ratraw(iR116)*ys(iH)
dfdy(iFE2P,iFE3P) =  ratraw(iR051)*ys(iELEC)+ratraw(iR097)+ratraw(iR117)*ys(iH)+ratraw(iR139)*ys(iHE)
dfdy(iFE2P,iELEC) = -ratraw(iR050)*ys(iFE2P)+ratraw(iR051)*ys(iFE3P)+ratraw(iR064)*ys(iFEP)-ratraw(iR065)*ys(iFE2P)+ratraw(iR097)

dfdy(iFE3P,iH)	  = -ratraw(iR117)*ys(iFE3P)+ratraw(iR118)*ys(iFE4P)
dfdy(iFE3P,iHE)	  = -ratraw(iR139)*ys(iFE3P)+ratraw(iR140)*ys(iFE4P)
dfdy(iFE3P,iFE2P) =  ratraw(iR065)*ys(iELEC)
dfdy(iFE3P,iFE3P) = -ratraw(iR051)*ys(iELEC)-ratraw(iR066)*ys(iELEC)-ratraw(iR098)-ratraw(iR117)*ys(iH)-ratraw(iR139)*ys(iHE)
dfdy(iFE3P,iFE4P) =  ratraw(iR052)*ys(iELEC)+ratraw(iR098)+ratraw(iR118)*ys(iH)+ratraw(iR140)*ys(iHE)
dfdy(iFE3P,iELEC) = -ratraw(iR051)*ys(iFE3P)+ratraw(iR052)*ys(iFE4P)+ratraw(iR065)*ys(iFE2P)-ratraw(iR066)*ys(iFE3P)+ratraw(iR098)

dfdy(iFE4P,iH)	  = -ratraw(iR118)*ys(iFE4P)
dfdy(iFE4P,iHE)	  = -ratraw(iR140)*ys(iFE4P)
dfdy(iFE4P,iFE3P) =  ratraw(iR066)*ys(iELEC)
dfdy(iFE4P,iFE4P) = -ratraw(iR052)*ys(iELEC)-ratraw(iR118)*ys(iH)-ratraw(iR140)*ys(iHE)
dfdy(iFE4P,iELEC) = -ratraw(iR052)*ys(iFE4P)+ratraw(iR066)*ys(iFE3P)

dfdy(iELEC,iH)	  = -ratraw(iR018)*ys(iELEC)+ratraw(iR018)*ys(iELEC)+ratraw(iR018)*ys(iELEC)
dfdy(iELEC,iHP)	  = -ratraw(iR017)*ys(iELEC)
dfdy(iELEC,iHE)	  = -ratraw(iR019)*ys(iELEC)+ratraw(iR019)*ys(iELEC)+ratraw(iR019)*ys(iELEC)
dfdy(iELEC,iHEP)  = -ratraw(iR001)*ys(iELEC)-ratraw(iR020)*ys(iELEC)+ratraw(iR020)*ys(iELEC)+ratraw(iR020)*ys(iELEC)
dfdy(iELEC,iHEPP) = -ratraw(iR036)*ys(iELEC)
dfdy(iELEC,iC)	  = -ratraw(iR021)*ys(iELEC)+ratraw(iR021)*ys(iELEC)+ratraw(iR021)*ys(iELEC)
dfdy(iELEC,iCP)	  = -ratraw(iR002)*ys(iELEC)-ratraw(iR022)*ys(iELEC)+ratraw(iR022)*ys(iELEC)+ratraw(iR022)*ys(iELEC)
dfdy(iELEC,iC2P)  = -ratraw(iR003)*ys(iELEC)-ratraw(iR023)*ys(iELEC)+ratraw(iR023)*ys(iELEC)+ratraw(iR023)*ys(iELEC)
dfdy(iELEC,iC3P)  = -ratraw(iR004)*ys(iELEC)-ratraw(iR024)*ys(iELEC)+ratraw(iR024)*ys(iELEC)+ratraw(iR024)*ys(iELEC)
dfdy(iELEC,iC4P)  = -ratraw(iR005)*ys(iELEC)-ratraw(iR025)*ys(iELEC)+ratraw(iR025)*ys(iELEC)+ratraw(iR025)*ys(iELEC)
dfdy(iELEC,iC5P)  = -ratraw(iR006)*ys(iELEC)
dfdy(iELEC,iN)	  = -ratraw(iR053)*ys(iELEC)+ratraw(iR053)*ys(iELEC)+ratraw(iR053)*ys(iELEC)
dfdy(iELEC,iNP)	  = -ratraw(iR039)*ys(iELEC)-ratraw(iR054)*ys(iELEC)+ratraw(iR054)*ys(iELEC)+ratraw(iR054)*ys(iELEC)
dfdy(iELEC,iN2P)  = -ratraw(iR040)*ys(iELEC)-ratraw(iR055)*ys(iELEC)+ratraw(iR055)*ys(iELEC)+ratraw(iR055)*ys(iELEC)
dfdy(iELEC,iN3P)  = -ratraw(iR041)*ys(iELEC)-ratraw(iR056)*ys(iELEC)+ratraw(iR056)*ys(iELEC)+ratraw(iR056)*ys(iELEC)
dfdy(iELEC,iN4P)  = -ratraw(iR042)*ys(iELEC)-ratraw(iR057)*ys(iELEC)+ratraw(iR057)*ys(iELEC)+ratraw(iR057)*ys(iELEC)
dfdy(iELEC,iN5P)  = -ratraw(iR043)*ys(iELEC)
dfdy(iELEC,iO)	  = -ratraw(iR026)*ys(iELEC)+ratraw(iR026)*ys(iELEC)+ratraw(iR026)*ys(iELEC)
dfdy(iELEC,iOP)	  = -ratraw(iR007)*ys(iELEC)-ratraw(iR027)*ys(iELEC)+ratraw(iR027)*ys(iELEC)+ratraw(iR027)*ys(iELEC)
dfdy(iELEC,iO2P)  = -ratraw(iR008)*ys(iELEC)-ratraw(iR028)*ys(iELEC)+ratraw(iR028)*ys(iELEC)+ratraw(iR028)*ys(iELEC)
dfdy(iELEC,iO3P)  = -ratraw(iR009)*ys(iELEC)-ratraw(iR029)*ys(iELEC)+ratraw(iR029)*ys(iELEC)+ratraw(iR029)*ys(iELEC)
dfdy(iELEC,iO4P)  = -ratraw(iR010)*ys(iELEC)-ratraw(iR030)*ys(iELEC)+ratraw(iR030)*ys(iELEC)+ratraw(iR030)*ys(iELEC)
dfdy(iELEC,iO5P)  = -ratraw(iR011)*ys(iELEC)-ratraw(iR031)*ys(iELEC)+ratraw(iR031)*ys(iELEC)+ratraw(iR031)*ys(iELEC)
dfdy(iELEC,iO6P)  = -ratraw(iR012)*ys(iELEC)-ratraw(iR032)*ys(iELEC)+ratraw(iR032)*ys(iELEC)+ratraw(iR032)*ys(iELEC)
dfdy(iELEC,iO7P)  = -ratraw(iR013)*ys(iELEC)
dfdy(iELEC,iNA)	  = -ratraw(iR035)*ys(iELEC)+ratraw(iR035)*ys(iELEC)+ratraw(iR035)*ys(iELEC)
dfdy(iELEC,iNAP)  = -ratraw(iR016)*ys(iELEC)
dfdy(iELEC,iMG)	  = -ratraw(iR033)*ys(iELEC)+ratraw(iR033)*ys(iELEC)+ratraw(iR033)*ys(iELEC)
dfdy(iELEC,iMGP)  = -ratraw(iR014)*ys(iELEC)-ratraw(iR034)*ys(iELEC)+ratraw(iR034)*ys(iELEC)+ratraw(iR034)*ys(iELEC)
dfdy(iELEC,iMG2P) = -ratraw(iR015)*ys(iELEC)
dfdy(iELEC,iSI)	  = -ratraw(iR058)*ys(iELEC)+ratraw(iR058)*ys(iELEC)+ratraw(iR058)*ys(iELEC)
dfdy(iELEC,iSIP)  = -ratraw(iR044)*ys(iELEC)-ratraw(iR059)*ys(iELEC)+ratraw(iR059)*ys(iELEC)+ratraw(iR059)*ys(iELEC)
dfdy(iELEC,iSI2P) = -ratraw(iR045)*ys(iELEC)-ratraw(iR060)*ys(iELEC)+ratraw(iR060)*ys(iELEC)+ratraw(iR060)*ys(iELEC)
dfdy(iELEC,iSI3P) = -ratraw(iR046)*ys(iELEC)-ratraw(iR061)*ys(iELEC)+ratraw(iR061)*ys(iELEC)+ratraw(iR061)*ys(iELEC)
dfdy(iELEC,iSI4P) = -ratraw(iR047)*ys(iELEC)-ratraw(iR062)*ys(iELEC)+ratraw(iR062)*ys(iELEC)+ratraw(iR062)*ys(iELEC)
dfdy(iELEC,iSI5P) = -ratraw(iR048)*ys(iELEC)
dfdy(iELEC,iFE)	  = -ratraw(iR063)*ys(iELEC)+ratraw(iR063)*ys(iELEC)+ratraw(iR063)*ys(iELEC)
dfdy(iELEC,iFEP)  = -ratraw(iR049)*ys(iELEC)-ratraw(iR064)*ys(iELEC)+ratraw(iR064)*ys(iELEC)+ratraw(iR064)*ys(iELEC)
dfdy(iELEC,iFE2P) = -ratraw(iR050)*ys(iELEC)-ratraw(iR065)*ys(iELEC)+ratraw(iR065)*ys(iELEC)+ratraw(iR065)*ys(iELEC)
dfdy(iELEC,iFE3P) = -ratraw(iR051)*ys(iELEC)-ratraw(iR066)*ys(iELEC)+ratraw(iR066)*ys(iELEC)+ratraw(iR066)*ys(iELEC)
dfdy(iELEC,iFE4P) = -ratraw(iR052)*ys(iELEC)
dfdy(iELEC,iELEC) = -ratraw(iR001)*ys(iHEP)-ratraw(iR002)*ys(iCP)-ratraw(iR003)*ys(iC2P)-ratraw(iR004)*ys(iC3P)-ratraw(iR005)*ys(iC4P)-ratraw(iR006)*ys(iC5P)-ratraw(iR007)*ys(iOP)-ratraw(iR008)*ys(iO2P)-ratraw(iR009)*ys(iO3P)-ratraw(iR010)*ys(iO4P)-ratraw(iR011)*ys(iO5P)-ratraw(iR012)*ys(iO6P)-ratraw(iR013)*ys(iO7P)-ratraw(iR014)*ys(iMGP)-ratraw(iR015)*ys(iMG2P)-ratraw(iR016)*ys(iNAP)-ratraw(iR017)*ys(iHP)-ratraw(iR018)*ys(iH)+ratraw(iR018)*ys(iH)+ratraw(iR018)*ys(iH)-ratraw(iR019)*ys(iHE)+ratraw(iR019)*ys(iHE)+ratraw(iR019)*ys(iHE)-ratraw(iR020)*ys(iHEP)+ratraw(iR020)*ys(iHEP)+ratraw(iR020)*ys(iHEP)-ratraw(iR021)*ys(iC)+ratraw(iR021)*ys(iC)+ratraw(iR021)*ys(iC)-ratraw(iR022)*ys(iCP)+ratraw(iR022)*ys(iCP)+ratraw(iR022)*ys(iCP)-ratraw(iR023)*ys(iC2P)+ratraw(iR023)*ys(iC2P)+ratraw(iR023)*ys(iC2P)-ratraw(iR024)*ys(iC3P)+ratraw(iR024)*ys(iC3P)+ratraw(iR024)*ys(iC3P)-ratraw(iR025)*ys(iC4P)+ratraw(iR025)*ys(iC4P)+ratraw(iR025)*ys(iC4P)-ratraw(iR026)*ys(iO)+ratraw(iR026)*ys(iO)+ratraw(iR026)*ys(iO)-ratraw(iR027)*ys(iOP)+ratraw(iR027)*ys(iOP)+ratraw(iR027)*ys(iOP)-ratraw(iR028)*ys(iO2P)+ratraw(iR028)*ys(iO2P)+ratraw(iR028)*ys(iO2P)-ratraw(iR029)*ys(iO3P)+ratraw(iR029)*ys(iO3P)+ratraw(iR029)*ys(iO3P)-ratraw(iR030)*ys(iO4P)+ratraw(iR030)*ys(iO4P)+ratraw(iR030)*ys(iO4P)-ratraw(iR031)*ys(iO5P)+ratraw(iR031)*ys(iO5P)+ratraw(iR031)*ys(iO5P)-ratraw(iR032)*ys(iO6P)+ratraw(iR032)*ys(iO6P)+ratraw(iR032)*ys(iO6P)-ratraw(iR033)*ys(iMG)+ratraw(iR033)*ys(iMG)+ratraw(iR033)*ys(iMG)-ratraw(iR034)*ys(iMGP)+ratraw(iR034)*ys(iMGP)+ratraw(iR034)*ys(iMGP)-ratraw(iR035)*ys(iNA)+ratraw(iR035)*ys(iNA)+ratraw(iR035)*ys(iNA)-ratraw(iR036)*ys(iHEPP)-ratraw(iR039)*ys(iNP)-ratraw(iR040)*ys(iN2P)-ratraw(iR041)*ys(iN3P)-ratraw(iR042)*ys(iN4P)-ratraw(iR043)*ys(iN5P)-ratraw(iR044)*ys(iSIP)-ratraw(iR045)*ys(iSI2P)-ratraw(iR046)*ys(iSI3P)-ratraw(iR047)*ys(iSI4P)-ratraw(iR048)*ys(iSI5P)-ratraw(iR049)*ys(iFEP)-ratraw(iR050)*ys(iFE2P)-ratraw(iR051)*ys(iFE3P)-ratraw(iR052)*ys(iFE4P)-ratraw(iR053)*ys(iN)+ratraw(iR053)*ys(iN)+ratraw(iR053)*ys(iN)-ratraw(iR054)*ys(iNP)+ratraw(iR054)*ys(iNP)+ratraw(iR054)*ys(iNP)-ratraw(iR055)*ys(iN2P)+ratraw(iR055)*ys(iN2P)+ratraw(iR055)*ys(iN2P)-ratraw(iR056)*ys(iN3P)+ratraw(iR056)*ys(iN3P)+ratraw(iR056)*ys(iN3P)-ratraw(iR057)*ys(iN4P)+ratraw(iR057)*ys(iN4P)+ratraw(iR057)*ys(iN4P)-ratraw(iR058)*ys(iSI)+ratraw(iR058)*ys(iSI)+ratraw(iR058)*ys(iSI)-ratraw(iR059)*ys(iSIP)+ratraw(iR059)*ys(iSIP)+ratraw(iR059)*ys(iSIP)-ratraw(iR060)*ys(iSI2P)+ratraw(iR060)*ys(iSI2P)+ratraw(iR060)*ys(iSI2P)-ratraw(iR061)*ys(iSI3P)+ratraw(iR061)*ys(iSI3P)+ratraw(iR061)*ys(iSI3P)-ratraw(iR062)*ys(iSI4P)+ratraw(iR062)*ys(iSI4P)+ratraw(iR062)*ys(iSI4P)-ratraw(iR063)*ys(iFE)+ratraw(iR063)*ys(iFE)+ratraw(iR063)*ys(iFE)-ratraw(iR064)*ys(iFEP)+ratraw(iR064)*ys(iFEP)+ratraw(iR064)*ys(iFEP)-ratraw(iR065)*ys(iFE2P)+ratraw(iR065)*ys(iFE2P)+ratraw(iR065)*ys(iFE2P)-ratraw(iR066)*ys(iFE3P)+ratraw(iR066)*ys(iFE3P)+ratraw(iR066)*ys(iFE3P)


return
end subroutine Chemistry_networkDJ