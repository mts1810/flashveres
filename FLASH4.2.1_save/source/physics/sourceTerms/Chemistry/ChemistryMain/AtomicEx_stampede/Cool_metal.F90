!!****Cool_metal
!!
!!
!!
!! NAME
!!  Cool_hydrogen
!!
!! SYNOPSIS
!!	Calculates the cooling due to other metals
!!  	Total cooling rate is returned as cout_other
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_metal(temp,rho,mfrac_in,tindex,nin,cout_other)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_other, mfrac_in
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: otherterm, nhh

!! This will calculate the cooing from hydrogen
!! We are linearaly interpolating along the cooling functions

cout_other = 0.0e0
otherterm = 0.0e0
nhh = nin(iH) + nin(iHP)

!! All of these are n_ion * n_elec * CF / rho

if(tindex == clength) then
   otherterm = cother(clength)
else if (tindex == 1) then
   otherterm = cother(1)
else
   otherterm = cother(tindex) + (cother(tindex+1)-cother(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
endif

otherterm = otherterm * nhh  * nin(iELEC) 
if(otherterm .ne. otherterm) otherterm = 1.0e-30
cout_other = mfrac_in*otherterm/rho
if(cout_other .ne. cout_other) cout_other = 1.0e-30

return 
end subroutine Cool_metal
