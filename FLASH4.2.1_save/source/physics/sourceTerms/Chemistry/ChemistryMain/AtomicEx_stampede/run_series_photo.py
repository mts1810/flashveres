from yt.config import ytcfg
ytcfg["yt","loglevel"] = "50"
from yt.mods import *
import numpy as np
import subprocess
import glob


def _hspec(field,data):
    return data['h   ']
add_field("hspec",function=_hspec)
def _hpspec(field,data):
    return data['hp  ']
add_field("hpspec",function=_hpspec)
def _hespec(field,data):
    return data['he  ']
add_field("hespec",function=_hespec)
def _hepspec(field,data):
    return data['hep ']
add_field("hepspec",function=_hepspec)
def _heppspec(field,data):
    return data['hepp']
add_field("heppspec",function=_heppspec)
def _cspec(field,data):
    return data['c   ']
add_field("cspec",function=_cspec)
def _cpspec(field,data):
    return data['cp  ']
add_field("cpspec",function=_cpspec)
def _c2pspec(field,data):
    return data['c2p ']
add_field("c2pspec",function=_c2pspec)
def _c3pspec(field,data):
    return data['c3p ']
add_field("c3pspec",function=_c3pspec)
def _c4pspec(field,data):
    return data['c4p ']
add_field("c4pspec",function=_c4pspec)
def _c5pspec(field,data):
    return data['c5p ']
add_field("c5pspec",function=_c5pspec)
def _ospec(field,data):
    return data['o   ']
add_field("ospec",function=_ospec)
def _opspec(field,data):
    return data['op  ']
add_field("opspec",function=_opspec)
def _o2pspec(field,data):
    return data['o2p ']
add_field("o2pspec",function=_o2pspec)
def _o3pspec(field,data):
    return data['o3p ']
add_field("o3pspec",function=_o3pspec)
def _o4pspec(field,data):
    return data['o4p ']
add_field("o4pspec",function=_o4pspec)
def _o5pspec(field,data):
    return data['o5p ']
add_field("o5pspec",function=_o5pspec)
def _o6pspec(field,data):
    return data['o6p ']
add_field("o6pspec",function=_o6pspec)
def _o7pspec(field,data):
    return data['o7p ']
add_field("o7pspec",function=_o7pspec)
def _nespec(field,data):
    return data['ne  ']
add_field("nespec",function=_nespec)
def _nepspec(field,data):
    return data['nep ']
add_field("nepspec",function=_nepspec)
def _ne2pspec(field,data):
    return data['ne2p']
add_field("ne2pspec",function=_ne2pspec)
def _ne3pspec(field,data):
    return data['ne3p']
add_field("ne3pspec",function=_ne3pspec)
def _ne4pspec(field,data):
    return data['ne4p']
add_field("ne4pspec",function=_ne4pspec)
def _ne5pspec(field,data):
    return data['ne5p']
add_field("ne5pspec",function=_ne5pspec)
def _ne6pspec(field,data):
    return data['ne6p']
add_field("ne6pspec",function=_ne6pspec)
def _ne7pspec(field,data):
    return data['ne7p']
add_field("ne7pspec",function=_ne7pspec)
def _ne8pspec(field,data):
    return data['ne8p']
add_field("ne8pspec",function=_ne8pspec)
def _ne9pspec(field,data):
    return data['ne9p']
add_field("ne9pspec",function=_ne9pspec)
def _naspec(field,data):
    return data['na  ']
add_field("naspec",function=_naspec)
def _napspec(field,data):
    return data['nap ']
add_field("napspec",function=_napspec)
def _mgspec(field,data):
    return data['mg  ']
add_field("mgspec",function=_mgspec)
def _mgpspec(field,data):
    return data['mgp ']
add_field("mgpspec",function=_mgpspec)
def _mg2pspec(field,data):
    return data['mg2p']
add_field("mg2pspec",function=_mg2pspec)
def _nspec(field,data):
    return data['n   ']
add_field("nspec",function=_nspec)
def _npspec(field,data):
    return data['np  ']
add_field("npspec",function=_npspec)
def _n2pspec(field,data):
    return data['n2p ']
add_field("n2pspec",function=_n2pspec)
def _n3pspec(field,data):
    return data['n3p ']
add_field("n3pspec",function=_n3pspec)
def _n4pspec(field,data):
    return data['n4p ']
add_field("n4pspec",function=_n4pspec)
def _n5pspec(field,data):
    return data['n5p ']
add_field("n5pspec",function=_n5pspec)
def _sispec(field,data):
    return data['si  ']
add_field("sispec",function=_sispec)
def _sipspec(field,data):
    return data['sip ']
add_field("sipspec",function=_sipspec)
def _si2pspec(field,data):
    return data['si2p']
add_field("si2pspec",function=_si2pspec)
def _si3pspec(field,data):
    return data['si3p']
add_field("si3pspec",function=_si3pspec)
def _si4pspec(field,data):
    return data['si4p']
add_field("si4pspec",function=_si4pspec)
def _si5pspec(field,data):
    return data['si5p']
add_field("si5pspec",function=_si5pspec)
def _fespec(field,data):
    return data['fe  ']
add_field("fespec",function=_fespec)
def _fepspec(field,data):
    return data['fep ']
add_field("fepspec",function=_fepspec)
def _fe2pspec(field,data):
    return data['fe2p']
add_field("fe2pspec",function=_fe2pspec)
def _fe3pspec(field,data):
    return data['fe3p']
add_field("fe3pspec",function=_fe3pspec)
def _fe4pspec(field,data):
    return data['fe4p']
add_field("fe4pspec",function=_fe4pspec)
def _elecspec(field,data):
    return data['elec']
add_field("elecspec",function=_elecspec)

def write_par(dens,temp,j21):
    fileout = open('flash.par','w')
    fileout.write('smallp                      = 1.0000000000000E-30 \n')
    fileout.write('smlrho                      = 1.0000000000000E-40 \n')
    fileout.write('smallu                      = 1.0000000000000E-10 \n')
    fileout.write('smallt                      = 1.0000000000000E-10 \n')
    fileout.write('#	checkpoint file output parameters \n')
    fileout.write('checkpointFileIntervalTime = 1.0e14 \n')
    fileout.write('checkpointFileIntervalStep = 0 \n')
    fileout.write('checkpointFileNumber = 0 \n')
    fileout.write('#	plotfile output parameters \n')
    fileout.write('plotfileIntervalTime = 0. \n')
    fileout.write('plotfileIntervalStep = 0 \n')
    fileout.write('plotfileNumber = 0 \n')
    fileout.write('plot_var_1 = "Velx" \n')
    fileout.write('# go for nend steps or tmax seconds, whichever comes first \n')
    fileout.write('nend            = 1000000 \n')
    fileout.write('tmax		= 1.0e16 \n')
    fileout.write('# for starting a new run \n')
    fileout.write('restart         = .false. \n')
    fileout.write('# initial, and minimum timesteps \n')
    fileout.write('dtmin           = 1.0e9 \n')
    fileout.write('dtmax		= 2.50e12 \n')
    fileout.write('dtinit		= 2.50e10 \n')
    fileout.write('# Grid geometry \n')
    fileout.write('geometry = "cartesian" \n')
    fileout.write('# Size of computational volume \n')
    fileout.write('xmin		= -5.0e+18 \n')
    fileout.write('xmax		=  5.0e+18 \n')
    fileout.write('ymin		= -5.0e+18 \n')
    fileout.write('ymax		=  5.0e+18 \n')
    fileout.write('zmin		= -5.0e+18 \n')
    fileout.write('zmax		=  5.0e+18 \n')
    fileout.write('# Boundary conditions \n')
    fileout.write('xl_boundary_type = "outflow" \n')
    fileout.write('xr_boundary_type = "outflow" \n')
    fileout.write('yl_boundary_type = "outflow" \n')
    fileout.write('yr_boundary_type = "outflow" \n')
    fileout.write('zl_boundary_type = "outflow" \n')
    fileout.write('zr_boundary_type = "outflow" \n')
    fileout.write('grav_boundary_type = "isolated" \n')
    fileout.write('# Variables for refinement test \n')
    fileout.write('refine_var_1    = "dens" \n')
    fileout.write('refine_var_2    = "pres" \n')
    fileout.write('refine_var_3    = "velx" \n')
    fileout.write('refine_var_4    = "vely" \n')
    fileout.write('# Refinement levels \n')
    fileout.write('lrefine_max     = 1 \n')
    fileout.write('lrefine_min     = 1 \n')
    fileout.write('sim_nblockx         = 1.0 \n')
    fileout.write('sim_nblocky         = 1.0 \n')
    fileout.write('sim_nblockz         = 1.0 \n')
    fileout.write('# Hydrodynamics parameters \n')
    fileout.write('cfl	        = 0.8 \n')
    fileout.write('# Simulation-specific parameters \n')
    fileout.write('basenm          = "Chem_" \n')
    fileout.write('run_number      = "001" \n')
    fileout.write('run_comment     = "hydrostatic spherical cluster" \n')
    fileout.write('log_file        = "cluster.log" \n')
    fileout.write('#Parameters for the Chemistry \n')
    fileout.write('sim_xH    = 0.63495e0 \n')
    fileout.write('sim_xHP   = 0.07055e0 \n')
    fileout.write('sim_xHE   = 0.25398e0 \n')
    fileout.write('sim_xHEP  = 0.02822e0 \n')
    fileout.write('sim_xHEPP = 0.00e0    \n')
    fileout.write('sim_xC    = 1.872e-3  \n')
    fileout.write('sim_xCP   = 2.080e-4  \n')
    fileout.write('sim_xC2P  = 0.00e0    \n')
    fileout.write('sim_xC3P  = 0.00e0    \n')
    fileout.write('sim_xC4P  = 0.00e0    \n')
    fileout.write('sim_xC5P  = 0.00e0    \n')
    fileout.write('sim_xO    = 4.97e-3   \n')
    fileout.write('sim_xOP   = 5.53e-4   \n')
    fileout.write('sim_xO2P  = 0.00e0    \n')
    fileout.write('sim_xO3P  = 0.00e0    \n')
    fileout.write('sim_xO4P  = 0.00e0    \n')
    fileout.write('sim_xO5P  = 0.00e0    \n')
    fileout.write('sim_xO6P  = 0.00e0    \n')
    fileout.write('sim_xO7P  = 0.00e0    \n')
    fileout.write('sim_xNE   = 1.27e-3   \n')
    fileout.write('sim_xNEP  = 1.41e-4   \n')
    fileout.write('sim_xNE2P = 0.00e0    \n')
    fileout.write('sim_xNE3P = 0.00e0    \n')
    fileout.write('sim_xNE4P = 0.00e0    \n')
    fileout.write('sim_xNE5P = 0.00e0    \n')
    fileout.write('sim_xNE6P = 0.00e0    \n')
    fileout.write('sim_xNE7P = 0.00e0    \n')
    fileout.write('sim_xNE8P = 0.00e0    \n')
    fileout.write('sim_xNE9P = 0.00e0    \n')
    fileout.write('sim_xNA   = 3.32e-5   \n')
    fileout.write('sim_xNAP  = 2.98e-6   \n')
    fileout.write('sim_xMG   = 5.87e-4   \n')
    fileout.write('sim_xMGP  = 0.00e0    \n')
    fileout.write('sim_xMG2P = 0.00e0    \n')
    fileout.write('sim_xN    = 7.56e-4   \n')
    fileout.write('sim_xNP   = 8.40e-5   \n')
    fileout.write('sim_xN2P  = 0.0e0     \n')
    fileout.write('sim_xN3P  = 0.0e0     \n')
    fileout.write('sim_xN4P  = 0.0e0     \n')
    fileout.write('sim_xN5P  = 0.0e0     \n')
    fileout.write('sim_xSI   = 6.17e-4   \n')
    fileout.write('sim_xSIP  = 6.85e-5   \n')
    fileout.write('sim_xSI2P = 0.0e0     \n')
    fileout.write('sim_xSI3P = 0.0e0     \n')
    fileout.write('sim_xSI4P = 0.0e0     \n')
    fileout.write('sim_xSI5P = 0.0e0     \n')
    fileout.write('sim_xFE   = 9.27e-4   \n')
    fileout.write('sim_xFEP  = 1.03e-4   \n')
    fileout.write('sim_xFE2P = 0.0e0     \n')
    fileout.write('sim_xFE3P = 0.0e0     \n')
    fileout.write('sim_xFE4P = 0.0e0     \n')
    fileout.write('sim_xELEC = 0.00e0    \n')  
    fileout.write('sim_meta  = 1.00e0    \n')
    fileout.write('# Test Cloudy \n')
    ln = 'sim_c_temp = %6.4e \n'%(temp)
    fileout.write(ln)
    ln = 'sim_c_den = %6.4e \n'%(dens)
    fileout.write(ln)
    fileout.write('# Chem time Constant \n')
    fileout.write('sim_chem_time = 0.1 \n')
    fileout.write('sim_shock_time = 6.0e13 \n')
    fileout.write('sim_cool_time = 0.1 \n')
    fileout.write('doCool = 0 \n')
    fileout.write('mCool  = 0 \n')
    fileout.write('ccCase = 1 \n')
    fileout.write('useChem = .true. \n')
    fileout.write('chem_redshift = 0.0e0 \n')
    fileout.write('chem_doct = 1 \n')
    fileout.write('chem_ip = 1 \n')
    ln = 'chem_j21 = %6.4e \n'%(j21)
    fileout.write(ln)
    #fileout.write('chem_j21 = 0.0e0 \n')
    fileout.close()


def get_finaldata(i):
    files = glob.glob("Chem_hdf5_chk_0[0-9][0-9][0-9]")
    files.sort()
    data = np.zeros((67,len(files)))

    for ii in range(len(files)):                 
    	pf = load(files[ii])
        dd = pf.h.all_data()
    
        data[0 ][ii] = dd.quantities['WeightedAverageQuantity']('temp','CellVolume')
        data[1 ][ii] = dd.quantities['WeightedAverageQuantity']('hspec','CellVolume')
        data[2 ][ii] = dd.quantities['WeightedAverageQuantity']('hpspec','CellVolume')
        hspec = data[1 ][ii] + data[2 ][ii]
        data[1 ][ii] = data[1 ][ii]/hspec
        data[2 ][ii] = data[2 ][ii]/hspec

        data[3 ][ii] = dd.quantities['WeightedAverageQuantity']('hespec','CellVolume')
        data[4 ][ii] = dd.quantities['WeightedAverageQuantity']('hepspec','CellVolume')
        data[5 ][ii] = dd.quantities['WeightedAverageQuantity']('heppspec','CellVolume')
        hespec = data[3 ][ii] + data[4 ][ii] + data[5 ][ii]
        data[3 ][ii] = data[3 ][ii]/hespec
        data[4 ][ii] = data[4 ][ii]/hespec
        data[5 ][ii] = data[5 ][ii]/hespec

        data[6 ][ii] = dd.quantities['WeightedAverageQuantity']('cspec','CellVolume')
        data[7 ][ii] = dd.quantities['WeightedAverageQuantity']('cpspec','CellVolume')
        data[8 ][ii] = dd.quantities['WeightedAverageQuantity']('c2pspec','CellVolume')
        data[9 ][ii] = dd.quantities['WeightedAverageQuantity']('c3pspec','CellVolume')
        data[10][ii] = dd.quantities['WeightedAverageQuantity']('c4pspec','CellVolume')
        data[11][ii] = dd.quantities['WeightedAverageQuantity']('c5pspec','CellVolume')
        cspec = data[6 ][ii] + data[7 ][ii] + data[8 ][ii] + data[9 ][ii] + data[10][ii] + data[11][ii]
        data[6 ][ii] = data[6 ][ii]/cspec
        data[7 ][ii] = data[7 ][ii]/cspec
        data[8 ][ii] = data[8 ][ii]/cspec
        data[9 ][ii] = data[9 ][ii]/cspec
        data[10][ii] = data[10][ii]/cspec
        data[11][ii] = data[11][ii]/cspec

        data[35][ii] = dd.quantities['WeightedAverageQuantity']('nspec','CellVolume')
        data[36][ii] = dd.quantities['WeightedAverageQuantity']('npspec','CellVolume')
        data[37][ii] = dd.quantities['WeightedAverageQuantity']('n2pspec','CellVolume')
        data[38][ii] = dd.quantities['WeightedAverageQuantity']('n3pspec','CellVolume')
        data[39][ii] = dd.quantities['WeightedAverageQuantity']('n4pspec','CellVolume')
        data[40][ii] = dd.quantities['WeightedAverageQuantity']('n5pspec','CellVolume')
        nspec = data[40][ii]+data[35][ii]+data[36][ii]+data[37][ii]+data[38][ii]+data[39][ii]
        data[35][ii] = data[35][ii]/nspec
        data[36][ii] = data[36][ii]/nspec
        data[37][ii] = data[37][ii]/nspec
        data[38][ii] = data[38][ii]/nspec
        data[39][ii] = data[39][ii]/nspec
        data[40][ii] = data[40][ii]/nspec

        data[12][ii] = dd.quantities['WeightedAverageQuantity']('ospec','CellVolume')
        data[13][ii] = dd.quantities['WeightedAverageQuantity']('opspec','CellVolume')
        data[14][ii] = dd.quantities['WeightedAverageQuantity']('o2pspec','CellVolume')
        data[15][ii] = dd.quantities['WeightedAverageQuantity']('o3pspec','CellVolume')
        data[16][ii] = dd.quantities['WeightedAverageQuantity']('o4pspec','CellVolume')
        data[17][ii] = dd.quantities['WeightedAverageQuantity']('o5pspec','CellVolume')
        data[18][ii] = dd.quantities['WeightedAverageQuantity']('o6pspec','CellVolume')
        data[19][ii] = dd.quantities['WeightedAverageQuantity']('o7pspec','CellVolume')
        ospec = data[12][ii] + data[13][ii] + data[14][ii] + data[15][ii] + data[16][ii] + data[17][ii] + data[18][ii] + data[19][ii]
        data[12][ii] = data[12][ii]/ospec
        data[13][ii] = data[13][ii]/ospec
        data[14][ii] = data[14][ii]/ospec
        data[15][ii] = data[15][ii]/ospec
        data[16][ii] = data[16][ii]/ospec
        data[17][ii] = data[17][ii]/ospec
        data[18][ii] = data[18][ii]/ospec
        data[19][ii] = data[19][ii]/ospec

        data[20][ii] = dd.quantities['WeightedAverageQuantity']('nespec','CellVolume')
        data[21][ii] = dd.quantities['WeightedAverageQuantity']('nepspec','CellVolume')
        data[22][ii] = dd.quantities['WeightedAverageQuantity']('ne2pspec','CellVolume')
        data[23][ii] = dd.quantities['WeightedAverageQuantity']('ne3pspec','CellVolume')
        data[24][ii] = dd.quantities['WeightedAverageQuantity']('ne4pspec','CellVolume')
        data[25][ii] = dd.quantities['WeightedAverageQuantity']('ne5pspec','CellVolume')
        data[26][ii] = dd.quantities['WeightedAverageQuantity']('ne6pspec','CellVolume')
        data[27][ii] = dd.quantities['WeightedAverageQuantity']('ne7pspec','CellVolume')
        data[28][ii] = dd.quantities['WeightedAverageQuantity']('ne8pspec','CellVolume')
        data[29][ii] = dd.quantities['WeightedAverageQuantity']('ne9pspec','CellVolume')
        nespec = data[20][ii] + data[21][ii] + data[22][ii] + data[23][ii] + data[24][ii] + data[25][ii] + data[26][ii] + data[27][ii] + data[28][ii] + data[29][ii]
        data[20][ii] = data[20][ii]/nespec
        data[21][ii] = data[21][ii]/nespec
        data[22][ii] = data[22][ii]/nespec
        data[23][ii] = data[23][ii]/nespec
        data[24][ii] = data[24][ii]/nespec
        data[25][ii] = data[25][ii]/nespec
        data[26][ii] = data[26][ii]/nespec
        data[27][ii] = data[27][ii]/nespec
        data[28][ii] = data[28][ii]/nespec
        data[29][ii] = data[29][ii]/nespec

        data[30][ii] = dd.quantities['WeightedAverageQuantity']('naspec','CellVolume')
        data[31][ii] = dd.quantities['WeightedAverageQuantity']('napspec','CellVolume')
        naspec = data[30][ii] + data[31][ii]
        data[30][ii] = data[30][ii]/naspec
        data[31][ii] = data[31][ii]/naspec

        data[32][ii] = dd.quantities['WeightedAverageQuantity']('mgspec','CellVolume')
        data[33][ii] = dd.quantities['WeightedAverageQuantity']('mgpspec','CellVolume')
        data[34][ii] = dd.quantities['WeightedAverageQuantity']('mg2pspec','CellVolume')
        mgspec = data[32][ii] + data[33][ii] + data[34][ii]
        data[32][ii] = data[32][ii]/mgspec
        data[33][ii] = data[33][ii]/mgspec
        data[34][ii] = data[34][ii]/mgspec
 
        data[41][ii] = dd.quantities['WeightedAverageQuantity']('sispec','CellVolume')
        data[42][ii] = dd.quantities['WeightedAverageQuantity']('sipspec','CellVolume')
        data[43][ii] = dd.quantities['WeightedAverageQuantity']('si2pspec','CellVolume')
        data[44][ii] = dd.quantities['WeightedAverageQuantity']('si3pspec','CellVolume')
        data[45][ii] = dd.quantities['WeightedAverageQuantity']('si4pspec','CellVolume')
        data[46][ii] = dd.quantities['WeightedAverageQuantity']('si5pspec','CellVolume')
        sispec = data[41][ii]+data[42][ii]+data[43][ii]+data[44][ii]+data[45][ii]+data[46][ii]
        data[41][ii] = data[41][ii]/sispec
        data[42][ii] = data[42][ii]/sispec
        data[43][ii] = data[43][ii]/sispec
        data[44][ii] = data[44][ii]/sispec
        data[45][ii] = data[45][ii]/sispec
        data[46][ii] = data[46][ii]/sispec

        data[47][ii] = dd.quantities['WeightedAverageQuantity']('fespec','CellVolume')
        data[48][ii] = dd.quantities['WeightedAverageQuantity']('fepspec','CellVolume')
        data[49][ii] = dd.quantities['WeightedAverageQuantity']('fe2pspec','CellVolume')
        data[50][ii] = dd.quantities['WeightedAverageQuantity']('fe3pspec','CellVolume')
        data[51][ii] = dd.quantities['WeightedAverageQuantity']('fe4pspec','CellVolume')
        fespec = data[47][ii]+data[48][ii]+data[49][ii]+data[50][ii]+data[51][ii]
        data[47][ii] = data[47][ii]/fespec
        data[48][ii] = data[48][ii]/fespec
        data[49][ii] = data[49][ii]/fespec
        data[50][ii] = data[50][ii]/fespec
        data[51][ii] = data[51][ii]/fespec

        data[52][ii] = dd.quantities['WeightedAverageQuantity']('elecspec','CellVolume')

        data[53][ii] = dd.quantities['WeightedAverageQuantity']('lh  ','CellVolume')
        data[54][ii] = dd.quantities['WeightedAverageQuantity']('lhe ','CellVolume')
        data[55][ii] = dd.quantities['WeightedAverageQuantity']('lc  ','CellVolume')
        data[56][ii] = dd.quantities['WeightedAverageQuantity']('ln  ','CellVolume')
        data[57][ii] = dd.quantities['WeightedAverageQuantity']('lo  ','CellVolume')
        data[58][ii] = dd.quantities['WeightedAverageQuantity']('lne ','CellVolume')
        data[59][ii] = dd.quantities['WeightedAverageQuantity']('lna ','CellVolume')
        data[60][ii] = dd.quantities['WeightedAverageQuantity']('lmg ','CellVolume')
        data[61][ii] = dd.quantities['WeightedAverageQuantity']('lsi ','CellVolume')
        data[62][ii] = dd.quantities['WeightedAverageQuantity']('lfe ','CellVolume')
        data[63][ii] = dd.quantities['WeightedAverageQuantity']('lmet','CellVolume')
        data[64][ii] = dd.quantities['WeightedAverageQuantity']('lfl ','CellVolume')
        data[65][ii] = dd.quantities['WeightedAverageQuantity']('lph ','CellVolume') 

        data[66][ii] = pf.current_time
    ln = 'flash_photo_%i.txt'%(i)
    np.savetxt(ln,data)

def run_flash():
    ln = './flash4'
    subprocess.call(ln,shell=True)

def del_chem():
    ln = 'rm Chem_hdf5_*'
    subprocess.call(ln,shell=True) 

def main():
    temp = np.array([1.0e4,3.16e4,1.0e5,3.16e5,1.0e6,3.16e6,1.0e7])
    
    #FOR NO UV
    #dens = np.array([2.0e-20]) #[2.37e-26]) #,5.0e-27,1.0e-26,5.0e-26,1.0e-25])
    #j21 = np.array([0.0e0])  #2.4e-4])

    #FOR UV
    dens = np.array([2.37e-26])
    j21  = np.array([2.4e-2]) #[2.4e-4])

    ndens = len(dens)
    ntemp = len(temp)
    nj21  = len(j21)

    for i in range(ntemp):
        write_par(dens[0],temp[i],j21[0])
        run_flash()
        get_finaldata(i)
        del_chem()

if __name__ == "__main__":
    main()
