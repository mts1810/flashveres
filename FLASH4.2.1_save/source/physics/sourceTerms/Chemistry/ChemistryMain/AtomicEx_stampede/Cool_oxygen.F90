!!****Cool_oxygen
!!
!!
!!
!! NAME
!!  Cool_helium
!!
!! SYNOPSIS
!!	Calculates the cooling due to oxygen
!!  	Total cooling rate is returned as cout_o
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_oxygen(temp,rho,tindex,nin,cout_o)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_o
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: o0term, o1term, o2term, o3term, o4term, o5term, o6term, ohiterm

!! This will calculate the cooing from hydrogen
!! We are linearaly interpolating along the cooling functions

cout_o = 0.0e0
o0term = 0.0e0
o1term = 0.0e0
o2term = 0.0e0
o3term = 0.0e0
o4term = 0.0e0
o5term = 0.0e0
o6term = 0.0e0
ohiterm = 0.0e0

!!O0 first. All of these are n_ion * n_elec * CF / rho
!!O7 has been altered to include contribution from higher ionization states


if(tindex == clength) then
   o0term = coolo0(clength)
   o1term = coolo1(clength)
   o2term = coolo2(clength)
   o3term = coolo3(clength)
   o4term = coolo4(clength)
   o5term = coolo5(clength)
   o6term = coolo6(clength)
   ohiterm = coolohi(clength)
else if (tindex == 1) then
   o0term = coolo0(1)
   o1term = coolo1(1)
   o2term = coolo2(1)
   o3term = coolo3(1)
   o4term = coolo4(1)
   o5term = coolo5(1)
   o6term = coolo6(1)
   ohiterm = coolohi(1)
else
   o0term  = coolo0(tindex)  + (coolo0(tindex+1) - coolo0(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   o1term  = coolo1(tindex)  + (coolo1(tindex+1) - coolo1(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   o2term  = coolo2(tindex)  + (coolo2(tindex+1) - coolo2(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   o3term  = coolo3(tindex)  + (coolo3(tindex+1) - coolo3(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   o4term  = coolo4(tindex)  + (coolo4(tindex+1) - coolo4(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   o5term  = coolo5(tindex)  + (coolo5(tindex+1) - coolo5(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   o6term  = coolo6(tindex)  + (coolo6(tindex+1) - coolo6(tindex) )  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   ohiterm = coolohi(tindex) + (coolohi(tindex+1)- coolohi(tindex))  *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
endif

o0term  = o0term  * nin(iO)   * nin(iELEC) 
if(o0term .ne. o0term) o0term = 1.0e-30
o1term  = o1term  * nin(iOP)  * nin(iELEC) 
if(o1term .ne. o1term) o1term = 1.0e-30
o2term  = o2term  * nin(iO2P) * nin(iELEC) 
if(o2term .ne. o2term) o2term = 1.0e-30
o3term  = o3term  * nin(iO3P) * nin(iELEC) 
if(o3term .ne. o3term) o3term = 1.0e-30
o4term  = o4term  * nin(iO4P) * nin(iELEC) 
if(o4term .ne. o4term) o4term = 1.0e-30
o5term  = o5term  * nin(iO5P) * nin(iELEC) 
if(o5term .ne. o5term) o5term = 1.0e-30
o6term  = o6term  * nin(iO6P) * nin(iELEC) 
if(o6term .ne. o6term) o6term = 1.0e-30
ohiterm = ohiterm * nin(iO7P) * nin(iELEC) 
if(ohiterm .ne. ohiterm) ohiterm = 1.0e-30

cout_o = (o0term+o1term+o2term+o3term+o4term+o5term+o6term+ohiterm)/rho
if(cout_o .ne. cout_o) cout_o = 1.0e-30

return 
end subroutine Cool_oxygen
