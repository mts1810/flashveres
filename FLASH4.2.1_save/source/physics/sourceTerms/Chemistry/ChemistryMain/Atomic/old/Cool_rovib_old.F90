!!****Cool_rovib
!!
!!
!!
!! NAME
!!  Cool_rovib
!!
!! SYNOPSIS
!!  
!!
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_rovib(temp_in,rho_in,nin,cool_rout,div_v,lco)

use Chemistry_data
use Cool_data
use Driver_interface, ONLY : Driver_abortFlash

implicit none
#include "constants.h"
#include "Flash.h"


real	:: temp_in,rho_in,cool_rout,div_v,lco
real, dimension(NSPECIES) :: nin
real :: ne_h2, ne_co, nm_h2o, nm_h2, nm_co
real :: ctemp
integer :: iLo0, iLo1  !! Upper and lower indicies for
integer :: thlow, thhigh, tclow, tchigh  !!High and low integers 
integer :: i, iTemp, iNH, bail
integer :: itlow, ithigh, inlow, inhigh
real :: sigmah2, sigmah, ve, ke, kh2
real :: Lo, Llte, nhalf, alpha, Lall
real :: k1,k2,k3,k4
real :: c1,c2,c3,c4
real :: term1, term2, term3, term4

real :: lcoe, lcoo, lh2oe, lh2oo


!!Here is where you would calc div_v, but for now assume some value
!!MAKE SURE THAT DIV_V IS NOT ZERO!
!!div_v = 1.0e0
!print*,'div_v: ', div_v
ctemp = temp_in
iTemp = 11
iNH = 10
cool_rout = 0.0e0

itlow  = 0
ithigh = 0
tclow  = 0
tchigh = 0
bail = 0
k1 = 0.0e0
k2 = 0.0e0
k3 = 0.0e0
k4 = 0.0e0
term1 = 0.0e0
term2 = 0.0e0
term3 = 0.0e0
term4 = 0.0e0

sigmah  = 2.3e-15
sigmah2 = 3.3e-16*(ctemp/1000.0)**(-0.25)
ve = 1.03e4*(ctemp)**(0.5)
ke  = 10**(-8.020 + 15.749*(ctemp)**(-0.166667) - 47.137*(ctemp)**(-0.333) + 76.648*ctemp**(-0.5) - 60.191*ctemp**(-0.666))
kh2 = 7.4e-12*ctemp**(0.5)

!write(*,'(6(A,1pe10.3))'), ' sigmah: ', sigmah, ' sigmah2: ', sigmah2,' ve: ', ve, ' ke:', ke, ' kh2:', kh2, ' div: ', div_v


ne_co = nin(iH2) + sqrt(2.0)*(sigmah/sigmah2)*nin(iH) + (1.3e-8/(sigmah2*ve))*nin(iELEC)
ne_h2 = nin(iH2) + 10.0*nin(iH) + (ke/kh2)*nin(iELEC)
!print *,'ne_h2: ', ne_h2

nm_h2o = log10(nin(iH2O)/div_v)
nm_co  = log10(nin(iCO)/div_v)
nm_h2  = log10(nin(iH2)/div_v)

!write(*,'(4(A,1pe10.3))'), 'n_h2o: ', nin(iH2O), ' n_co: ', nin(iCO), ' n_h2: ', nin(iH2), ' ctemp: ', ctemp
!write(*,'(4(A,1pe10.3))'), 'nm_h2o: ', nm_h2o, ' nm_co: ', nm_co, ' nm_h2: ', nm_h2, ' ctemp: ', ctemp

!!Ok, need to get positions in temperature and optical depth space
!!H2O first

ithigh = -1
itlow = -1
inhigh = -1
itlow = -1
!!I have 3 cases:
!! We are bracketed in the temperature and density space, so we do a bilinear interp
!! We are bracketed in Temp but not in NH, so we do a 1-D interp in Temp
!! We are bracketed in NH but not in Temp, so we do a 1-D interp in NH

!write(*,'(2(A,1pe10.3))'), 'h2otemp1: ', h2o_temp(1), ' h2otempitemp: ', h2o_temp(iTemp)
!write(*,'(2(A,1pe10.3))'), 'h2on1: ', logN_h2o(1), ' h2onihn: ', logN_h2o(iNH)

!Case 1: Perfectely Bracketed
if( ctemp < h2o_temp(iTemp) .and. ctemp > h2o_temp(1) .and. nm_h2o < logN_h2o(iNH) .and. nm_h2o > logN_h2o(1) ) then
	do i=2,iTemp
		if(ctemp < h2o_temp(i) .and. ctemp > h2o_temp(i-1) ) then
			ithigh = i
			itlow = i-1
!			!write(*,'(3(A,1pe10.3))'), 'ctemp: ', ctemp, ' h2o_low: ', h2o_temp(itlow), ' h2o_high: ', h2o_temp(ithigh)
			exit
		endif
	enddo
	do i=2,iNH
		if(nm_h2o < logN_h2o(i) .and. nm_h2o > logN_h2o(i-1) ) then
			inhigh = i
			inlow = i-1
			exit
		endif	
	enddo
!	print *, ' Perfectely Bracketed: H2O Rot '
!	write(*,'(4(A,I))'), 'ithigh: ', ithigh, ' itlow: ', itlow, ' inhigh: ', inhigh, ' inlow: ', inlow
!	write(*,'(4(A,1pe10.3))'), 'thigh: ', h2o_temp(ithigh), ' tlow: ', h2o_temp(itlow), ' nhigh: ', logN_h2o(inhigh), ' nlow: ', logN_h2o(inlow)
!	write(*,'(2(A,1pe10.3))'), 'Lo_l: ' ,h2o_rL0(itlow), ' Lo_h: ', h2o_rL0(ithigh)
	c1 = 0.0e0
	c2 = 0.0e0
	c3 = 0.0e0
	c4 = 0.0e0
	c1 = 10**(-h2o_rL0(itlow))
	c2 = 10**(-h2o_rL0(ithigh))
	c3 = (ctemp - h2o_temp(itlow))
	if(c3 .lt. 1.0e-5) c3 = 0.0e0
	c4 = (h2o_temp(ithigh) - h2o_temp(itlow))
	if(c4 .eq. 0.0e0) c4 = 1.0
	
!        write(*,'(5(A,1pe10.3))'), 'c1: ', c1, ' c2:', c2, ' c3:' ,c3 , ' c4: ', c4, ' ctemp: ', ctemp
	
	Lo = c1 + (c3/c4)*(c2-c1)
!	!print *, 'Lo: ', log10(Lo)
!	!print *, 'Calling abort()'
!	call abort()
	
	k1 = (h2o_temp(ithigh)-ctemp)/(h2o_temp(ithigh)-h2o_temp(itlow))
	k2 = (ctemp-h2o_temp(itlow))/(h2o_temp(ithigh)-h2o_temp(itlow))
	k3 = (10**(logN_h2o(inhigh))-10**(nm_h2o))/(10**(logN_h2o(inhigh))-10**(logN_h2o(inlow)))
	k4 = (10**(nm_h2o)-10**(logN_h2o(inlow)))/(10**(logN_h2o(inhigh))-10**(logN_h2o(inlow)))

	!write(*,'(5(A,1pe10.3))'), 'k1: ', k1, ' k2:', k2, ' k3:' ,k3 , ' k4: ', k4, ' ctemp: ', ctemp
	
	Llte =  k3*(k1*(10**(-h2o_rLlte(itlow,inlow)))+k2*(10**(-h2o_rLlte(ithigh,inlow)))) + &
	      & k4*(k1*(10**(-h2o_rLlte(itlow,inhigh)))+k2*(10**(-h2o_rLlte(ithigh,inhigh))))


	nhalf =  k3*(k1*(10**(h2o_rn12(itlow,inlow)))+k2*(10**(h2o_rn12(ithigh,inlow)))) + &
	      &  k4*(k1*(10**(h2o_rn12(itlow,inhigh)))+k2*(10**(h2o_rn12(ithigh,inhigh))))
	      
	alpha =  k3*(k1*h2o_ralp(itlow,inlow)+k2*h2o_ralp(ithigh,inlow)) + &
	       & k4*(k1*h2o_ralp(itlow,inhigh)+k2*h2o_ralp(ithigh,inhigh))

	!write(*,'(3(A,1pe10.3))'), 'Llte: ', -log10(Llte), ' nhalf: ', log10(nhalf), ' alpha: ', alpha
!!	!print *,'Calling abort'
!!	call abort()

else if( ctemp > h2o_temp(iTemp) .or. ctemp < h2o_temp(1) .and.  nm_h2o > logN_h2o(iNH) .or. nm_h2o < logN_h2o(1) ) then
	!print *, 'Way out of bounds'
	!print *, 'If this is true, then just bail with zeros!'

	Lo = 0.0e0
	Llte = 0.0e0
	nhalf = 0.0e0
	alpha = 0.0e0
	bail = 1
	
else if( ctemp > h2o_temp(iTemp) .or. ctemp < h2o_temp(1) ) then  !!Ok, out of range in temperature
!!First get position in NH
!!	!print *, ' Out of Range in Temperature'
	do i=2,iNH
		if(nm_h2o < logN_h2o(i) .and. nm_h2o > logN_h2o(i-1) ) then
			inhigh = i
			inlow = i-1
			exit
		endif	
	enddo
	if(ctemp > h2o_temp(iTemp) ) then	
		!print *, 'H2O_Rot: Too Hot!'
		Lo = 10**(-h2o_rL0(iTemp))  !!Just take max value
		
		c1 = 10**(-h2o_rLlte(iTemp,inlow)) 
		c2 = 10**(-h2o_rLlte(iTemp,inhigh))
		c3 = (10**(nm_h2o) - 10**(logN_h2o(inlow)) )
		c4 = (10**(logN_h2o(inhigh)) - 10**(logN_h2o(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
		
		c1 = 10**(h2o_rn12(iTemp,inlow)) 
		c2 = 10**(h2o_rn12(iTemp,inhigh))
		c3 = (10**(nm_h2o) - 10**(logN_h2o(inlow)) )
		c4 = (10**(logN_h2o(inhigh)) - 10**(logN_h2o(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		nhalf = c1 + (c3/c4)*(c2-c1)
		
		c1 = (h2o_ralp(iTemp,inlow)) 
		c2 = (h2o_ralp(iTemp,inhigh))
		c3 = (10**(nm_h2o) - 10**(logN_h2o(inlow)) )
		c4 = (10**(logN_h2o(inhigh)) - 10**(logN_h2o(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		alpha = c1 + (c3/c4)*(c2-c1)
	else if (ctemp < h2o_temp(1) ) then
		!print *, 'H2O_Rot: Too Cold!'
		Lo = 10**(-h2o_rL0(1))  !!Just take min value
		!print *, ' Lo: ', Lo
		!print *, ' inlow: ', inlow, ' inhigh: ', inhigh
		c1 = 10**(-h2o_rLlte(1,inlow)) 
		c2 = 10**(-h2o_rLlte(1,inhigh))
		c3 = (10**(nm_h2o) - 10**(logN_h2o(inlow)) )
		c4 = (10**(logN_h2o(inhigh)) - 10**(logN_h2o(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
		!print *, ' Llte: ', Llte
		
		c1 = 10**(h2o_rn12(1,inlow)) 
		c2 = 10**(h2o_rn12(1,inhigh))
		c3 = (10**(nm_h2o) - 10**(logN_h2o(inlow)) )
		c4 = (10**(logN_h2o(inhigh)) - 10**(logN_h2o(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		nhalf = c1 + (c3/c4)*(c2-c1)
		
		c1 = (h2o_ralp(1,inlow)) 
		c2 = (h2o_ralp(1,inhigh))
		c3 = (10**(nm_h2o) - 10**(logN_h2o(inlow)) )
		c4 = (10**(logN_h2o(inhigh)) - 10**(logN_h2o(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		alpha = c1 + (c3/c4)*(c2-c1)
		
		!write(*,'(4(A,1pe10.3))'), 'Lo: ', Lo, ' Llte: ', Llte, ' nhalf: ', nhalf, ' alpha: ', alpha
	else
		!print *, "ERROR IN COOL_ROVIB"
		!print *, "CANT FIND CASE FOR H2O Cooling, CHECK NUMBER DENSITIES AND TEMPS"
		call Driver_abortFlash("ERROR IN COOL_ROVIB: CAN'T FIND CASE FOR H2O COOLING")
	endif
else if( nm_h2o > logN_h2o(iNH) .or. nm_h2o < logN_h2o(1) ) then
	!!First get position in Temp
	!print *, ' Out of Range in Density '
	do i=2,iTemp
		if( ctemp < h2o_temp(i) .and. ctemp > h2o_temp(i-1) ) then
			ithigh = i
			itlow = i-1
			exit
		endif	
	enddo
	if( nm_h2o > logN_h2o(iNH) ) then	
		!print *, 'H2O_Rot Too Dense'
		!!Lo = -10**(h2o_rL0(iTemp))  !!Just take max value
		c1 = 10**(-h2o_rL0(itlow))
		c2 = 10**(-h2o_rL0(ithigh))
		c3 = (ctemp - h2o_temp(itlow))
		c4 = (h2o_temp(ithigh) - h2o_temp(itlow))
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Lo = c1 + (c3/c4)*(c2-c1)
			
		c1 = 10**(-h2o_rLlte(itlow,iNH)) 
		c2 = 10**(-h2o_rLlte(ithigh,iNH))
		c3 = (ctemp - h2o_temp(itlow) )
		c4 = (h2o_temp(ithigh) - h2o_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
		
		c1 = 10**(h2o_rn12(itlow,iNH)) 
		c2 = 10**(h2o_rn12(ithigh,iNH))
		c3 = (ctemp - h2o_temp(itlow) )
		c4 = (h2o_temp(ithigh) - h2o_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		nhalf = c1 + (c3/c4)*(c2-c1)
		
		c1 = (h2o_ralp(itlow,iNH)) 
		c2 = (h2o_ralp(ithigh,iNH))
		c3 = (ctemp - h2o_temp(itlow) )
		c4 = (h2o_temp(ithigh) - h2o_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		alpha = c1 + (c3/c4)*(c2-c1)
	else if (nm_h2o < logN_h2o(1) ) then
	
		!print *, 'H2O_Rot: Too Sparse'
		
		c1 = 10**(-h2o_rL0(itlow))
		c2 = 10**(-h2o_rL0(ithigh))
		c3 = (ctemp - h2o_temp(itlow))
		c4 = (h2o_temp(ithigh) - h2o_temp(itlow))
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		!write(*,'(2(A,I))'), 'itlow: ', itlow, ' ithigh', ithigh
		!write(*,'(4(A,1pe10.3))'), 'c1: ', c1, ' c2: ', c2, ' c3:', c3, ' c4: ', c4
	
		Lo = c1 + (c3/c4)*(c2-c1)
		!print *, 'Lo: ', Lo
		
		c1 = 10**(-h2o_rLlte(itlow,1)) 
		c2 = 10**(-h2o_rLlte(ithigh,1))
		c3 = (ctemp - h2o_temp(itlow) )
		c4 = (h2o_temp(ithigh) - h2o_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
		!print *, 'Llte: ', Llte
		
		c1 = 10**(h2o_rn12(itlow,1)) 
		c2 = 10**(h2o_rn12(ithigh,1))
		c3 = (ctemp - h2o_temp(itlow) )
		c4 = (h2o_temp(ithigh) - h2o_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		nhalf = c1 + (c3/c4)*(c2-c1)
		!print *, 'nhalf: ', nhalf
		
		c1 = (h2o_ralp(itlow,1)) 
		c2 = (h2o_ralp(ithigh,1))
		c3 = (ctemp - h2o_temp(itlow) )
		c4 = (h2o_temp(ithigh) - h2o_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		alpha = c1 + (c3/c4)*(c2-c1)
		!print *, 'alpha: ', alpha
	else
		!print *, "ERROR IN COOL_ROVIB"
		!print *, "CANT FIND CASE FOR H2O Cooling, CHECK NUMBER DENSITIES AND TEMPS"
		call Driver_abortFlash("ERROR IN COOL_ROVIB: CANT FIND CASE FOR H2O COOLING 2")
	endif
else
	!print *, "WTF1"
	call Driver_abortFlash("COOL_ROVIB: WTF1")
endif
!!	!print *, 'HERE!!!'

	Lall = 0.0e0
	if( bail .eq. 0) then
		Lall = (1.0/Lo) + nin(iH2)/Llte + (1.0/Lo)*(nin(iH2)/nhalf)**(alpha)*(1.0-(nhalf*Lo)/Llte)
		Lall = (1.0/Lall)
!		write(*,'(5(A,1pe10.3))'), ' Lall: ', Lall, ' Lo: ', Lo, ' Llte: ', Llte, ' nhalf: ', nhalf, ' alpha: ', alpha
	else
		Lall = 0.0e0
	endif
	!print *, 'cool_rout0: ', cool_rout
	cool_rout = cool_rout + Lall*nin(iH2O)*ne_h2
	term1 = Lall
	!print *, 'cool_rout1: ', cool_rout

!
!	
!!!Ok, now for rot CO
!
!

bail = 0

!write(*,'(3(A,1pe10.3))'), ' ctemp: ', ctemp, ' ctemp_high: ', co_temp(iTemp), ' ctemp_low: ', co_temp(1)
!write(*,'(3(A,1pe10.3))'), ' nm_co: ', nm_co, ' nco_high: ', LogN_co(iNH), ' nco_low: ', LogN_co(1)

if( ctemp < co_temp(iTemp) .and. ctemp > co_temp(1) .and. nm_co <	     logN_co(iNH) .and. nm_co > logN_co(1) ) then
	do i=2,iTemp
		if(ctemp < co_temp(i) .and. ctemp > co_temp(i-1) ) then
			ithigh = i
			itlow = i-1
			exit
		endif
	enddo
	do i=2,iNH
		if(nm_co < logN_co(i) .and. nm_co > logN_co(i-1) ) then
			inhigh = i
			inlow = i-1
			exit
		endif	
	enddo
	!print *, 'Perfectely Bracketed: CO Rot'
	c1 = 10**(-co_rL0(itlow))
	c2 = 10**(-co_rL0(ithigh))
	c3 = (ctemp - co_temp(itlow))
	c4 = (co_temp(ithigh) - co_temp(itlow))
	if(c4 .eq. 0.0e0) c4 = 1.0
	
	Lo = c1 + (c3/c4)*(c2-c1)
	
	k1 = (co_temp(ithigh)-ctemp)/(co_temp(ithigh)-co_temp(itlow))
	k2 = (ctemp-co_temp(itlow))/(co_temp(ithigh)-co_temp(itlow))
	k3 = (10**(logN_co(inhigh))-10**(nm_co))/(10**(logN_co(inhigh))-10**(logN_co(inlow)))
	k4 = (10**(nm_co)-10**(logN_co(inlow)))/(10**(logN_co(inhigh))-10**(logN_co(inlow)))
	
	Llte =  k3*(k1*(10**(-co_rLlte(itlow,inlow)))+k2*(10**(-co_rLlte(ithigh,inlow)))) + &
	      & k4*(k1*(10**(-co_rLlte(itlow,inhigh)))+k2*(10**(-co_rLlte(ithigh,inhigh))))

	nhalf =  k3*(k1*(10**(co_rn12(itlow,inlow)))+k2*(10**(co_rn12(ithigh,inlow)))) + &
	      &  k4*(k1*(10**(co_rn12(itlow,inhigh)))+k2*(10**(co_rn12(ithigh,inhigh))))
	      
	alpha =  k3*(k1*co_ralp(itlow,inlow)+k2*co_ralp(ithigh,inlow)) + &
	       & k4*(k1*co_ralp(itlow,inhigh)+k2*co_ralp(ithigh,inhigh))
else if( ctemp > co_temp(iTemp) .or. ctemp < co_temp(1) .and.  nm_co > logN_co(iNH) .or. nm_co < logN_h2o(1) ) then
	!print *, 'Way out of bounds'
	!print *, 'If this is true, then just bail with zeros!'

	Lo = 0.0e0
	Llte = 0.0e0
	nhalf = 0.0e0
	alpha = 0.0e0
	bail = 1
else if( ctemp > co_temp(iTemp) .or. ctemp < co_temp(1) ) then  !!Ok, out of range in temperature
!!First get position in NH
	do i=2,iNH
		if(nm_co < logN_co(i) .and. nm_co > logN_co(i-1) ) then
			inhigh = i
			inlow = i-1
			exit
		endif	
	enddo
	if(ctemp > co_temp(iTemp) ) then	
		!print *,' CO_ROT: Too Hot'
		Lo = 10**(-co_rL0(iTemp))  !!Just take max value
		!print *,' Lo: ', Lo, ' iTemp: ', iTemp
		!print *,' inlow: ', inlow, ' inhigh: ', inhigh, ' nm_co:', nm_co
		!print *,' ncohigh: ', 10**(logN_co(inhigh)), ' ncolow: ', 10**(logN_co(inlow))
		c1 = 10**(-co_rLlte(iTemp,inlow)) 
		c2 = 10**(-co_rLlte(iTemp,inhigh))
		c3 = (10**(nm_co) - 10**(logN_co(inlow)) )
		c4 = (10**(logN_co(inhigh)) - 10**(logN_co(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		print *,' HERE1'
		Llte = c1 + (c3/c4)*(c2-c1)
		
		c1 = 10**(co_rn12(iTemp,inlow)) 
		c2 = 10**(co_rn12(iTemp,inhigh))
		c3 = (10**(nm_co) - 10**(logN_co(inlow)) )
		c4 = (10**(logN_co(inhigh)) - 10**(logN_co(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		print *,' HERE2'
		nhalf = c1 + (c3/c4)*(c2-c1)
		
		c1 = (co_ralp(iTemp,inlow)) 
		c2 = (co_ralp(iTemp,inhigh))
		c3 = (10**(nm_co) - 10**(logN_co(inlow)) )
		c4 = (10**(logN_co(inhigh)) - 10**(logN_co(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		print *,' HERE3'
		alpha = c1 + (c3/c4)*(c2-c1)
	else if (ctemp < co_temp(1) ) then
		!print *,' CO_ROT: Too Cold'
		Lo = 10**(-co_rL0(1))  !!Just take min value
		
		c1 = 10**(-co_rLlte(1,inlow)) 
		c2 = 10**(-co_rLlte(1,inhigh))
		c3 = (10**(nm_co) - 10**(logN_co(inlow)) )
		c4 = (10**(logN_co(inhigh)) - 10**(logN_co(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
		
		c1 = 10**(co_rn12(1,inlow)) 
		c2 = 10**(co_rn12(1,inhigh))
		c3 = (10**(nm_co) - 10**(logN_co(inlow)) )
		c4 = (10**(logN_co(inhigh)) - 10**(logN_co(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		nhalf = c1 + (c3/c4)*(c2-c1)
		
		c1 = (co_ralp(1,inlow)) 
		c2 = (co_ralp(1,inhigh))
		c3 = (10**(nm_co) - 10**(logN_co(inlow)) )
		c4 = (10**(logN_co(inhigh)) - 10**(logN_co(inlow)) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		alpha = c1 + (c3/c4)*(c2-c1)
	else
		!print *, "ERROR IN COOL_ROVIB"
		!print *, "CANT FIND CASE FOR H2O Cooling, CHECK NUMBER DENSITIES AND TEMPS"
		call abort()
	endif
else if( nm_co > logN_co(iNH) .or. nm_co < logN_co(1) ) then
	!!First get position in Temp
	do i=2,iTemp
		if( ctemp < co_temp(i) .and. ctemp > co_temp(i-1) ) then
			ithigh = i
			itlow = i-1
			exit
		endif	
	enddo
	if( nm_co > logN_co(iNH) ) then	
		!print *,' CO_ROT: Too Dense'
		!!Lo = -10**(co_rL0(iTemp))  !!Just take max value
		c1 = 10**(-co_rL0(itlow))
		c2 = 10**(-co_rL0(ithigh))
		c3 = (ctemp - co_temp(itlow))
		c4 = (co_temp(ithigh) - co_temp(itlow))
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Lo = c1 + (c3/c4)*(c2-c1)
			
		c1 = 10**(-co_rLlte(itlow,iNH)) 
		c2 = 10**(-co_rLlte(ithigh,iNH))
		c3 = (ctemp - co_temp(itlow) )
		c4 = (co_temp(ithigh) - co_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
		
		c1 = 10**(co_rn12(itlow,iNH)) 
		c2 = 10**(co_rn12(ithigh,iNH))
		c3 = (ctemp - co_temp(itlow) )
		c4 = (co_temp(ithigh) - co_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		nhalf = c1 + (c3/c4)*(c2-c1)
		
		c1 = (co_ralp(itlow,iNH)) 
		c2 = (co_ralp(ithigh,iNH))
		c3 = (ctemp - co_temp(itlow) )
		c4 = (co_temp(ithigh) - co_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		alpha = c1 + (c3/c4)*(c2-c1)
	else if (nm_co < logN_co(1) ) then
		!print *,' CO_ROT: Too Sparse'	
		c1 = 10**(-co_rL0(itlow))
		c2 = 10**(-co_rL0(ithigh))
		c3 = (ctemp - co_temp(itlow))
		c4 = (co_temp(ithigh) - co_temp(itlow))
		if(c4 .eq. 0.0e0) c4 = 1.0
	
		Lo = c1 + (c3/c4)*(c2-c1)
		
		c1 = 10**(-co_rLlte(itlow,1)) 
		c2 = 10**(-co_rLlte(ithigh,1))
		c3 = (ctemp - co_temp(itlow) )
		c4 = (co_temp(ithigh) - co_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
		
		c1 = 10**(co_rn12(itlow,1)) 
		c2 = 10**(co_rn12(ithigh,1))
		c3 = (ctemp - co_temp(itlow) )
		c4 = (co_temp(ithigh) - co_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		nhalf = c1 + (c3/c4)*(c2-c1)
		
		c1 = (co_ralp(itlow,1)) 
		c2 = (co_ralp(ithigh,1))
		c3 = (ctemp - co_temp(itlow) )
		c4 = (co_temp(ithigh) - co_temp(itlow) )
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		alpha = c1 + (c3/c4)*(c2-c1)
	else
		!print *, "ERROR IN COOL_ROVIB"
		!print *, "CANT FIND CASE FOR H2O Cooling, CHECK NUMBER DENSITIES AND TEMPS"
		call abort()
	endif
else
	!print *, "WTF2"
	call abort()
endif

	Lall = 0.0e0
	if(bail .eq. 0) then
		Lall = (1.0/Lo) + nin(iH2)/Llte + (1.0/Lo)*(nin(iH2)/nhalf)**(alpha)*(1.0-(nhalf*Lo)/Llte)
		Lall = 1.0/Lall
	else
		Lall = 0.0e0
	endif
	!print *, 'cool_rout2: ', cool_rout
	cool_rout = cool_rout + Lall*nin(iCO)*ne_h2
	term2 = Lall
	lco = lco + Lall*nin(iCO)*ne_h2
	!print *, 'cool_rout3: ', cool_rout
	

!!Now for Vibrational H2O
!!Update array sizes
iTemp = 6
iNH = 8

!!Update ne_co, ne_h2o
lcoe = 1.03e-10*(ctemp/300.0)**(0.938) !!*exp(-3080.0/ctemp)
lcoo = 1.14e-14*exp(-68.0*ctemp**(-0.333)) !!*exp(-3080.0/ctemp)
lh2oe = 2.6e-6*ctemp**(-0.5) !!*exp(-2325.0/ctemp)
lh2oo = 0.64e-14*exp(-47.5*ctemp**(-0.3333)) !!*exp(-2325.0/ctemp)

ne_co  = nin(iH2) + 50.0*nin(iH) + (lcoe/lcoo)*nin(iELEC)
ne_h2  = nin(iH2) + 10.0*nin(iH) + (lh2oe/lh2oo)*nin(iELEC)

!write(*,'(3(A,1pe10.3))'), ' lcoe: ', lcoe, ' lcoo: ', lcoo, ' lh2oe: ', lh2oe
!write(*,'(3(A,1pe10.3))'), ' lh2oo: ', lh2oo, ' ne_co: ', ne_co, ' ne_h2: ', ne_h2

Lo = 1.03e-26*ctemp*exp(-47.5*ctemp**(-0.333))*exp(-2325.0/ctemp) !!!Yay Analytic
!print *, 'Lo: ', Lo
bail = 0
if(ctemp > vib_temp(1) .and. ctemp < vib_temp(iTemp) .and. nm_h2o > vib_logN(1) .and. nm_h2o < vib_logN(iNH) ) then
!!Then we are bracketted 
!!Find the positions in NH and Temp
	do i=2,iTemp
		if(ctemp < vib_temp(i) .and. ctemp > vib_temp(i-1) ) then
			ithigh = i
			itlow = i-1
			exit
		endif
	enddo
	do i=2,iNH
		if(nm_h2o < vib_logN(i) .and. nm_h2o > vib_logN(i-1) ) then
			inhigh = i
			inlow = i-1
			exit
		endif	
	enddo
	!print *,' VIB_H2O: Perfectely Brackted'
	k1 = (vib_temp(ithigh)-ctemp)/(vib_temp(ithigh)-vib_temp(itlow))
	k2 = (ctemp-vib_temp(itlow)) /(vib_temp(ithigh)-vib_temp(itlow))
	k3 = (10**(vib_logN(inhigh)) - 10**(nm_h2o) ) / (10**(vib_logN(inhigh)) - 10**(vib_logN(inlow)))
	k4 = (10**(nm_h2o) - 10**(vib_logN(inlow) ) ) / (10**(vib_logN(inhigh)) - 10**(vib_logN(inlow)))
	
	Llte =  k3*(k1*(10**(-h2o_vLlte(itlow,inlow)))+k2*(10**(-h2o_vLlte(ithigh,inlow)))) + &
	      & k4*(k1*(10**(-h2o_vLlte(itlow,inhigh)))+k2*(10**(-h2o_vLlte(ithigh,inhigh))))
	      
	Llte = Llte / exp(2325.0*ctemp**(-1.0))

else if(ctemp < vib_temp(1) .or. ctemp > vib_temp(iTemp) .and.  nm_h2o > vib_logN(iNH) .or. nm_h2o < vib_logN(1) ) then
	!print *,' BAIL OUT'
	bail = 1
else if ( ctemp < vib_temp(1) .or. ctemp > vib_temp(iTemp) ) then
	!print *,' VIB_H2O: O.R.T'
	do i=2,iNH
		if(nm_h2o < vib_logN(i) .and. nm_h2o > vib_logN(i-1) ) then
			inhigh = i
			inlow = i-1
			exit
		endif	
	enddo
	!write(*,'(2(A,I))'), 'inhigh: ', inhigh, ' inlow:', inlow
	if( ctemp > vib_temp(iTemp) ) then  !!Too hot
		
		c1 = 10**(-h2o_vLlte(iTemp,inlow)) *(1.0/exp(-2325.0*ctemp**(-1.0)))
		c2 = 10**(-h2o_vLlte(iTemp,inhigh))*(1.0/exp(-2325.0*ctemp**(-1.0)))
		c3 = (10**(nm_h2o) - 10**(vib_logN(inlow)) )
		c4 = (10**(vib_logN(inhigh)) - 10**(vib_logN(inlow)))
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
	else if (ctemp < vib_temp(1) ) then !!Too Cold
		c1 = 10**(-h2o_vLlte(1,inlow)) *(1.0/exp(-2325.0*ctemp**(-1.0)))
		c2 = 10**(-h2o_vLlte(1,inhigh))*(1.0/exp(-2325.0*ctemp**(-1.0)))
		c3 = (10**(nm_h2o) - 10**(vib_logN(inlow)) )
		c4 = (10**(vib_logN(inhigh)) - 10**(vib_logN(inlow)))
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
	else
		!print *, 'Error in Cool_rovib: H2O Vibration'
		!print *, 'Check temperature or num dens'
		call abort()
	endif
	
else if( nm_h2o > vib_logN(iNH) .or. nm_h2o < vib_logN(1) ) then  !!Density
	!print *,' VIB_H2O: Out of bounds, density'
	do i=2,iTemp
		if(ctemp < vib_temp(i) .and. ctemp > vib_temp(i-1) ) then
			ithigh = i
			itlow = i-1
			exit
		endif
	enddo
	
	if(nm_h2o > vib_logN(iNH) ) then
		!print *,' VIB_H2O: O.R.D. Too Dense'
		c1 = 10**(-h2o_vLlte(itlow,iNH) *(1.0/exp(2325.0*ctemp**(-1.0))))
		c2 = 10**(-h2o_vLlte(ithigh,iNH) *(1.0/exp(2325.0*ctemp**(-1.0))))
		c3 = (ctemp - vib_temp(itlow))
		c4 = (vib_temp(ithigh) - ctemp)
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
	
	else if (nm_h2o < vib_logN(1) ) then !!Too sparse
		!print *,' VIB_H2O: O.R.D. Too Sparse'		
		c1 = 10**(-h2o_vLlte(itlow,1)) !! *(1.0/exp(2325.0*ctemp**(-1.0))))
		c2 = 10**(-h2o_vLlte(ithigh,1)) !! *(1.0/exp(2325.0*ctemp**(-1.0))))
		c3 = (ctemp - vib_temp(itlow))
		c4 = (vib_temp(ithigh) - ctemp)
		!write(*,'(2(A,I))'), 'itlow: ', itlow, ' ithigh', ithigh
		!write(*,'(4(A,1pe10.3))'), 'c1: ', c1, ' c2: ', c2, ' c3:', c3, ' c4: ', c4
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
	
	else
		!print *, 'Error in Cool_rovib: H2O Vibration'
		!print *, 'Check temperature or num dens'
		call abort()
	endif

else
	!print *, 'WTF3'
	call abort()
endif

Lall = 0.0e0
if(bail .eq. 0) then
	Lall = (1.0/Lo) + (nin(iH2))/(Llte)
	Lall = (1.0/Lall)
endif
!print *, 'cool_rout4: ', cool_rout
cool_rout = cool_rout + Lall * nin(iH2O) * ne_h2
term3 = Lall 
!print *, 'cool_rout5: ', cool_rout

!!Finally, time for CO vibrational
bail = 0
Lo = 1.83e-26*ctemp*exp(-68.0*ctemp**(-0.333))*exp(-3080.0*ctemp**(-1.0))  !!!Yay Analytic
if(ctemp > vib_temp(1) .and. ctemp < vib_temp(iTemp) .and. nm_co > vib_logN(1) .and. nm_co < vib_logN(iNH) ) then
!!Then we are bracketted 
!!Find the positions in NH and Temp
	do i=2,iTemp
		if(ctemp < vib_temp(i) .and. ctemp > vib_temp(i-1) ) then
			ithigh = i
			itlow = i-1
			exit
		endif
	enddo
	do i=2,iNH
		if(nm_co < vib_logN(i) .and. nm_co > vib_logN(i-1) ) then
			inhigh = i
			inlow = i-1
			exit
		endif	
	enddo
	!print *,' CO_VIB: Perfectely Bracketed'
	k1 = (vib_temp(ithigh)-ctemp)/(vib_temp(ithigh)-vib_temp(itlow))
	k2 = (ctemp-vib_temp(itlow)) /(vib_temp(ithigh)-vib_temp(itlow))
	k3 = (10**(vib_logN(inhigh)) - 10**(nm_co) ) / (10**(vib_logN(inhigh)) - 10**(vib_logN(inlow)))
	k4 = (10**(nm_co) - 10**(vib_logN(inlow) ) ) / (10**(vib_logN(inhigh)) - 10**(vib_logN(inlow)))
	
	Llte =  k3*(k1*(10**(-co_vLlte(itlow,inlow)))+k2*(10**(-co_vLlte(ithigh,inlow)))) + &
	      & k4*(k1*(10**(-co_vLlte(itlow,inhigh)))+k2*(10**(-co_vLlte(ithigh,inhigh))))
	      
	Llte = Llte / (exp(3080.0*ctemp**(-1.0)))

else if ( ctemp < vib_temp(1) .or. ctemp > vib_temp(iTemp) .and. nm_co > vib_logN(iNH) .or. nm_co < vib_logN(1) ) then
	!print *,' BAIL OUT 2'
	bail = 1
else if ( ctemp < vib_temp(1) .or. ctemp > vib_temp(iTemp) ) then
	do i=2,iNH
		if(nm_co < vib_logN(i) .and. nm_co > vib_logN(i-1) ) then
			inhigh = i
			inlow = i-1
			exit
		endif	
	enddo
	
	if( ctemp > vib_temp(iTemp) ) then  !!Too hot
		!print *,' CO_VIB: Too Hot'
		c1 = 10**(-co_vLlte(iTemp,inlow)) *(1.0/exp(-2325.0*ctemp**(-1.0)))
		c2 = 10**(-co_vLlte(iTemp,inhigh))*(1.0/exp(-2325.0*ctemp**(-1.0)))
		c3 = (10**(nm_co) - 10**(vib_logN(inlow)) )
		c4 = (10**(vib_logN(inhigh)) - 10**(vib_logN(inlow)))
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
	else if (ctemp < vib_temp(1) ) then !!Too Cold
		!print *,' CO_VIB: Too Cold'
		c1 = 10**(-co_vLlte(1,inlow)) *(1.0/exp(-2325.0*ctemp**(-1.0)))
		c2 = 10**(-co_vLlte(1,inhigh))*(1.0/exp(-2325.0*ctemp**(-1.0)))
		c3 = (10**(nm_co) - 10**(vib_logN(inlow)) )
		c4 = (10**(vib_logN(inhigh)) - 10**(vib_logN(inlow)))
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
	else
		!print *, 'Error in Cool_rovib: H2O Vibration'
		!print *, 'Check temperature or num dens'
		call Driver_abortFlash("ERROR IN COOL_ROVIB: CO ERROR1")
	endif
	
else if( nm_co > vib_logN(iNH) .or. nm_co < vib_logN(1) ) then  !!Density
	do i=2,iTemp
		if(ctemp < vib_temp(i) .and. ctemp > vib_temp(i-1) ) then
			ithigh = i
			itlow = i-1
			exit
		endif
	enddo
	
	if(nm_co > vib_logN(iNH) ) then
		!print *,'CO_VIB: Too Dense'
		c1 = 10**(-co_vLlte(itlow,iNH)) *(1.0/exp(-2325.0*ctemp**(-1.0)))
		c2 = 10**(-co_vLlte(ithigh,iNH))*(1.0/exp(-2325.0*ctemp**(-1.0)))
		c3 = (ctemp - vib_temp(itlow))
		c4 = (vib_temp(ithigh) - ctemp)
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
	
	else if (nm_co < vib_logN(1) ) then !!Too Cold
		!print *,'CO_VIB: Too Sparse'
		c1 = 10**(-co_vLlte(itlow,1)) *(1.0/exp(-2325.0*ctemp**(-1.0)))
		c2 = 10**(-co_vLlte(ithigh,1))*(1.0/exp(-2325.0*ctemp**(-1.0)))
		c3 = (ctemp - vib_temp(itlow))
		c4 = (vib_temp(ithigh) - ctemp)
		if(c4 .eq. 0.0e0) c4 = 1.0
		
		Llte = c1 + (c3/c4)*(c2-c1)
	
	else
		!print *, 'Error in Cool_rovib: H2O Vibration'
		!print *, 'Check temperature or num dens'
		call Driver_abortFlash("ERROR IN COOL_ROVIB CO VIB")
	endif

else
	!print *, 'WTF4'
	call Driver_abortFLash("COOL_ROVIB: WTF4")
endif

Lall = 0.0e0
if(bail .eq. 0) then
	Lall = (1.0/Lo) + (nin(iH2))/(Llte)
	Lall = 1.0/Lall
endif
!print *, 'cool_rout6: ', cool_rout
cool_rout = cool_rout + Lall * nin(iCO) * ne_co
lco = lco + Lall * nin(iCO) * ne_co
term4 = Lall
!print *, 'cool_rout7: ', cool_rout

!print *, 'END OF COOL_ROVIB: '
!print *, 'cool_rout: ', cool_rout

!print *, 'CALLING ABORT '
!call abort()

write(*,'(10(1pe12.4))') rho_in,temp_in,div_v,nin(iH2),nin(iH2O),nin(iCO),term1,term2,term3,term4


return

end subroutine Cool_rovib
