!!****Cool_magnesium
!!
!!
!!
!! NAME
!!  Cool_magnesium
!!
!! SYNOPSIS
!!	Calculates the cooling due to magnesium
!!  	Total cooling rate is returned as cout_mg
!!
!! ARGUMENTS
!!
!!*****

subroutine Cool_magnesium(temp,rho,tindex,nin,cout_mg)

use Chemistry_data
use Cool_data

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp, rho, cout_mg
integer :: tindex
real, dimension(NSPECIES) :: nin

real :: mg0term, mg1term, mghiterm

!! This will calculate the cooing from magnesium
!! We are linearaly interpolating along the cooling functions

cout_mg = 0.0e0
mg0term = 0.0e0
mg1term = 0.0e0
mghiterm = 0.0e0

!!Mg0 first. All of these are n_ion * n_elec * CF / rho

if(tindex == clength) then
   mg0term = coolmg0(clength)
   mg1term = coolmg1(clength)
   mghiterm = coolmghi(clength)
else if (tindex == 1) then
   mg0term = coolmg0(1)
   mg1term = coolmg1(1)
   mghiterm = coolmghi(1)
else
   mg0term = coolmg0(tindex) + (coolmg0(tindex+1)-coolmg0(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   mg1term = coolmg1(tindex) + (coolmg1(tindex+1)-coolmg1(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
   mghiterm = coolmghi(tindex) + (coolmghi(tindex+1)-coolmghi(tindex) ) *( (temp - cooltemp(tindex)) / (cooltemp(tindex+1) - cooltemp(tindex)))
endif

mg0term  = mg0term * nin(iMG)   * nin(iELEC) 
mg1term  = mg1term * nin(iMGP)  * nin(iELEC) 
mghiterm = mghiterm * nin(iMG2P)* nin(iELEC) 

cout_mg = (mg0term+mg1term+mghiterm)/rho

!!print *, 'ninH: ', nin(iH), ' ninHP: ', nin(iHP), 'ninE: ', nin(iELEC)
!!print *, 'h0: ', h0term, ' h1: ', h1term
!!print *, 'cout_h: ', cout_h, ' rho: ', rho

return 
end subroutine Cool_magnesium
