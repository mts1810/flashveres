!!****Chemistry_coolMolecule
!!
!!
!!
!! NAME
!!  Chemistry_coolMolecule
!!
!! SYNOPSIS
!!
!! This routine does the H2 molecular cooling
!!
!! ARGUMENTS
!!
!!*****
subroutine Chemistry_coolMolecule(temp_in,rho_in,nin,cool_out,lum)

use Chemistry_data
use Chemistry_coolData

implicit none

#include "constants.h"
#include "Flash.h"

real :: temp_in, rho_in, cool_out   !! current temperature
real, dimension(7) 	  :: lum
real, dimension(NSPECIES) :: nin  !!number densities of my species

real :: cool_sum, T3, tmp, term, rho
real :: lt1, lt2, lt3, lt4, lt5
real :: c1,c2,c3

tmp = temp_in
rho = rho_in
T3 = temp_in/1000.0
lt1 = log10(T3)
lt2 = (log10(T3))**(2)
lt3 = (log10(T3))**(3)
lt4 = (log10(T3))**(4)
lt5 = (log10(T3))**(5)

cool_sum = 0.0e0
c1 = 0.0e0
c2 = 0.0e0
c3 = 0.0e0
lum(:) = 0.0e0

!!Time to go over all cooling rates and such

!!First is H - H2
term = 0.0e0
if(tmp .lt. 10.0) then
   term = 0.0e0
else if(tmp .gt. 10.0 .and. tmp .lt. 100.0) then
   term = -16.818342 + 37.383713*lt1 + 58.145166*lt2 + 48.656103*lt3 + 20.159831*lt4 + 3.8479610*lt5
   term = 10**(term)
else if(tmp .gt. 100.0 .and. tmp .lt. 1000.0) then
   term = -24.311209 + 3.5692468*lt1 - 11.332860*lt2 - 27.850082*lt3 - 21.328264*lt4 - 4.2519023*lt5
   term = 10**(term)
else if(tmp .gt. 1000.0 .and. tmp .lt. 6000.0) then
   term = -24.311209 + 4.6450521*lt1 - 3.7209846*lt2 + 5.9369081*lt3 - 5.5108047*lt4 + 1.5538288*lt5
   term = 10**(term)
else
   term = 0.0e0
endif
	cool_sum = cool_sum + term*(nin(iH)*nin(iH2)/rho)
   
!!   write(*,*) 'Cool_sum1: ', cool_sum

!! H2 & H2
term = 0.0e0
if(tmp .gt. 100.0 .and. tmp .lt. 6000.0) then
	term = -23.962112 + 2.09433740*lt1 - 0.77151436*lt2 + 0.43693353*lt3 - 0.14813216*lt4 - 0.033638326*lt5
	term = 10**(term)
endif
	cool_sum = cool_sum + term*(nin(iH2)*nin(iH2)/rho)
	
!!	write(*,*) 'Cool_sum2: ', cool_sum
	
!! H2 & He
term = 0.0e0
if(tmp .gt. 100.0 .and. tmp .lt. 6000.0) then
	term = -23.689237 + 2.1892372*lt1 - 0.81520438*lt2 + 0.29036281*lt3  - 0.16596184*lt4 + 0.19191375*lt5
	term = 10**(term)
endif
	cool_sum = cool_sum + term*(nin(iH2)*nin(iHE)/rho)
	
!!	write(*,*) 'Cool_sum3: ', cool_sum
	
!! H2 & HP
term = 0.0e0
if(tmp .gt. 10.0 .and. tmp .lt. 10000.0) then
	term = -21.716699 + 1.3865783*lt1 - 0.37915285*lt2 + 0.11453688*lt3 - 0.23214154*lt4 + 0.058538864*lt5
	term = 10**(term)
endif
	cool_sum = cool_sum + term*(nin(iH2)*nin(iHP)/rho)
	
!!	write(*,*) 'Cool_sum4: ', cool_sum
	
!! H2 & e
term = 0.0e0
if(tmp .gt. 10.0 .and. tmp .lt. 200.0) then
	term = -34.286155 - 48.537163*lt1 - 77.121176*lt2 - 51.352459*lt3 - 15.169160*lt4 - 0.98120322*lt5
	term = 10**(term)
else if(tmp .gt. 200.0 .and. tmp .lt. 10000.0) then
	term = -22.190316 + 1.5728955*lt1 - 0.21335100*lt2 + 0.96149759*lt3 - 0.91023195*lt4 + 0.13749749*lt5
	term = 10**(term)
endif
	cool_sum = cool_sum + term*(nin(iH2)*nin(iELEC)/rho)
	
!!	write(*,*) 'Cool_sum5: ', cool_sum
		
!!H2+ + e
term = 0.0e0
if(tmp .gt. 1.0 .and. tmp .lt. 2000.0) then
   term = 1.1e-19*tmp**(-0.34)*exp(-3025.0/tmp)
else
   term = 3.35e-21*tmp**(0.12)*exp(-3025.0/tmp)
endif
   c1 = term*nin(iELEC)
   
!!H2+ + H
term = 0.0e0
if(tmp .gt. 1.0 .and. tmp .lt. 1000.0) then
	term = 1.36e-22*exp(-3152.0/tmp)
	term = term*nin(iH)
else
	term = -35.42 + 5.95*log10(tmp) - 0.526*log10(tmp)**2
	term = 10**(term)*nin(iH)
endif
	c1 = c1 + term

if(c1 .lt. 1.0e-50) c1 = 1.0e-50
!write(*,*) ' c1: ', c1

!! LTE H2
term = 0.0e0
term = 2.0e-19*tmp**(0.1)*exp(-3125.0/tmp)
c2 = term 

term = 0.0e0

term = c2/(1.0 + c2/c1)

cool_sum = cool_sum + term * nin(iH) * nin(iH2P)/rho 

!write(*,*) 'Cool_sum7: ', cool_sum


if(cool_sum .lt. 1.0e-50) cool_sum = 1.0e-50

lum(1) = lum(1) + cool_sum

cool_out = cool_sum

!write(*,*) 'Cool_out: ', cool_out, 'TEMP: ', tmp
!!call abort()

return

end subroutine Chemistry_coolMolecule
