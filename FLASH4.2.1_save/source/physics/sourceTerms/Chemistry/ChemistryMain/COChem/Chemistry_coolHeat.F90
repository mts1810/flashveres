!!****Chemistry_coolHeat
!!
!!
!!
!! NAME
!!  Chemistry_coolHeat
!!
!! SYNOPSIS
!!	Calculates the heating from Glover 2007
!!
!!
!!
!! ARGUMENTS
!!
!!*****

subroutine Chemistry_coolHeat(temp_in,rho_in,nin,mfrac,cool_hout,delta_h,lum)

use Chemistry_data
use Chemistry_coolData

implicit none
#include "constants.h"
#include "Flash.h"


real	:: temp_in, rho_in, cool_hout, delta_h
real, dimension(7)  	  :: lum
real, dimension(NSPECIES) :: nin
real	:: eps, Gconst, psi, ctemp, tfour, term, mfrac
real	:: n_h, Rdiss, n1, n2, ncr, tcmb, sumchi
real	:: Inuu

ctemp = temp_in
tfour = ctemp/(1.0e4)
term = 0.0e0
n_h = 0.0e0
cool_hout = 0.0e0
Rdiss = 0.0e0
Gconst = 0.0e0
sumchi = 0.0e0
Inuu = 0.0e0
tcmb = 2.726

Inuu = 0.437157*1.0/(exp(1.493e5/tcmb)-1)

Gconst = sigh

n_h = nin(iH) + nin(iHP) + 2.0*nin(iH2) !!Total hydrogen number density

!!First is photoelectic effect
eps = 0.0e0
psi = 0.0e0
psi = Gconst*sqrt(ctemp)/(0.5*nin(iELEC))

eps = 4.9e-2/(1.0+4.0e-3*psi**(0.73)) + 3.7e-2*tfour**(0.7)/(1.0 + 2.0e-4*psi)
term = 1.3e-24*eps*Gconst*mfrac*n_h

!print *,'0: cool_hout: ', cool_hout
cool_hout = cool_hout + term
!print *,'1: cool_hout: ', cool_hout

!! H2 photo--> Don't need as I track binding energies
!term = 6.4e-13*1.38e9*Inuu*nin(iH2)   !!Have to define Inu
!print *,'2: cool_hout: ', cool_hout
!cool_hout = cool_hout + term
!print *,'3: cool_hout: ', cool_hout

!! UV H2 Pumping
n1 = 10**(4.000-0.416*(ctemp/1.0e4)-0.327*(ctemp/1.0e4)**2)/10.0
n2 = 10**(4.845-1.300*(ctemp/1.0e4)+1.620*(ctemp/1.0e4)**2)
ncr = (1.0/2.0)*(1.0/n1 + 1.0/n2)
ncr = 1.0/ncr
term = 2.7e-11*1.38e9*Inuu*nin(iH2)*(n_H/(n_H + ncr))

!print *,'4: cool_hout: ', cool_hout
cool_hout = cool_hout + term
!print *,'5: cool_hout: ', cool_hout

!!Cosmic Rays
sumchi = 1.0 + (1.1*nin(iHE)+0.037*nin(iH2)+0.22*nin(iH2)+6.5e-4*nin(iH2)+2.0*nin(iH2))/n_h
term = 3.2e-11*sumchi*n_h*sigh

!term = 6.4e-20*nin(iH2)

!print *,'6: cool_hout: ', cool_hout
cool_hout = cool_hout + term
!print *,'7: cool_hout: ', cool_hout

!!That should be it

cool_hout = cool_hout/rho_in
!print *,' cool_hout: ', cool_hout
!!call abort()

!Heating due to photoionization of H
term = 0.0e0
term = ratraw(iR199)*nin(iH)
term = term / (term + ratraw(iR004)*nin(iH)*nin(iH2P) + ratraw(iR011)*nin(iH)*nin(iELEC) + ratraw(iR018)*nin(iHEP)*nin(iH) + ratraw(iR028)*nin(iCP)*nin(iH) + ratraw(iR089)*nin(iH2)*nin(iHEP) + ratraw(iR097)*nin(iH2O)*nin(iHEP) + ratraw(iR106)*nin(iCOP)*nin(iH))
if(term .lt. 0.0e0) then
   term = 0.0e0
endif
if(term .gt. 1.0e0) then
   term = 1.0e0
endif
term = delta_h*term
!!print *,'TERM: ', term

cool_hout = cool_hout + term
cool_hout = cool_hout/rho_in
lum(7) = lum(7) + cool_hout

return

end subroutine Chemistry_coolHeat
