!!****Chemisty_init
!!
!!
!!
!! NAME
!!  Chemistry_init
!!
!! SYNOPSIS
!!	Initialize the variables from Chemistry_data
!!
!! ARGUMENTS
!!
!!*****

subroutine Chemistry_init()

use Chemistry_data
use Driver_interface, ONLY : Driver_abortFlash
use RuntimeParameters_interface, ONLY : RuntimeParameters_get

implicit none
#include "constants.h"
#include "Flash.h"

!!Eventuall will need parameter file read here for 
!! j21, fssh2, fsshd, chem_doCool, chem_mCool, chem_cc_case 



!! Define integers for the species

iH    = 1
iHP   = 2
iHM   = 3
iH2   = 4
iH2P  = 5
iH3P  = 6
iHE   = 7
iHEP  = 8 
iC    = 9
iCP   = 10
iCM   = 11
iO    = 12
iOP   = 13
iOM   = 14
iC2   = 15
iO2   = 16
iO2P  = 17
iOH   = 18
iOHP  = 19
iCO   = 20
iCOP  = 21
iCH   = 22
iCHP  = 23
iCH2  = 24
iCH2P = 25
iHCOP = 26
iHOCP = 27
iH2O  = 28
iH2OP = 29
iH3OP = 30
iCH3P = 31
iELEC = 32

!! Define integers for reactions
iR001 = 1
iR002 = 2
iR003 = 3
iR004 = 4
iR005 = 5
iR006 = 6
iR007 = 7
iR008 = 8
iR009 = 9
iR010 = 10
iR011 = 11
iR012 = 12
iR013 = 13
iR014 = 14
iR015 = 15
iR016 = 16
iR017 = 17
iR018 = 18
iR019 = 19
iR020 = 20
iR021 = 21
iR022 = 22
iR023 = 23
iR024 = 24
iR025 = 25
iR026 = 26
iR027 = 27
iR028 = 28
iR029 = 29
iR030 = 30
iR031 = 31
iR032 = 32
iR033 = 33
iR034 = 34
iR035 = 35
iR036 = 36
iR037 = 37
iR038 = 38
iR039 = 39
iR040 = 40
iR041 = 41
iR042 = 42
iR043 = 43
iR044 = 44
iR045 = 45
iR046 = 46
iR047 = 47
iR048 = 48
iR049 = 49
iR050 = 50
iR051 = 51
iR052 = 52
iR053 = 53
iR054 = 54
iR055 = 55
iR056 = 56
iR057 = 57
iR058 = 58
iR059 = 59
iR060 = 60
iR061 = 61
iR062 = 62
iR063 = 63
iR064 = 64
iR065 = 65
iR066 = 66
iR067 = 67
iR068 = 68
iR069 = 69
iR070 = 70
iR071 = 71
iR072 = 72
iR073 = 73
iR074 = 74
iR075 = 75
iR076 = 76
iR077 = 77
iR078 = 78
iR079 = 79
iR080 = 80
iR081 = 81
iR082 = 82
iR083 = 83
iR084 = 84
iR085 = 85
iR086 = 86
iR087 = 87
iR088 = 88
iR089 = 89
iR090 = 90
iR091 = 91
iR092 = 92
iR093 = 93
iR094 = 94
iR095 = 95
iR096 = 96
iR097 = 97
iR098 = 98
iR099 = 99
iR100 = 100
iR101 = 101
iR102 = 102
iR103 = 103
iR104 = 104
iR105 = 105
iR106 = 106
iR107 = 107
iR108 = 108
iR109 = 109
iR110 = 110
iR111 = 111
iR112 = 112
iR113 = 113
iR114 = 114
iR115 = 115
iR116 = 116
iR117 = 117
iR118 = 118
iR119 = 119
iR120 = 120
iR121 = 121
iR122 = 122
iR123 = 123
iR124 = 124
iR125 = 125
iR126 = 126
iR127 = 127
iR128 = 128
iR129 = 129
iR130 = 130
iR131 = 131
iR132 = 132
iR133 = 133
iR134 = 134
iR135 = 135
iR136 = 136
iR137 = 137
iR138 = 138
iR139 = 139
iR140 = 140
iR141 = 141
iR142 = 142
iR143 = 143
iR144 = 144
iR145 = 145
iR146 = 146
iR147 = 147
iR148 = 148
iR149 = 149
iR150 = 150
iR151 = 151
iR152 = 152
iR153 = 153
iR154 = 154
iR155 = 155
iR156 = 156
iR157 = 157
iR158 = 158
iR159 = 159
iR160 = 160
iR161 = 161
iR162 = 162
iR163 = 163
iR164 = 164
iR165 = 165
iR166 = 166
iR167 = 167
iR168 = 168
iR169 = 169
iR170 = 170
iR171 = 171
iR172 = 172
iR173 = 173
iR174 = 174
iR175 = 175
iR176 = 176
iR177 = 177
iR178 = 178
iR179 = 179
iR180 = 180
iR181 = 181
iR182 = 182
iR183 = 183
iR184 = 184
iR185 = 185
iR186 = 186
iR187 = 187
iR188 = 188
iR189 = 189
iR190 = 190
iR191 = 191
iR192 = 192
iR193 = 193
iR194 = 194
iR195 = 195
iR196 = 196
iR197 = 197
iR198 = 198
iR199 = 199
iR200 = 200
iR201 = 201
iR202 = 202
iR203 = 203
iR204 = 204
iR205 = 205
iR206 = 206
iR207 = 207
iR208 = 208
iR209 = 209
iR210 = 210
iR211 = 211
iR212 = 212
iR213 = 213
iR214 = 214
iR215 = 215
iR216 = 216
iR217 = 217
iR218 = 218

!!Now for the properties of each species
!!Start with total atomic mass
aion(iH)    = 1.0
aion(iHP)   = 0.999451
aion(iHM)   = 1.000549
aion(iH2)   = 2.0
aion(iH2P)  = 1.999451
aion(iH3P)  = 2.999451
aion(iHE)   = 4.0
aion(iHEP)  = 3.999451
aion(iC)	= 12.000000
aion(iCP)	= 11.999451
aion(iCM)	= 12.000549
aion(iO)	= 16.000000
aion(iOP)	= 15.999451
aion(iOM)	= 16.000549
aion(iC2)	= 24.000000
aion(iO2)	= 32.000000
aion(iO2P)	= 31.999451
aion(iOH)	= 17.000000
aion(iOHP)	= 16.999451
aion(iCO)	= 28.000000
aion(iCOP)	= 27.999451
aion(iCH)	= 13.000000
aion(iCHP)	= 12.999451
aion(iCH2)	= 14.000000
aion(iCH2P)	= 13.999451
aion(iHCOP)	= 28.999451
aion(iHOCP) = 28.999451
aion(iH2O)	= 18.000000
aion(iH2OP) = 17.999451
aion(iH3OP) = 18.999451
aion(iCH3P) = 14.999451
aion(iELEC) =  0.000549


!!Start with total Number of Protons
zion(iH)    = 1.0
zion(iHP)   = 1.0
zion(iHM)   = 1.0
zion(iH2)   = 2.0
zion(iH2P)  = 2.0
zion(iH3P)  = 3.0
zion(iHE)   = 2.0
zion(iHEP)  = 2.0
zion(iC)	= 6.0
zion(iCP)	= 6.0
zion(iCM)	= 6.0
zion(iO)	= 8.0
zion(iOP)	= 8.0
zion(iOM)	= 8.0
zion(iC2)	= 12.0
zion(iO2)	= 16.0
zion(iO2P)	= 16.0
zion(iOH)	= 9.0
zion(iOHP)	= 9.0
zion(iCO)	= 14.0
zion(iCOP)	= 14.0
zion(iCH)	= 7.0
zion(iCHP)	= 7.0
zion(iCH2)	= 8.0
zion(iCH2P)	= 8.0
zion(iHCOP)	= 15.0
zion(iHOCP) = 15.0
zion(iH2O)	= 10.0
zion(iH2OP) = 10.0
zion(iH3OP) = 11.0
zion(iCH3P) = 9.0
zion(iELEC) =  0.0

!!Binding Energies !!IN EV's
bion(iH)    =   0.00000
bion(iHP)   = -13.59844
bion(iHM)   =   0.77000
bion(iH2)   =   4.47788
bion(iH2P)  = -10.95000
bion(iH3P)  = -10.00000
bion(iHE)   =   0.00000
bion(iHEP)  = -24.58741
bion(iC)    =   0.00000
bion(iCP)   = -11.26030
bion(iCM)   =   1.26000
bion(iO)    =   0.00000
bion(iOP)   = -13.61810
bion(iOM)   =   1.46000
bion(iC2)   =   6.21000
bion(iO2)   =   5.11600
bion(iO2P)  =  -5.40000
bion(iOH)   =   4.39200
bion(iOHP)  =  -7.90000 
bion(iCO)   =  11.09200
bion(iCOP)  =  -5.67000
bion(iCH)   =   3.47000
bion(iCHP)  =  -10.6400
bion(iCH2)  =    4.0000
bion(iCH2P) =   -6.4000
bion(iHCOP) =    0.0000
bion(iHOCP) =    0.0000
bion(iH2O)  =    5.1100
bion(iH2OP) =   -7.5000
bion(iH3OP) =    0.0000
bion(iCH3P) =    0.0000
bion(iELEC) =    0.0000



!!Gamma

gam(iH)     = 1.666
gam(iHP)    = 1.666
gam(iHM)    = 1.666
gam(iH2)    = 1.40
gam(iH2P)   = 1.40
gam(iH3P)   = 1.310
gam(iHE)    = 1.666
gam(iHEP)   = 1.666
gam(iC)	    = 1.666
gam(iCP)    = 1.666
gam(iCM)    = 1.666
gam(iO)	    = 1.666
gam(iOP)    = 1.666
gam(iOM)    = 1.666
gam(iC2)	= 1.40
gam(iO2)	= 1.40
gam(iO2P)	= 1.40
gam(iOH)	= 1.40
gam(iOHP)	= 1.40
gam(iCO)	= 1.40
gam(iCOP)	= 1.40
gam(iCH)	= 1.40
gam(iCHP)	= 1.40
gam(iCH2)	= 1.310
gam(iCH2P)	= 1.310
gam(iHCOP)	= 1.310
gam(iHOCP)  = 1.310
gam(iH2O)	= 1.310
gam(iH2OP)  = 1.310
gam(iH3OP)  = 1.310
gam(iCH3P)  = 1.310
gam(iELEC)  = 1.666  

!!Defaults for some variables

call RuntimeParameters_get("useChem", useChem) 
call RuntimeParameters_get("fssh2", fssh2)
call RuntimeParameters_get("sigh", sigh)
call RuntimeParameters_get("Avv", Avv)
call RuntimeParameters_get("Inu", Inu)
call RuntimeParameters_get("j21", j21)
call RuntimeParameters_get("doChemCool", chem_doCool)
call RuntimeParameters_get("mChemCool", chem_mCool)
call RuntimeParameters_get("ccCase", chem_cc_case)
call RuntimeParameters_get("chem_tempmin", chem_tempmin)
call RuntimeParameters_get("chem_kida", chem_kida)

call RuntimeParameters_get("chem_cool_h2cl", chem_cool_h2cl)
call RuntimeParameters_get("chem_cool_rocl", chem_cool_rocl)
call RuntimeParameters_get("chem_cool_atcl", chem_cool_atcl)
call RuntimeParameters_get("chem_cool_hheh", chem_cool_hheh)
call RuntimeParameters_get("chem_time", chem_time)



!!j21 = 0.0
!!Avv = 0.0
!!Inu = 0.0e0
!!fssh2 = 1.0
!!sigh = 0.0 !! IF USED MUST BE SMALL (~1.0e-20 or so)
!!chem_doCool = 1
!!chem_mCool = 1
!!chem_cc_case = 1
!!pgcont = 1  !!Default yes for pgplot

return 

end subroutine Chemistry_init

