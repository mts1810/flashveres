!!****Chemistry_coolAtomic
!!
!!
!!
!! NAME
!!  Chemistry_coolAtomic
!!
!! SYNOPSIS
!!	Calculates the cooling due to the fine structure of atoms
!!  Total cooling rate is returned as chem_cool_aout
!!
!! ARGUMENTS
!!
!!*****

subroutine Chemistry_coolAtomic(temp_in,rho_in,nin,chem_cool_aout)

use Chemistry_data
use Chemistry_coolData

implicit none

#include "constants.h"
#include "Flash.h"

real, intent(IN)	 :: temp_in, rho_in
real, intent(INOUT)	 :: chem_cool_aout
real, dimension(NSPECIES) :: nin  !!number densities of my species
real	:: ctemp, t2, tcmb
real	:: hc, kconst, term
real	:: cpg1, cpg0, cpE10, cpl10, cpA10, cpC01, cpC10, cpn0, cpn1, cpI, cpI2, kk1,kk2
real	:: cg2, cg1, cg0, cE20, cE21, cE10, cA21, cA20, cA10, cC01, cC10, cC20, cC02, cC21, cC12, cn0, cn1, cn2, cI10, cI20, cI21
real	:: og2, og1, og0, oE20, oE21, oE10, oA21, oA20, oA10, oC01, oC10, oC20, oC02, oC21, oC12, on0, on1, on2, oI10, oI20, oI21
real	:: k1,k2,k3,k4,k5,k6


ctemp = temp_in
t2 = 1.0e-2*ctemp
tcmb = 2.726
chem_cool_aout = 0.0e0

hc = 1.0
kconst = 1.38066e-16  !! erg/K

!print *,' rho_in: ', rho_in, ' ctemp: ', ctemp
!print *,' nin: ', nin

!!Lets try C+ first
!! First thing first, Calculate C10

cpg1 = 4.0
cpg0 = 2.0
cpE10 = 92.0   !! E10/k (K)
cpA10 = 2.3e-6 !! 1/s
cpI2 = 1.0/(exp(cpE10/tcmb)-1.0)
!!print *, 'cpI2: ', cpI2
!!2.20272e-15 !! erg/(cm^2 Hz sr)



cpC10 = 0.0
if(ctemp > 2000.0) then
	cpC10 = cpC10 + 2.43e-7*t2**(-0.345)*nin(iELEC)  !!Electron
!!	!print *, 'cpC10: ', cpC10
	cpC10 = cpC10 + 3.1e-10*t2**(0.385)*nin(iH)		 !!Hydrogen
!!	!print *, 'cpC10: ', cpC10
else
	cpC10 = cpC10 + 3.86e-7*t2**(-0.5)*nin(iELEC)	 !!Electrons
!!	!print *, 'cpC10: ', cpC10
	cpC10 = cpC10 + 8.0e-10*t2**(0.07)*nin(iH)		 !!Hydrogen
!!	!print *, 'cpC10: ', cpC10
endif
if(ctemp > 250.0) then
	cpC10 = cpC10 + 0.75*5.85e-10*ctemp**(0.07)*nin(iH2)		 !!H2-ortho
!!	!print *, 'cpC10: ', cpC10
	cpC10 = cpC10 + 0.25*4.85e-10*ctemp**(0.07)*nin(iH2)		 !!H2-para
!!	!print *, 'cpC10: ', cpC10
else
	cpC10 = cpC10 + 0.75*(4.7e-10+4.6e-13*ctemp)*nin(iH2)     !!H2-ortho
!!	!print *, 'cpC10: ', cpC10	
	cpC10 = cpC10 + 0.25*2.5e-10*ctemp**(0.12)*nin(iH2)		 !!H2-para
!!	!print *, 'cpC10: ', cpC10
endif

cpC01 = cpC10*(cpg1/cpg0)*exp(-cpE10/ctemp)
!!print *, 'cpC01: ', cpC01
!!Ok, now we can calculate the number densities of each level
!!Assuming that Aij = Bij*I 
kk1 = 0.0e0
kk2 = 0.0e0
kk1 = (cpA10*cpI2*(cpg1/cpg0) + cpC01)
kk2 = (cpA10 + cpA10*cpI2 + cpC10)


!kk = (cpA10+cpC01)/(cpA10+(cpA10/cpI2)+cpC10)  !! this is n1/n0
!!print *, 'kk: ', kk
!cpn0 = nin(iCP)/(1.0+kk)
!cpn1 = nin(iCP) - cpn0
cpn0 = kk2/(kk1+kk2)*nin(iCP)
cpn1 = kk1/(kk1+kk2)*nin(iCP)

!print *,' nincp: ', nin(iCP)
!print *,' ncp0: ', cpn0/(nin(iCP))
!print *,' ncp1: ', cpn1/nin(iCP)
!print *,' ncpt: ', (cpn0+cpn1)/nin(iCP)
!print *,' A: ', cpA10, '  B: ', cpA10*cpI2
!print *,' Cool: ', cpE10*cpn1*(cpA10+cpA10*cpI2) , ' Heat: ', (cpA10*cpI2*cpE10*cpn0)

!print *, 'Chemistry_coolAout: ', chem_cool_aout
chem_cool_aout = chem_cool_aout + ( (cpE10*cpn1*(cpA10+cpA10*cpI2) - (cpA10*cpI2*(cpg1/cpg0)*cpE10*cpn0)) )
!chem_cool_aout = chem_cool_aout - cpE10*(cpA10*cpn1+(cpA10*cpI2)*(cpn1-(cpg1/cpg0)*cpn0))
!print *, 'Chemistry_coolAout: ', chem_cool_aout

!!
!!
!!
!!Ok, now to try the neutral C atom
!!
!!
!!


cg0 = 1.0
cg1 = 3.0
cg2 = 5.0
cE10 = 24.0
cE20 = 63.0
cE21 = 39.0
cA10 = 7.9e-8
cA20 = 2.1e-14
cA21 = 2.7e-7
cI10 = cA10/(exp(cE10/tcmb)-1.0)
cI20 = cA20/(exp(cE20/tcmb)-1.0)
cI21 = cA21/(exp(cE21/tcmb)-1.0)

!write(*,'(3(A,1pe10.3))'), 'A10: ', cA10, ' A20: ', cA20, ' A21: ', cA21
!write(*,'(3(A,1pe10.3))'), 'I10: ', cI10, ' I20: ', cI20, ' I21: ', cI21

!!Need to calculate the Cxx's
!!Do cC10 first
cC10 = 0.0e0
term = 0.0e0

cC10 = (8.7e-11 - 6.6e-11*exp(-ctemp/218.3) + 6.6e-11*exp(-2.0*ctemp/218.3))*nin(iH2)*0.75 + &
	 & (7.9e-11 - 8.7e-11*exp(-ctemp/126.4) + 1.3e-10*exp(-2.0*ctemp/126.4))*nin(iH2)*0.25  !! H2
if(ctemp > 5000.0) then
	cC10 = cC10 + 8.9e-10*ctemp**(0.117)*nin(iHP)
else
	cC10 = cC10 + (9.6e-11 - 1.8e-14*ctemp + 1.9e-18*ctemp*ctemp)*ctemp**(0.45)*nin(iHP)  !!H+
endif

if(ctemp > 1000.0) then
	term = 2.88e-6*ctemp**(-0.5)*exp(-4.446e2-2.27913e2*log(ctemp)+4.2595e1*(log(ctemp))**2 - 3.47620*(log(ctemp))**3  + 1.0508e-1*(log(ctemp))**4)
	cC10 = cC10 + term*nin(iELEC)
else
	term = 2.88e-6*ctemp**(-0.5)*exp(-9.25141-7.73782e-1*log(ctemp)+3.61184e-1*(log(ctemp))**2-1.50892e-2*(log(ctemp))**3-6.56325e-4*(log(ctemp))**4)
	cC10 = cC10 + term*nin(iELEC)
endif !!Electrons

term = 0.0e0
term = 3.6593 + 56.6023*ctemp**(-0.25) - 802.9765*ctemp**(-0.50) + 5025.1882*ctemp**(-0.75) - 17874.4255*ctemp**(-1.0) + &
	 & 38343.6655*ctemp**(-1.25) - 49249.4895*ctemp**(-1.50) + 34789.3941*ctemp**(-1.75) - 10390.9809*ctemp**(-2.0) 
term = 1.0e-11*exp(term)
!!term = 0.0e0
!!term = 1.6e-10*t2**(0.14)
cC10 = cC10 + term*nin(iH)

!!Next is cC20
term = 0.0e0
cC20 = 0.0e0
term = 1.2e-10 - 6.1e-11*exp(-ctemp/387.3)
cC20 = 0.75*term * nin(iH2)
!print *, '1 cC20: ', cC20, ' term: ', term

term = 1.1e-10 - 8.6e-11*exp(-ctemp/223.0) + 8.7e-11*exp(-2.0*ctemp/223.0)
cC20 = cC20 + 0.25*term*nin(iH2)
!print *, '2 cC20: ', cC20, ' term: ', term

if(ctemp > 5000.0) then
	term = 2.3e-9*ctemp**(0.0965)
else
	term = (3.1e-12 - 6.0e-16*ctemp + 3.9e-20*ctemp*ctemp)*ctemp
endif
cC20 = cC20 + term * nin(iHP)
!print *, '3 cC20: ', cC20, ' term: ', term

if(ctemp > 1000.0) then
	term = 1.73e-6*ctemp**(-0.5)*exp(3.50609e2 - 1.87474e2*log(ctemp) + 3.61803e1*(log(ctemp))**2 - &
		 & 3.03283e0*(log(ctemp))**3 + 9.38138e-2*(log(ctemp))**4)
else
	term = 1.73e-6*ctemp**(-0.5)*exp(-7.69735 - 1.30743*log(ctemp) + 0.697638*(log(ctemp))**2 - &
		 & 0.111338*(log(ctemp))**3 + 0.705277e-2*(log(ctemp))*4)
endif
cC20 = cC20 + term * nin(iELEC)
!print *, '4 cC20: ', cC20, ' term: ', term

term = 0.0e0
if(ctemp .lt. 1000.0 .and. ctemp .gt. 4.0) then
	term = 10.8377 - 173.4153*ctemp**(-0.333) + 2024.0272*ctemp**(-0.6666) - 13391.6549*ctemp**(-1.0) + &
		 & 52198.5522*ctemp**(-1.3333) - 124518.3586*ctemp**(-1.6666) + 178182.5823*ctemp**(-2.0) - &
	 	& 140970.6106*ctemp**(-2.333) + 47504.5861*ctemp**(-2.6666)
	term = 1.0e-11*exp(term)
else 
	term = 0.0e0
endif
!term = 9.2e-11*t2**(0.26)
cC20 = cC20 + term * nin(iH)
!print *, '5 cC20: ', cC20, ' term: ', term

!!Next is cC21
term = 0.0e0
cC21 = 0.0e0

term = 2.9e-10 - 1.9e-10*exp(-ctemp/348.9)    !! ortho h2
cC21 = cC21 + term * nin(iH2) * 0.75
term = 2.7e-10 - 2.6e-10*exp(-ctemp/250.7) + 1.8e-10*exp(-2.0*ctemp/250.7)
cC21 = cC21 + term * nin(iH2) * 0.25

if(ctemp > 5000.0) then
	term = 9.2e-9*ctemp**(0.0535)
else
	term = (1.0e-10 - 2.2e-14*ctemp + 1.7e-18*ctemp*ctemp)*ctemp**(0.70)
endif
cC21 = cC21 + term * nin(iHP)

if(ctemp > 1000.0) then
	term = 1.73e-6*ctemp**(-0.5)*exp(3.86186e2 - 2.02192e2*log(ctemp) + 3.85049e1*(log(ctemp))**2 - &
		 & 3.19268*(log(ctemp))**3 + 9.78573e-2*(log(ctemp))**4)
else
	term = 1.73e-6*ctemp**(-0.5)*exp(-7.4387 - 0.57443*log(ctemp) + 0.358264*(log(ctemp))**2 - &
	     & 4.18166e-2*(log(ctemp))**3 + 2.35272e-3*(log(ctemp))**4)
endif
cC21 = cC21 + term * nin(iELEC)

term = 0.0e0
term = 15.8996 - 201.3030*ctemp**(-0.25) + 1533.6164*ctemp**(-0.5) - 6491.0083*ctemp**(-0.75) + &
     & 15921.9239*ctemp**(-1.0) - 22691.1623*ctemp**(-1.25) + 17334.7529*ctemp**(-1.50) - 5517.9360*ctemp**(-1.75)
term = 1.0e-11*exp(term)
!!term = 2.9e-10*t2**(0.26)
cC21 = cC21 + term * nin(iH)

cC01 = cC10*(cg1/cg0)*exp(-cE10/ctemp)
cC02 = cC20*(cg2/cg0)*exp(-cE20/ctemp)
!write(*,'(3(A,1pe10.3))'), 'cC02: ', cC02, ' cC20: ', cC20, ' cE20: ', cE20, ' ctemp: ', ctemp
cC12 = cC21*(cg2/cg1)*exp(-cE21/ctemp)

!write(*,'(3(A,1pe10.3))'), ' cC10: ', cC10, ' cC20: ', cC20, ' cC21: ', cC21
!write(*,'(3(A,1pe10.3))'), ' cC01: ', cC01, ' cC02: ', cC02, ' cC12: ', cC12

!k1 = cI10*(cg1/cg0) + cC01 + cI20*(cg2/cg0) + cC02
!k2 = cA10 + cI10 + cC10
!k3 = cA20 + cI20 + cC20
!k4 = cI10 + cC10 + cI21*(cg2/cg1) + cC12
!k5 = cI10*(cg1/cg0) + cC01
!k6 = cA21 + cI21 + cC21

k1 = cI10*(cg1/cg0) + cC01 + cI20*(cg2/cg0) + cC02
!write(*,'(4(A,1pe10.3))'), 'cI10: ', cI10, ' cC01: ', cC01, ' cI20: ', cI20, ' cC02: ', cC02
k2 = cA10 + cI10 + cC10
k3 = cA20 + cI20 + cC20
k4 = cA10 + cI10 + cC10 + cI21*(cg2/cg1) + cC12
k5 = cI10*(cg1/cg0) + cC01
k6 = cA21 + cI21 + cC21

!cn0 = -(k2*k6+k3*k5)*nin(iC)/(-k2*k6-k3*k4+k5*k2-k1*k4-k1*k6-k5*k3) 
!cn1 = -(k1*k6+k3*k5)*nin(iC)/(-k2*k6-k3*k4+k5*k2-k1*k4-k1*k6-k5*k3)
!cn2 = (-k1*k4+k5*k2)*nin(iC)/(-k2*k6-k3*k4+k5*k2-k1*k4-k1*k6-k5*k3)

!write(*,'(3(A,1pe10.3))'), ' k1: ', k1, ' k2: ', k2, ' k3: ', k3
!write(*,'(3(A,1pe10.3))'), ' k4: ', k4, ' k5: ', k5, ' k6: ', k6
!write(*,'(2(A,1pe10.3))'), ' top: ', (k1*k4-k5*k2), ' bot: ', (k2*k6+k3*k4-k5*k2+k5*k3+k1*k4+k1*k6)

cn0 = ((k2*k6+k3*k4)*nin(iC)/(k2*k6+k3*k4-k5*k2+k5*k3+k1*k4+k1*k6))
cn1 = ((k1*k6+k3*k5)*nin(iC)/(k2*k6+k3*k4-k5*k2+k5*k3+k1*k4+k1*k6))
cn2 = ((k1*k4-k5*k2)*nin(iC)/(k2*k6+k3*k4-k5*k2+k5*k3+k1*k4+k1*k6))

!print *, ' nin(iC): ', nin(iC)
!print *, ' cn0: ', cn0/nin(iC)
!print *, ' cn1: ', cn1/nin(iC)
!print *, ' cn2: ', cn2/nin(iC)
!print *, ' cnt: ', (cn0/nin(iC) + cn1/nin(iC) + cn2/nin(iC))

term = 0.0e0
term = (cA10 + cI10)*cE10*cn1 + (cA20 + cI20)*cE20*cn2 + (cA21 + cI21)*cE21*cn2
term = term - (cI10*(cg1/cg0)*cE10*cn0 + cI20*(cg2/cg1)*cE20*cn0 + cI21*(cg2/cg1)*cE21*cn1)

!print *, 'Chemistry_coolAout: ', chem_cool_aout, ' term: ', term
chem_cool_aout = chem_cool_aout + term
!print *, 'Chemistry_coolAout2: ', chem_cool_aout


!
!
!
!!Ok, Now for Oxygen. 
!
!
!


og0 = 5.0
og1 = 3.0
og2 = 1.0
oE10 = 230.0
oE20 = 330.0
oE21 = 98.0
oA10 = 8.9e-5
oA20 = 1.3e-10
oA21 = 1.8e-5
oI10 = oA10/(exp(oE10/tcmb)-1.0)
oI20 = oA20/(exp(oE20/tcmb)-1.0)
oI21 = oA21/(exp(oE21/tcmb)-1.0)

!!Ok, oC10 first
term = 0.0e0
oC10 = 0.0e0

term = 2.7e-11*ctemp**(0.362) * nin(iH2) * 0.75
term = term + 3.46e-11*ctemp**(0.316) * nin(iH2) * 0.25
oC10 = oC10 + term  !!H2

if(ctemp > 3686.0) then
	term = 2.65e-10*ctemp**(0.37)
else if(ctemp < 3685.0 .and. ctemp > 194.0) then
	term = 7.75e-12*ctemp**(0.80)
else
	term = 6.38e-11*ctemp**(0.40)
endif
oC10 = oC10 + term * nin(iHP) !!H+

term = 5.12e-10*ctemp**(-0.075)

oC10 = oC10 + term * nin(iELEC) !!electrons

term = 0.0e0
term = 3.437 + 17.443*ctemp**(-0.5) - 618.761*ctemp**(-1.0) + 3757.156*ctemp**(-1.5) + &
     & -12736.468*ctemp**(-2.0) + 22785.226*ctemp**(-2.5) - 22759.228*ctemp**(-3.0) + 12668.261*ctemp**(-3.5)
term = 1.0e-11*exp(term)
!term = 9.2e-11*t2**(0.67)
oC10 = oC10 + term * nin(iH)

!!ok, oC20 next
term = 0.0e0
oC20 = 0.0e0

term = 5.49e-11*ctemp**(0.317) * 0.75 + 7.07e-11*ctemp**(0.268) * 0.25
oC20 = oC20 + term * nin(iH2)

term = 4.86e-10*ctemp**(-0.026)
oC20 = oC20 + term * nin(iELEC)

if(ctemp < 511.0) then
	term = 6.10e-13*ctemp**(1.10)
else if (ctemp > 7510.0) then	
	term = 4.49e-10*ctemp**(0.30)
else
	term = 2.12e-12*ctemp**(0.90)
endif
oC20 = oC20 + term * nin(iHP)

term= 0.0e0
term = 3.297 - 168.382*ctemp**(-0.75) + 1844.099*ctemp**(-1.50) - 68362.889*ctemp**(-2.25)  + &
	 & 1376864.737*ctemp**(-3.0) - 17964610.169*ctemp**(-3.75) + 134374927.808*ctemp**(-4.50) - &
	 & 430107587.886*ctemp**(-5.25)
term = 1.0e-11*exp(term)
!term = 4.3e-11*t2**(0.80)
oC20 = oC20 + term * nin(iH)

!!ok, oC21 next

term = 0.0e0
oC21 = 0.0e0

term = 2.74e-14*ctemp**(1.060) * 0.75 + 3.33e-15*ctemp**(1.360)*0.25
oC21 = oC21 + term * nin(iH2)

if(ctemp > 2090.0) then
	term = 3.43e-10*ctemp**(0.19)
else
	term = 2.03e-11*ctemp**(0.56)
endif
oC21 = oC21 + term * nin(iHP)

term = 1.08e-14*ctemp**(0.926)
oC21 = oC21 + term * nin(iELEC)

term = 0.0e0
term = 4.581 - 156.118*ctemp**(-0.75) + 2679.979*ctemp**(-1.50) - 78996.962*ctemp**(-2.25) + &
     & 1308323.468*ctemp**(-3.0) - 13011761.861*ctemp**(-3.75) + 71010784.971*ctemp**(-4.50) - &
     & 162826621.855*ctemp**(-5.25)
term = 1.0e-11*exp(term)
!term = 1.1e-10*t2**(0.44)
oC21 = oC21 + term * nin(iH)



oC01 = oC10*(og1/og0)*exp(-oE10/ctemp)
oC02 = oC20*(og2/og0)*exp(-oE20/ctemp)
oC12 = oC21*(og2/og1)*exp(-oE21/ctemp)


!k1 = oI10 + oC01 + oI20 + oC02
!k2 = oA10 + oI10 + oC10
!k3 = oA20 + oI20 + oC20
!k4 = oI10 + oC10 + oI21 + oC21
!k5 = oI10 + oC01
!k6 = oA21 + oI21 + oC21

k1 = oI10*(og1/og0) + oC01 + oI20*(og2/og0) + oC02
k2 = oA10 + oI10 + oC10
k3 = oA20 + oI20 + oC20
k4 = oA10 + oI10 + oC10 + oI21*(og2/og1) + oC12
k5 = oI10*(og1/og0) + oC01
k6 = oA21 + oI21 + oC21


on0 = (k2*k6+k3*k4)*nin(iO)/(k2*k6+k3*k4-k5*k2+k5*k3+k1*k4+k1*k6)
on1 = (k1*k6+k3*k5)*nin(iO)/(k2*k6+k3*k4-k5*k2+k5*k3+k1*k4+k1*k6)
on2 = (k1*k4-k5*k2)*nin(iO)/(k2*k6+k3*k4-k5*k2+k5*k3+k1*k4+k1*k6)


!print *, ' on0: ', on0/nin(iO)
!print *, ' on1: ', on1/nin(iO)
!print *, ' on2: ', on2/nin(iO)
!print *, ' ont: ', (on0/nin(iO) + on1/nin(iO) + on2/nin(iO))

term = 0.0e0
term = (oA10 + oI10)*oE10*on1 + (oA20 + oI20)*oE20*on2 + (oA21 + oI21)*oE21*on2
term = term - (oI10*(og1/og0)*oE10*on0 + oI20*(og2/og0)*oE20*on0 + oI21*(og2/og1)*oE21*on1)


!print *, 'Chemistry_coolAout: ', chem_cool_aout, ' term: ', term
chem_cool_aout = chem_cool_aout + term
!print *, 'Chemistry_coolAout3: ', chem_cool_aout

chem_cool_aout = chem_cool_aout*kconst/rho_in
!print *, 'Chemistry_coolAout4: ', chem_cool_aout

!!Putting Compton cooling here
term = 1.017e-37*tcmb**4*(ctemp-tcmb)*nin(iELEC)

chem_cool_aout = chem_cool_aout + (term/rho_in)
!print *, ' Chemistry_coolAout5: ', chem_cool_aout

!call abort()
return

end subroutine Chemistry_coolAtomic
