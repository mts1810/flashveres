#include<fstream>
#include<iostream>
#include<cstring>
#include<vector>
#include<algorithm>
#include<sstream>
#include<string>
using namespace std;

struct reaction{
    string rxnnumber;
    int num_reactants;
    int num_products;
    vector<string> R;
    vector<string> P;
};

void read_spec(vector<string> *);
void read_rxns(vector<reaction> *);
void prep_dydt_jacob(int, vector<string> *, vector<string> *, vector<string> );
int mapspec(vector<string>, string);
void write_dydt(vector<string>);
void write_jacob(vector<string>);
void write_mcord(vector<string>,vector<string>);
void write_sj(vector<string>,vector<string>);

int main()
{
    int num_spec = 0;
    int i,j=0;
    int iat=0;
    string line;
    //Vectors that hold our species list and reactions
    vector<string> species;
    vector<reaction> reactions;

    vector<string> dydt; //Holds the dY/dt or ODES
    vector<string> jacob; //Holds the Jacbian values
    
    //Now read in the species and reactions
    read_spec(&species);
    num_spec =species.size();
    read_rxns(&reactions);

    //Now for the fun part. I need to loop over the reactions and fill up the dy/dt and d(dy/dt)/dy.
    prep_dydt_jacob(num_spec,&dydt,&jacob,species);
    
    for(i=0;i<reactions.size();i++)
    {
        //I guess I will follow what I did for the python version and have a bunch of
        //if statements for each case
        //First check for UV or CR
        //Loop over products and reactants and map them
        int iR[3] = {-10,-10,-10};
        int iP[3] = {-10,-10,-10};
        //vector<string> iVR;
        for(j=0;j<reactions[i].num_reactants;j++)
        {
            iR[j] = mapspec(species,reactions[i].R[j]);
        }
        for(j=0;j<reactions[i].num_products;j++)
        {
            iP[j] = mapspec(species,reactions[i].P[j]);
        }
        //Ok, lets look for UV and CR first
        //Usually CR and UV will be defined in R[1]
        //There should be a cleaner way to do this..Loops and some such.
        if( iR[1] == -4 || iR[1] == -5)
        {
        	if(reactions[i].num_products==2)
        	{
            	//Do dydt first
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";
            	dydt[iR[0]].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";
            	dydt[iP[0]].append(line);
            	dydt[iP[1]].append(line);
            	//Now the Jacobian stuff
            	line = "-ratraw(i"+reactions[i].rxnnumber+")";
            	iat = iR[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")";
            	iat = iR[0]*num_spec + iP[0];
            	jacob[iat].append(line);
            	iat = iR[0]*num_spec + iP[1];
            	jacob[iat].append(line);
            }else if(reactions[i].num_products==3)
            {
            	//Do dydt first
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";
            	dydt[iR[0]].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";
            	dydt[iP[0]].append(line);
            	dydt[iP[1]].append(line);
            	dydt[iP[2]].append(line);
            	//Now the Jacobian stuff
            	line = "-ratraw(i"+reactions[i].rxnnumber+")";
            	iat = iR[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")";
            	iat = iR[0]*num_spec + iP[0];
            	jacob[iat].append(line);
            	iat = iR[0]*num_spec + iP[1];
            	jacob[iat].append(line);
            	iat = iR[0]*num_spec + iP[2];
            	jacob[iat].append(line);
            
            }else
            {
            	cout << "I don't know how to handle RXN: " << reactions[i].rxnnumber << endl;
        		exit(1);
        	}
        }else if( reactions[i].num_reactants == 2)
        {
        	if(reactions[i].num_products==1)
        	{
        		//dYdt First
        		cout << "RXN: " << reactions[i].rxnnumber << " is a 2x1 " << endl;
        		line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";
        		dydt[iR[0]].append(line);
        		dydt[iR[1]].append(line);
        		line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";
        		dydt[iP[0]].append(line);
				//Jacobian
				line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")";  
				iat = iR[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";  
				iat = iR[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[1];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")";  
				iat = iP[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";  
				iat = iP[0]*num_spec + iR[1];
            	jacob[iat].append(line);
        	}else if(reactions[i].num_products==2)
        	{
        		cout << "RXN: " << reactions[i].rxnnumber << " is a 2x2 " << endl;
        		line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";
        		dydt[iR[0]].append(line);
        		dydt[iR[1]].append(line);
        		line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";
        		dydt[iP[0]].append(line);
        		dydt[iP[1]].append(line);
				//Jacobian
				line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")";  
				iat = iR[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";  
				iat = iR[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[1];
            	jacob[iat].append(line);         	            	
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")";  
				iat = iP[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";  
				iat = iP[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[1];
            	jacob[iat].append(line);
				
        	}else if(reactions[i].num_products==3)
        	{
        		cout << "RXN: " << reactions[i].rxnnumber << " is a 2x3 " << endl;
        		line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";
        		dydt[iR[0]].append(line);
        		dydt[iR[1]].append(line);
        		line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";
        		dydt[iP[0]].append(line);
        		dydt[iP[1]].append(line);
        		dydt[iP[2]].append(line);
				//Jacobian
				line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")";  
				iat = iR[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";  
				iat = iR[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[1];
            	jacob[iat].append(line);       
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")";  
				iat = iP[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iP[2]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")";  
				iat = iP[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iP[2]*num_spec + iR[1];
            	jacob[iat].append(line);
        	
        	}else
        	{
        		cout << "I don't know how to handle RXN: " << reactions[i].rxnnumber << endl;
        		exit(1);
        	}
        }else if( reactions[i].num_reactants == 3)
        {
			if(reactions[i].num_products==1)
        	{
        		cout << "RXN: " << reactions[i].rxnnumber << " is a 3x1 " << endl;
        		//dYdt First
        		line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";
        		dydt[iR[0]].append(line);
        		dydt[iR[1]].append(line);
        		dydt[iR[2]].append(line);
        		line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";
        		dydt[iP[0]].append(line);
				//Jacobian
				line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iR[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iR[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[1];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";  
				iat = iR[0]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[2];
            	jacob[iat].append(line);
				line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iP[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iP[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";  
				iat = iP[0]*num_spec + iR[2];
            	jacob[iat].append(line);
        	}else if(reactions[i].num_products==2)
        	{
        		cout << "RXN: " << reactions[i].rxnnumber << " is a 3x2 " << endl;
        		//dYdt First
        		line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";
        		dydt[iR[0]].append(line);
        		dydt[iR[1]].append(line);
        		dydt[iR[2]].append(line);
        		line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";
        		dydt[iP[0]].append(line);
        		dydt[iP[1]].append(line);
				//Jacobian
				line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iR[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iR[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[1];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";  
				iat = iR[0]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[2];
            	jacob[iat].append(line);
				line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iP[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iP[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[1];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";  
				iat = iP[0]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[2];
            	jacob[iat].append(line);
        	}else if(reactions[i].num_products==3)
        	{
        		cout << "RXN: " << reactions[i].rxnnumber << " is a 3x3 " << endl;
        		//dYdt First
        		line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";
        		dydt[iR[0]].append(line);
        		dydt[iR[1]].append(line);
        		dydt[iR[2]].append(line);
        		line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";
        		dydt[iP[0]].append(line);
        		dydt[iP[1]].append(line);
        		dydt[iP[2]].append(line);
				//Jacobian
				line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iR[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iR[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[1];
            	jacob[iat].append(line);
            	line = "-ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";  
				iat = iR[0]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iR[1]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iR[2]*num_spec + iR[2];
            	jacob[iat].append(line);
				line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[1]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iP[0]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[0];
            	jacob[iat].append(line);
            	iat = iP[2]*num_spec + iR[0];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[2]+")";  
				iat = iP[0]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[1];
            	jacob[iat].append(line);
            	iat = iP[2]*num_spec + iR[1];
            	jacob[iat].append(line);
            	line = "+ratraw(i"+reactions[i].rxnnumber+")*ys(i"+reactions[i].R[0]+")*ys(i"+reactions[i].R[1]+")";  
				iat = iP[0]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iP[1]*num_spec + iR[2];
            	jacob[iat].append(line);
            	iat = iP[2]*num_spec + iR[2];
            	jacob[iat].append(line);
        	}else
        	{
        		cout << "I don't know how to handle RXN: " << reactions[i].rxnnumber << endl;
        		exit(1);
        	}
        }else
        {
        	cout << "I don't know how to handle RXN: " << reactions[i].rxnnumber << endl;
        	exit(1);
        }
        
    }
	//Ok, write out the stuffs
	//I include mcord and sj in case it is a sparse matrix
    write_dydt(dydt);
    write_jacob(jacob);
    write_mcord(jacob,species);
    write_sj(jacob,species);
    
    return 0;
}

void read_spec(vector<string>* species)
{
    ifstream myfile;
    string line;
    int ispec=0;
    myfile.open("spec.dat");
    
    if(myfile.is_open())
    {
        while(getline(myfile,line))
        {
            line.erase(remove(line.begin(),line.end(),' '),line.end());
            species->push_back(line);
            ispec++;
        }
    }else
    {
        cout << "Can't open spec.dat!\n";
        exit(1);
    }
}

void read_rxns(vector<reaction>* reactions)
{
    ifstream myfile;
    string line;
    int sofar = 0;
    myfile.open("input.dat");
    
    if(myfile.is_open())
    {
        while(getline(myfile,line))
        {
            istringstream iss(line);
            vector<string> subs;
            do{
                string sub;
                iss >> sub;
                subs.push_back(sub);
            }while(iss);
            
            reaction temp;
            temp.rxnnumber = subs[0];
            temp.num_reactants = stoi(subs[1]);
            temp.num_products = stoi(subs[2]);
            if(temp.num_reactants==1)
            {
                cout << "Number of Reactants is equal to 1 \n";
                exit(1);
            }
            if(temp.num_reactants==2)
            {
                temp.R.push_back(subs[3]);
                temp.R.push_back(subs[4]);
                temp.R.push_back(" ");
                sofar = 5;
            }else if(temp.num_reactants==3)
            {
                temp.R.push_back(subs[3]);
                temp.R.push_back(subs[4]);
                temp.R.push_back(subs[5]);
                sofar = 6;
            }else
            {
                cout << "I only understand num_reactants ==2 or 3!\n";
                exit(1);
            }
            if(temp.num_products==1)
            {
                temp.P.push_back(subs[sofar]);
                temp.P.push_back(" ");
                temp.P.push_back(" ");
            }else if(temp.num_products==2)
            {
                temp.P.push_back(subs[sofar]);
                temp.P.push_back(subs[sofar+1]);
                temp.P.push_back(" ");
            }else if(temp.num_products==3)
            {
                temp.P.push_back(subs[sofar]);
                temp.P.push_back(subs[sofar+1]);
                temp.P.push_back(subs[sofar+2]);
            }else
            {
                cout << "I only understand num_products == 1 or 2 or 3!\n";
                exit(1);
            }
            reactions->push_back(temp);
        }
    }else
    {
        cout << "Can't open spec.dat!\n";
        exit(1);
    }
}

void prep_dydt_jacob(int nspec, vector<string>* dydt, vector<string>* jacob, vector<string> species)
{
    int i,j = 0;
    string temp;
    
    for(i=0;i<nspec;i++)
    {
        temp = "dydt(i"+(species[i])+")= ";
        dydt->push_back(temp);
        
        for(j=0;j<nspec;j++)
        {
            temp = "dfdy(i"+species[i]+",i"+species[j]+")= ";
            jacob->push_back(temp);
        }
    }
}

int mapspec(vector<string> specs, string wanted_spec)
{
    int i,iat = -20;
    if(wanted_spec == "UV" || wanted_spec == "CR")
    {
        iat = -5;
    }else if(wanted_spec=="")
    {
        iat = -10;
    }else
    {
        for(i=0;i<specs.size();i++)
        {
            if(specs[i]==wanted_spec)
            {
                iat = i;
                break;
            }
        }
    }
    if(iat==-20)
    {
    	//Did not find the species wanted. This is bad
    	cout << "Did not find species: " << wanted_spec << endl;
    	cout << "Check input files! " << endl;
    	exit(1);
    }
    return iat;
}

void write_dydt(vector<string> dydt)
{
	ofstream ofile;
	ofile.open("dydt.txt");
	int i = dydt.size();
	
	for(i=0;i<dydt.size();i++)
	{
		ofile << dydt[i] << endl;
	}
	ofile.close();
}

void write_jacob(vector<string> jacob)
{
	ofstream ofile;
	ofile.open("jacob.txt");
	int i = jacob.size();
	
	for(i=0;i<jacob.size();i++)
	{
		ofile << jacob[i] << endl;
	}
	ofile.close();
}

void write_mcord(vector<string> jacob, vector<string> species)
{
	ofstream ofile;
	ofile.open("mcord.txt");
	int i = jacob.size();
	int nspec = species.size();
	string line;
	int ii,jj=0;
	for(i=0;i<jacob.size();i++)
	{
		ii = i/nspec;
		jj = i-(ii*nspec);
		if(jacob[i].size() > 20)
		{
			//cout << jacob[i] << endl;
			line = "call Chemistry_mcord(i"+species[ii]+",i"+species[jj]+",iloc,jloc,nzo,np,eloc,nterms,neloc)";
			ofile << line << endl;
		}
	}
	ofile.close();
}

void write_sj(vector<string> jacob, vector<string> species)
{
	ofstream ofile;
	ofile.open("sparsejacobian.txt");
	int nspec = species.size();
	int i=0;
	string line,line2;
	string eq="=";
	int ii,jj=0;
	size_t found;
	
	for(i=0;i<jacob.size();i++)
	{
		ii=i/nspec;
		jj=i-(ii*nspec);
		if(jacob[i].size() > 20)
		{
			line = "\n!!dfdy(i"+species[ii]+",i"+species[jj]+")";
			ofile << line << endl;
			line = "nt = nt + 1";
			ofile << line << endl;
			line = "iat = eloc(nt)";
			ofile << line << endl;
			found = jacob[i].find(eq);
			line2 = jacob[i];
			line2.erase(0,found+1);
			line = "dfdy(iat) = "+line2;
			ofile << line << endl;
		}
	}
	ofile.close();
}
