import os,sys
import numpy as np
import scipy

def stsp(line):
    tmp=(line.strip()).split()
    return tmp

def mapspec(cc):
	ii = -10
	if(cc=='E'):
		ii=41
    	elif(cc=='H'):
        	ii=0
	elif(cc=='HP'):
        	ii=1
	elif(cc=='HE'):
        	ii=2
	elif(cc=='HEP'):
        	ii=3
	elif(cc=='HEPP'):
        	ii=4
	elif(cc=='C'):
        	ii=5
	elif(cc=='CP'):
        	ii=6
	elif(cc=='C2P'):
        	ii=7
	elif(cc=='C3P'):
        	ii=8
	elif(cc=='C4P'):
        	ii=9
	elif(cc=='C5P'):
		ii=10
	elif(cc=='O'):
		ii=11
	elif(cc=='OP'):
	    	ii=12
	elif(cc=='O2P'):
	    	ii=13
	elif(cc=='O3P'):
	   	ii=14
	elif(cc=='O4P'):
	   	ii=15
	elif(cc=='O5P'):
        	ii=16
	elif(cc=='O6P'):
        	ii=17
	elif(cc=='O7P'):
        	ii=18
	elif(cc=='NA'):
        	ii=19
	elif(cc=='NAP'):
        	ii=20
    	elif(cc=='MG'):
        	ii=21
    	elif(cc=='MGP'):
        	ii=22
    	elif(cc=='MG2P'):
        	ii=23
	elif(cc=='N'):
		ii=24
	elif(cc=='NP'):
		ii=25
	elif(cc=='N2P'):
		ii=26
	elif(cc=='N3P'):
		ii=27
	elif(cc=='N4P'):
		ii=28
	elif(cc=='N5P'):
		ii=29
	elif(cc=='SI'):
		ii=30
	elif(cc=='SIP'):
		ii=31
	elif(cc=='SI2P'):
		ii=32
	elif(cc=='SI3P'):
		ii=33
	elif(cc=='SI4P'):
		ii=34
	elif(cc=='SI5P'):
		ii=35
	elif(cc=='FE'):
		ii=36
	elif(cc=='FEP'):
		ii=37
	elif(cc=='FE2P'):
		ii=38
	elif(cc=='FE3P'):
		ii=39
	elif(cc=='FE4P'):
		ii=40
	elif(cc=='UV'):
		ii = -5
	elif(cc=='CR'):
		ii = -4
	else:
		print "DING - PROBLEM IN INPUT - CHECK SPECIES CASE"
                print cc
		exit()
	return ii

nspec	= 42	
species = scipy.zeros(nspec)
dts   = np.chararray(nspec,itemsize=5000)
dts[:] = ''
jacob = np.chararray((nspec,nspec),itemsize=5000)
jacob[:][:] = ''
cname = np.chararray(nspec,itemsize=5000)
cname[:] = ''
for i in range(0,nspec):
    species[i] = i

cname[0] = 'iH'
cname[1] = 'iHP'
cname[2] = 'iHE'
cname[3] = 'iHEP'
cname[4] = 'iHEPP'
cname[5] = 'iC'
cname[6] = 'iCP'
cname[7] = 'iC2P'
cname[8] = 'iC3P'
cname[9] = 'iC4P'
cname[10] = 'iC5P'
cname[11] = 'iO'
cname[12] = 'iOP'
cname[13] = 'iO2P'
cname[14] = 'iO3P'
cname[15] = 'iO4P'
cname[16] = 'iO5P'
cname[17] = 'iO6P'
cname[18] = 'iO7P'
cname[19] = 'iNA'
cname[20] = 'iNAP'
cname[21] = 'iMG'
cname[22] = 'iMGP'
cname[23] = 'iMG2P'
cname[24] = 'iN'
cname[25] = 'iNP'
cname[26] = 'iN2P'
cname[27] = 'iN3P'
cname[28] = 'iN4P'
cname[29] = 'iN5P'
cname[30] = 'iSI'
cname[31] = 'iSIP'
cname[32] = 'iSI2P'
cname[33] = 'iSI3P'
cname[34] = 'iSI4P'
cname[35] = 'iSI5P'
cname[36] = 'iFE'
cname[37] = 'iFEP'
cname[38] = 'iFE2P'
cname[39] = 'iFE3P'
cname[40] = 'iFE4P'
cname[41] = 'iE'

ih	= 0
ihp	= 1
ihe	= 2
ihep	= 3
ihepp 	= 4
ic	= 5
icp	= 6
ic2p	= 7
ic3p	= 8
ic4p	= 9
ic5p	= 10
io	= 11
iop	= 12
io2p	= 13
io3p	= 14
io4p	= 15
io5p	= 16
io6p	= 17
io7p	= 18
ina	= 19
inap	= 20
img     = 21
imgp    = 22
img2p   = 23
inn	= 24
inp	= 25
in2p 	= 26
in3p	= 27
in4p	= 28
in5p	= 29
isi	= 30
isip	= 31
isi2p	= 32
isi3p	= 33
isi4p	= 34
isi5p   = 35
ife	= 36
ifep	= 37
ife2p	= 38
ife3p	= 39
ife4p	= 40
ielec	= 41

linp = open('input.dat','r').readlines()
nleft = 0
nright = 0
nreact = 0

for line in linp:
	nreact = 0
	nleft = 0
	nright = 0
	tmp=stsp(line)
        print tmp
	nreact = tmp[0]
	nleft = int(tmp[1])
	nright = int(tmp[2])
	
	lreac= np.chararray(nleft,itemsize = 5)
        lreac[:] = ''
	rreac= np.chararray(nright,itemsize = 5)
        rreac[:] = ''
	ilreac = scipy.zeros(nleft,int)
	irreac = scipy.zeros(nright,int)
	
	for i in range(0,nleft):
		lreac[i]=(tmp[i+3])
                print lreac[i],tmp[i+3]
		ilreac[i] = mapspec(lreac[i])
		
	for i in range(0,nright):
		rreac[i]=tmp[i+3+nleft]
		irreac[i] = mapspec(rreac[i])
		
	print nreact, nleft, nright, ilreac, lreac, irreac, rreac
	name = " "
	
	if ilreac[1] == -5:
		il1 = ilreac[0]
		il2 = ilreac[1]
		l1 = lreac[0]
		l2 = lreac[1]
		ir1 = irreac[0]
		ir2 = irreac[1]
		r1 = rreac[0]
		r2 = rreac[1]
		nr = nreact

		name = "-ratraw(i"+nr+")*ys(i"+l1+") "
		dts[int(il1)] = dts[int(il1)] + name
		name = "+ratraw(i"+nr+")*ys(i"+l1+") "
		dts[int(ir1)] = dts[int(ir1)] + name
		dts[int(ir2)] = dts[int(ir2)] + name
		
		name = "-ratraw(i"+nr+") "
		jacob[int(il1),int(il1)] = jacob[int(il1),int(il1)] + name		
		name = "+ratraw(i"+nr+") "
		jacob[int(ir1),int(il1)] = jacob[int(ir1),int(il1)] + name
		jacob[int(ir2),int(il1)] = jacob[int(ir2),int(il1)] + name
		
	elif ilreac[1] == -4:
		if nright == 2:
			il1 = ilreac[0]
			il2 = ilreac[1]
			l1 = lreac[0]
			l2 = lreac[1]
			nr = nreact
			
			ir1 = irreac[0]
			ir2 = irreac[1]
			r1 = rreac[0]
			r2 = rreac[1]
			
			name = "-ratraw(i"+nr+")*ys(i"+l1+") "
			dts[int(il1)] = dts[int(il1)] + name
			name = "+ratraw(i"+nr+")*ys(i"+l1+") "
			dts[int(ir1)] = dts[int(ir1)] + name
			dts[int(ir2)] = dts[int(ir2)] + name
			
			name = "-ratraw(i"+nr+") "
			jacob[int(il1),int(il1)] = jacob[int(il1),int(il1)] + name
			name = "+ratraw(i"+nr+") "
			jacob[int(ir1),int(il1)] = jacob[int(ir1),int(il1)] + name
			jacob[int(ir2),int(il1)] = jacob[int(ir2),int(il1)] + name

		elif nright == 3:
			il1 = ilreac[0]
			il2 = ilreac[1]
			l1 = lreac[0]
			l2 = lreac[1]
			nr = nreact
			
			ir1 = irreac[0]
			ir2 = irreac[1]
			ir3 = irreac[2]
			r1 = rreac[0]
			r2 = rreac[1]
			r3 = rreac[2]
			
			name = "-ratraw(i"+nr+")*ys(i"+l1+") "
			dts[int(il1)] = dts[int(il1)] + name
			name = "+ratraw(i"+nr+")*ys(i"+l1+") "
			dts[int(ir1)] = dts[int(ir1)] + name
			dts[int(ir2)] = dts[int(ir2)] + name
			dts[int(ir3)] = dts[int(ir3)] + name
			
			name = "-ratraw(i"+nr+") "
			jacob[int(il1),int(il1)] = jacob[int(il1),int(il1)] + name
			name = "+ratraw(i"+nr+") "
			jacob[int(ir1),int(il1)] = jacob[int(ir1),int(il1)] + name
			jacob[int(ir2),int(il1)] = jacob[int(ir2),int(il1)] + name
			jacob[int(ir3),int(il1)] = jacob[int(ir3),int(il1)] + name
		else:
			print "CR does not support NL !=2 and NR !=2 or 3"
	
	elif nleft == 2:
		il1 = ilreac[0]
		il2 = ilreac[1]
		l1 = lreac[0]
		l2 = lreac[1]
		nr = nreact
		name = "-ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l2+") "
		dts[int(il1)] = dts[int(il1)] + name 
		dts[int(il2)] = dts[int(il2)] + name
		name = "-ratraw(i"+nr+")*ys(i"+l2+") "
		jacob[int(il1),int(il1)] = jacob[int(il1),int(il1)] + name
		jacob[int(il2),int(il1)] = jacob[int(il2),int(il1)] + name
		name = "-ratraw(i"+nr+")*ys(i"+l1+") "
		jacob[int(il1),int(il2)] = jacob[int(il1),int(il2)] + name
		jacob[int(il2),int(il2)] = jacob[int(il2),int(il2)] + name
		if nright == 1:
			ir1 = irreac[0]
			r1 = rreac[0]
			name = "+ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l2+") "
			dts[int(ir1)] = dts[int(ir1)] + name
			name = "+ratraw(i"+nr+")*ys(i"+l2+") "
			jacob[int(ir1),int(il1)] = jacob[int(ir1),int(il1)]+name
			name = "+ratraw(i"+nr+")*ys(i"+l1+") "
			jacob[int(ir1),int(il2)] = jacob[int(ir1),int(il2)]+name
		elif nright == 2:
			ir1 = irreac[0]
			ir2 = irreac[1]
			r1 = rreac[0]
			r2 = rreac[1]
			#name = "+(k_"+nr+" Y_"+l1+" Y_"+l2+")*den*Na"
			name = "+ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l2+") "
			dts[int(ir1)] = dts[int(ir1)] + name
			dts[int(ir2)] = dts[int(ir2)] + name
			#name = "+(k_"+nr+" Y_"+l2+")*den*na"
			name = "+ratraw(i"+nr+")*ys(i"+l2+") "
			jacob[int(ir1),int(il1)] = jacob[int(ir1),int(il1)]+name
			jacob[int(ir2),int(il1)] = jacob[int(ir2),int(il1)]+name
			#name = "+(k_"+nr+" Y_"+l1+")*den*na"
			name = "+ratraw(i"+nr+")*ys(i"+l1+") "
			jacob[int(ir1),int(il2)] = jacob[int(ir1),int(il2)]+name
			jacob[int(ir2),int(il2)] = jacob[int(ir2),int(il2)]+name
		elif nright == 3:
			ir1 = irreac[0]
			ir2 = irreac[1]
			ir3 = irreac[2]
			r1 = rreac[0]
			r2 = rreac[1]
			r3 = rreac[2]
			#name = "+(k_"+nr+" Y_"+l1+" Y_"+l2+")*den*Na"
			name = "+ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l2+") "
			dts[int(ir1)] = dts[int(ir1)] + name
			dts[int(ir2)] = dts[int(ir2)] + name
			dts[int(ir3)] = dts[int(ir3)] + name
			#name = "+(k_"+nr+" Y_"+l2+")*den*na"
			name = "+ratraw(i"+nr+")*ys(i"+l2+") "
			jacob[int(ir1),int(il1)] = jacob[int(ir1),int(il1)]+name
			jacob[int(ir2),int(il1)] = jacob[int(ir2),int(il1)]+name
			jacob[int(ir3),int(il1)] = jacob[int(ir3),int(il1)]+name
			#name = "+(k_"+nr+" Y_"+l1+")*den*na"
			name = "+ratraw(i"+nr+")*ys(i"+l1+") "
			jacob[int(ir1),int(il2)] = jacob[int(ir1),int(il2)]+name
			jacob[int(ir2),int(il2)] = jacob[int(ir2),int(il2)]+name
			jacob[int(ir3),int(il2)] = jacob[int(ir3),int(il2)]+name
		else:
			print "NOT YET"
	elif nleft == 3:
		if nright == 2:
			il1 = ilreac[0]
			il2 = ilreac[1]
			il3 = ilreac[2]
			l1 = lreac[0]
			l2 = lreac[1] 
			l3 = lreac[2]
			nr = nreact 
			ir1 = irreac[0]
			ir2 = irreac[1]
			r1 = rreac[0]
			r2 = rreac[1]
			#name = "-(k_"+nr+" Y_"+l1+" Y_"+l2+ " Y_"+l3+")*den^2*Na^2"
			name = "-ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l2+")*ys(i"+l3+") "
			dts[int(il1)] = dts[int(il1)] + name
			dts[int(il2)] = dts[int(il2)] + name
			dts[int(il3)] = dts[int(il3)] + name
			#name = "-(k_"+nr+" Y_"+l2+" Y_"+l3+")*den^2*Na^2"
			name = "-ratraw(i"+nr+")*ys(i"+l2+")*ys(i"+l3+") "
			jacob[int(il1),int(il1)] = jacob[int(il1),int(il1)] + name	
			jacob[int(il2),int(il1)] = jacob[int(il2),int(il1)] + name
			jacob[int(il3),int(il1)] = jacob[int(il3),int(il1)] + name
			#name = "-(k_"+nr+" Y_"+l1+" Y_"+l3+")*den^2*Na^2"
			name = "-ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l3+") "
			jacob[int(il1),int(il2)] = jacob[int(il1),int(il2)] + name	
			jacob[int(il2),int(il2)] = jacob[int(il2),int(il2)] + name
			jacob[int(il3),int(il2)] = jacob[int(il3),int(il2)] + name
			#name = "-(k_"+nr+" Y_"+l1+" Y_"+l2+")*den^2*Na^2"
			name = "-ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l2+") "
			jacob[int(il1),int(il3)] = jacob[int(il1),int(il3)] + name	
			jacob[int(il2),int(il3)] = jacob[int(il2),int(il3)] + name
			jacob[int(il3),int(il3)] = jacob[int(il3),int(il3)] + name
			#name = "+(k_"+nr+" Y_"+l1+" Y_"+l2+ " Y_"+l3+")*den^2*Na^2"
			name = "+ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l2+")*ys(i"+l3+") "
			dts[int(ir1)] = dts[int(ir1)] + name
			dts[int(ir2)] = dts[int(ir2)] + name
			#name = "+(k_"+nr+" Y_"+l2+" Y_"+l3+")*den^2*Na^2"
			name = "+ratraw(i"+nr+")*ys(i"+l2+")*ys(i"+l3+") "
			jacob[int(ir1),int(il1)] = jacob[int(ir1),int(il1)] + name	
			jacob[int(ir2),int(il1)] = jacob[int(ir2),int(il1)] + name
			#name = "+(k_"+nr+" Y_"+l1+" Y_"+l3+")*den^2*Na^2"
			name = "+ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l3+") "
			jacob[int(ir1),int(il2)] = jacob[int(ir1),int(il2)] + name	
			jacob[int(ir2),int(il2)] = jacob[int(ir2),int(il2)] + name
			#name = "+(k_"+nr+" Y_"+l1+" Y_"+l2+")*den^2*Na^2"
			name = "+ratraw(i"+nr+")*ys(i"+l1+")*ys(i"+l2+") "
			jacob[int(ir1),int(il3)] = jacob[int(ir1),int(il3)] + name	
			jacob[int(ir2),int(il3)] = jacob[int(ir2),int(il3)] + name
		else:
			print "NL=3, ONLY SUPPORTS NR==2"
		
file_dts = open("./dts2.txt","w")
		
for i in range(0,nspec):
	print i,dts[i]
	s=repr(i+1)
	sa = cname[i]
        #file_dts.write(s)
	saa = "dydt("+sa+") = "
        file_dts.write(saa)
        file_dts.write(dts[i])
	file_dts.write("\n\n")
		
file_jac = open("./jacob2.txt","w")		

for i in range(0,nspec):
	for j in range(0,nspec):
            if(len(jacob[i,j]) > 1):
		print i,j, jacob[i,j]
		#s1 = repr(i+1)
		#s1 = repr(cname[i])
		s1 = cname[i]
		#s2 = repr(j+1)
		#s2 = repr(cname[j])
		s2 = cname[j]
		s3 = "dfdy("+s1+","+s2+")= "
		#file_jac.write(s1)
		#file_jac.write(" ")
		#file_jac.write(s2)
		file_jac.write(s3)
		file_jac.write(jacob[i,j])
		file_jac.write("\n\n")
	file_jac.write("\n\n")

file_mcord = open("./mcord2.txt","w")
for i in range(0,nspec):
    for j in range(0,nspec):
        if( len(jacob[i,j]) > 1 ):
            ln = 'call Chemistry_mcord('+cname[i]+','+cname[j]+',iloc,jloc,nzo,np,eloc,nterms,neloc)'
            file_mcord.write(ln+'\n')
            print ln
    ln = '\n'
    file_mcord.write(ln)
    print ln

file_sj = open("./networkSJ2.txt","w")
for i in range(0,nspec):
    for j in range(0,nspec):
        if( len(jacob[i,j]) > 1):
            ln = '\n!!dfdy(%s,%s)'%(cname[i],cname[j])
            file_sj.write(ln+'\n')
            ln = 'nt = nt + 1 \n'
            file_sj.write(ln)
            ln = 'iat = eloc(nt)\n'
            file_sj.write(ln)
            ln = 'dfdy(iat) = '+jacob[i,j]+'\n'
            file_sj.write(ln)
	
exit()
    
