!!****if* source/physics/Cosmology/CosmologyMain/csm_chevalierCleggDeriv
!!
!! NAME
!!
!!  csm_chevalierCleggDeriv
!!
!! SYNOPSIS
!!
!!  call csm_chevalierCleggDeriv(real, intent(IN)  :: M,
!!                          real, intent(INOUT)  :: dMdtvar)
!!
!! DESCRIPTION
!!  
!!  This is the ChevalierClegg Derivative equation that is used by
!!  csm_integrateChevalierClegg.  This will return the RHS of the 
!!  ChevalierClegg equation.
!!
!! ARGUMENTS
!!
!!   M    : The initial Mach number 
!!
!!   dMdt : The derivative of the Mach number with respect to time
!!
!!
!!
!!***

subroutine csm_chevalierCleggDeriv (M, dMdt)


  use Cosmology_data, ONLY : csm_r, csm_r0,csm_v,csm_gamma
  implicit none
  
  real, intent(IN) :: M
  real, intent(INOUT) :: dMdt
  real :: gam
  real :: alpha,beta,term1,dterm1,term2,dterm2

  gam = csm_gamma
  alpha = 2./(gam-1.)
  beta  = (gam+1)/(2*(gam-1.))
  term1  = M**alpha
  dterm1 = alpha*M**(alpha-1.)
  term2  = ((gam-1.+2./M/M)/(1.+gam))**beta
  dterm2 = beta*(2./(1+gam))*(-2.)/M**3.*((gam-1+2./M/M)/(1+gam))**(beta-1.) 
  dMdt = 2.*csm_v/csm_r0*(csm_r/csm_r0)/(dterm1*term2+term1*dterm2)

           
!===============================================================================

  return
end subroutine csm_chevalierCleggDeriv
